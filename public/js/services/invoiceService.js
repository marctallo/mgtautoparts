(function(){
	
	'use strict';
	
	angular.module('main')
		.factory('invoiceFactory',function($http){
		
		var invoice = {
			
			'getCategory' : function(){
				
				return $http({ method: 'GET',url: 'getCategory',});
			},
			'getItems' : function(id){
				
				return $http({method: 'GET',url: 'getItems',params : {'cid' : id}});
			}
		}
		
		return invoice;
	});
	
})();
