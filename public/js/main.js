var invoice_ids = [];

$(document).ready(function(){
	

 
  	$('#reminder').fullCalendar({
			header: {
				left: '',
				center: 'title',
				right: 'prev,next today'
			},
			defaultDate: new Date(),
			selectable: true,
			selectHelper: true,
			select: function(start, end) {
				
				var title;
				
				bootbox.prompt("Reminder Title", function(result){ 

					
					if(result != null){
						
						
						
						if($.trim(result).length > 0){
							
							title = result;
							var eventData;
					
							
							eventData = {
								title: title,
								start: start.format(),
								end: end.format()
							};
				

							$.get($('#url').val()+"/saveReminder",eventData, function(data){
								
								
								if(data.success == 1){
									
									eventData.id = data.id;
									
									$('#reminder').fullCalendar('renderEvent', eventData, true); // stick? = true		
								}
								
							});
							
						}
					}
				
				});
				
				
				$('#reminder').fullCalendar('unselect');
			},
			eventLimit: true,
			eventSources : {
				
				url : $('#url').val()+"/getReminders"
			},
			
			eventClick : function(calEvent){
				
				
				bootbox.confirm("Delete this reminder?", function(result){ 
					
					if(result){
						
						
						$.get($('#url').val()+"/removeReminder",{id : calEvent.id}, function(data){
							
							if(data.success == 1){
								
								$('#reminder').fullCalendar('removeEvents', calEvent.id);
							}
							
						});
						
					}
				
				});
				
			}
			
	});
  $('#date_from').datetimepicker({
    
    format : 'YYYY-MM-DD'
  });
  $('#date_to').datetimepicker({
    useCurrent: false,
    format : 'YYYY-MM-DD'
  });
  $("#date_from").on("dp.change", function (e) {
      $('#date_to').data("DateTimePicker").minDate(e.date);
  });
  $("#date_to").on("dp.change", function (e) {
      $('#date_from').data("DateTimePicker").maxDate(e.date);
  });

    
   sidebarAccordion();
   
   $('[data-toggle="popover"]').popover({html:true});
    
   $('.markup').on('click',function(ev){
   
        var self = ev.currentTarget,
            val = $(self).find('.markup-value'),
            markup = $(val).val(),
            price = $('#wholesale_price').val();
       
       if(price.trim() != ''){
       
            var retail = parseFloat(price)+parseFloat(price*markup);
           
           $('#retail_price').val(retail.toFixed(2));
       }
   });
	
	$('.double-calc').on('click',function(){
		
    
		bootbox.dialog({
          message: $('#discount-calculator-form').html(),
          title: "Discount Calculator",
          size: 'small',
          buttons: {
            success: {
              label: "Select",
              className: "btn-success",
              callback: function() {
               $('.double-calc-val').val($('#new_price').val());
              }
            }
          }
    });
	});
	

    
    
   $('#datetimepicker12').datetimepicker({
        inline: true,
        sideBySide: true
   });
    
   $('#datetimepicker1').datetimepicker({
    language: 'pt-BR'
   });

   $('#alert-message-box').delay(10000).slideUp();
   $('.summernote').summernote({
   
       height:200
   });
    
   $('.date-input').datetimepicker({
       format : 'YYYY-MM-DD'
   });
    
    
   $('#expense-month').on('change',function(){
   
        $('#expense-form').submit();
       
   });
    
   $('#item-clear').click(clearForm);
    
    
   $('#add-expense-category').click(function(){
   
        bootbox.prompt({
          title: "Add New Expense Type",
          size: "small",
          callback: function(result) {
            if (result) {
                
               var url = "http://localhost/autoparts/public/expenses/saveType";
                
                $.post(url,{type:$('.bootbox-input').val(),_token:$('[name="_token"]').val()},function(data){
                    
                    var expense_option =  $('#expense-category').find('option');
                    expense_option.not(expense_option.eq(0)).remove();
                    
                    $.each(data, function(k, v) {
                        
                        $('#expense-category').append("<option value='"+v.id+"'>"+v.cname+"</option>");
                     
                    });
                });
            }
          }
        });
   
   });
    
    
    $('.save-invoic-btn').click(function(){
    
        if(validateInvoiceForm()){
        
            $('#new-invoice-frm').submit();
        }
    });
    
    
    $('.toggle-field').click(function(ev){
    
        
        var type = event.target.type;
        
        if(type == 'text') {
          
          var input_toggle = $('input:text.toggle-field');
       
          
          if($(this).hasClass('date-input')){
            
            input_toggle.not('.date-input').val('');


          }else{
            $('input:text.toggle-field').not(this).val('');

          }
            
            //$(".toggle-field").prop("selectedIndex", 0);
     
        }
        
        
         
    });
    
});

function computeDiscount(){
	
	var original_price = $('#original_price').val(),
			first_discount = ((100 - $('#first_discount').val()) / 100),
			second_discount = ((100- $('#second_discount').val()) / 100),
			new_price = 0;
		
		
			new_price = (original_price * first_discount) * second_discount;
			

			$('#new_price').val(new_price.toFixed(2));
	
}

function validateInvoiceForm(){

    var message = [],
        ctr = 0,
        errors = ''; 
    
    if(!validate.isEmpty($('#invoice-date').val())){
    
        ctr++;
        message.push('Date is required.');
    }
    
    if(!validate.isEmpty($('#invoice-number').val())){
        ctr++;
        message.push('Invoice is required.');
    }
    
    if($('.subtotal').length == 0){
    
        ctr++;
        message.push('Please add an item.');
        
    }else{
    
        $('.subtotal').each(function(k,v){
        
            if($(this).text() == ''){
                ctr++;
                message.push('Item information is incomplete.');
                
                return false;
            }
        });
        
    }
    
    
    if(ctr != 0){
        
        for(var i = 0; message.length > i; i++){
            
            
            errors+=message[i]+"\r";
        }
        
        alert(errors);
    
        return false;
    }
    
    return true;

}



function computeTotal(values){

    
    var total = 0;
    
    values.each(function(key,value){
        
        if($(this).text() != ""){
        
            total = total + parseFloat($(this).text());
        }
    });
    
    return total;
}
function confirmModal(msg,ev){
       
    bootbox.confirm({
        title:'Alert',
        size: 'small',
        message: msg, 
        callback: function(result){ 
            if(result){

                $(ev).closest('form').submit();

            }
        }
    });
}

function showOrderSlip(ev){
    
    var order_id =$(ev).attr('data-id'),
        url = $('#url').val()+'/getOrderSlip';
    
    $.get(url,{id:order_id},function(data){
        bootbox.dialog({
                title: "Order Slip",
                message: '<div class="row">  ' +
                    '<div class="col-md-12 order-slip-holder"><img> ' +data+
                   
                    '</div></div>',
                
            }
     );
    });
    
    
}


function uploadCsv(){

    bootbox.dialog({
          message: $('#upload-csv-form').html(),
          title: "Upload Csv",
          size: 'small',
          buttons: {
            success: {
              label: "Upload",
              className: "btn-success",
              callback: function() {
                $('#upload-csv-frm').submit();
              }
            }
          }
    });
}



function clearForm(){
    
    $('.clear-field').val('');
    $('.clear-field').prop('selectedIndex',0);
    
    location.replace($('#url').val());

}

function sidebarAccordion(){

    
    $('.list-group-dropdow').on('click',function(){
       
    
        var self = $(this),
            target = self.attr('data-target'),
            arrow = self.find('.dropdown-arrow');
      
        
        if($('#'+target).is(':hidden')){
            
            self.addClass('active');
            arrow.removeClass('fa-caret-down').addClass('fa-caret-up');
            $('#'+target).slideDown();
        }else{
            self.removeClass('active');
            arrow.addClass('fa-caret-down').removeClass('fa-caret-up');
            $('#'+target).slideUp();
        }
        
    });
}


function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
var validate = {


    'isEmpty' : function(field){
    
        if(field == ''){
            return true;
        }
        
        return false;
    }
};


var userList = angular.module("userList", []);
        
    userList.controller("userListCtrl", function($scope) {

        $scope.deleteUser = function(msg,id){


       }
    });    



var app = angular.module('main', ['ngTable','chart.js','ngSanitize'])
	.controller('pagingCtrl', function($scope, ngTableParams, tableData,$http) {

       //console.log($scope)
        var data = tableData.data;
        $scope.loading = true;
        $scope.id = 1;
        
        //console.log(tableData.loading);
       
        $scope.tableParams = new ngTableParams(
          {
            page: 1,            // show first page
            count: 10,           // count per page
            sorting: {name:'asc'}
          },
          {
            counts:[10,20],
            total: 0, // length of data
            getData: function($defer, params) {
              
              tableData.getData($defer,params,$scope.filter);
                   
            }
        });

        $scope.checkboxes = { 'checked': false, items: {} };

        $scope.$watch("checkboxes.checked",function(){
            if($scope.checkboxes.checked){
                $scope.showdelete = true;

                for(var i = 0; i < $('.toggle-checkboxes').length; i++){

                    $scope.checkboxes.items[i] = true;
                }
                //$('.toggle-checkboxes').prop('checked',true);
            }else{

                for(var i = 0; i < $('.toggle-checkboxes').length; i++){

                    $scope.checkboxes.items[i] = false;
                }
                $scope.showdelete = false;
            }
        });

        $scope.toggleCheckbox = function(i){

            var count = 0;
            if($scope.checkboxes.items[i]){
                $scope.showdelete = true;
            }else{

              if($('.toggle-checkboxes:checked').length == 0){
                 $scope.showdelete = false;
              }
            }

        }
        // watch for data checkboxes

        $scope.$watch("filter.$", function () {

           $scope.tableParams.reload();      

        });

        $scope.modal = {
          "title": "Delete",
          "content": "Delete this user?"
        };

        $scope.updateCurrentProductPrice = function(id,price){
          
          $http({
            
            method : "PUT",
            url : "http://localhost/autoparts/public/catalog/products/details/updateCurrentProductPrice",
            data : { id : id , price : price }
          }).then(function successCallback(response) {
            
            
            
            if(response.data.success == 1){
              
              location.reload();
            }else {
              
              alert("Something went wrong!");
            }
          }, function errorCallback(response) {
           
          });
        }
				
				$scope.updateCurrentProductSupplier = function(id,supplier_id){
          
          $http({
            
            method : "PUT",
            url : "http://localhost/autoparts/public/catalog/products/details/updateCurrentProductSupplier",
            data : { id : id , supplier_id : supplier_id }
          }).then(function successCallback(response) {
            
            
            
            if(response.data.success == 1){
              
              location.reload();
            }else {
              
              alert("Something went wrong!");
            }
          }, function errorCallback(response) {
           
          });
        }
   });


app.factory('modalFactory',function(){
	
	
	var confirm = function (msg,ev){

			bootbox.confirm({
					title:'Alert',
					size: 'small',
					message: msg, 
					callback: function(result){ 
							if(result){

									$(ev.target).closest('form').submit();

							}
					}
			});
	}
	
	var modal = {
		
		confirm : confirm
	}
	
	
	return modal;
	
});

app.factory('pcodeList', function($http){
  
  
  var getData = function(){
    
    return $http({
            
      method : "GET",
      url : "http://localhost/autoparts/public/catalog/products/getPcodeList",
    }).then(function successCallback(response) {
      
      return response.data;
    
      
    
    }, function errorCallback(response) {

    });
  }
  
  
  return {
    
    list : getData
  };
  
});
app.provider('tableData',function tableDataProvider(){

    this.url = "";

    this.setUrl = function(newUrl,trash){

        this.url = newUrl+"/getList/"+trash;
    }
    
    

    this.$get = function($http,$filter){

        var url = this.url;
    
        function filterData(data, filter){
           // console.log(filter);


            return $filter('filter')(data, filter)
        }

        function orderData(data, params){
            return params.sorting() ? $filter('orderBy')(data, params.orderBy()) : filteredData;
        }

        function sliceData(data, params){
            return data.slice((params.page() - 1) * params.count(), params.page() * params.count())
        }

        function transformData(data,filter,params){
            return sliceData( orderData( filterData(data,filter), params ), params);
        }

        var service = {
        cachedData:[],
 
        getData:function($defer, params, filter){


          if(service.cachedData.length>0){
            //console.log("using cached data")
            var filteredData = filterData(service.cachedData,filter);
            var transformedData = sliceData(orderData(filteredData,params),params);
            params.total(filteredData.length)
            $defer.resolve(transformedData);

            //console.log(filteredData);
          }
          else{
           // console.log("fetching data")
            $http.get(url).success(function(resp)
            {
              angular.copy(resp,service.cachedData)
              params.total(resp.length)
              var filteredData = $filter('filter')(resp, filter);
              var transformedData = transformData(resp,filter,params)

              $defer.resolve(transformedData);
             
             // console.log(service.loading);    
              
          
            }).error(function(data, status){
               // location.reload();
            });  
          }

        }
      };
      return service; 
    }
});


app.config(['tableDataProvider',function(tableDataProvider){
    tableDataProvider.setUrl($('#url').val(),$('#is_trashed').val());   
}]);
