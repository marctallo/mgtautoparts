(function(){
	
	'use strict';
	
	angular.module('main')
		.filter('concatInvoiceVal',function(){
		
		 function htmlDecode(input) {
        var e = document.createElement('div');
        e.innerHTML = input;
        return e.childNodes[0].nodeValue;
     }
  
			var output;

			return function(item){

				output = item.maker+" "+item.pcode+" ("+item.current_stock+") [ wholesale price = "+item.wholesale_price + "]";

				if(item.is_updated == 1){

					output = htmlDecode("&#10003 ")+output;

				}

				return output;
			}
		});
})();