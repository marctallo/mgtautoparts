(function(){
	
	'use strict';
	
	angular.module('main')
		.directive('tableLoading', function() { //this directive is a temporary hack change this

			return function(scope, element, attrs) {



					if (scope.$last){

						 // scope.loadingimage = true;
							$('.tableLoading').hide();
					}
				};
    })
		.directive('autoComplete',function(pcodeList){
  
  
		var list,
				pcodePromise = pcodeList.list(); 

		 return {


			link : function(scope, elem, attr){


				pcodePromise.then(function(result){

					elem.autocomplete({
						source : [result.data],
						limit : 10
					});

				});

			}
		}
	}).directive('datepicker', function() {
			return {

				restrict: 'A',
				// Always use along with an ng-model
				require: '?ngModel',

				link: function(scope, element, attrs, ngModel) {
					if (!ngModel) return;

					element.datetimepicker({
							format : 'YYYY-MM-DD'
					});      

					element.datetimepicker().on("dp.change",function(event){

							scope.$apply(function() {
								 ngModel.$setViewValue(element.val());
							});


					});
				}
			};
	})
	.directive('invoiceTable',function($timeout){
		
		return {
			restrict : "E",
			templateUrl : "http://localhost/autoparts/public/js/directives/templates/invoice-table.tpl",
				
		}
	})
})();