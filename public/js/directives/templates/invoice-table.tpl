<table class="table invoice-table">
	<thead>
		<tr>
			<th style="width:20%">Product</th>
			<th style="width:10%">Qty</th>
			<th style="width:15%">Unit Price</th>
			<th style="width:15%">Wholesale Price</th>
			<th style="width:5%">Stock</th>
			<th class="text-right">Sub Total</th>
			<th style="width:5%"></th>
		</tr>
	</thead>
	<tbody>
		<tr ng-repeat="item in inv.items">
			<td>
				<input type="text" class="form-control input-sm" ng-click="inv.showProducts(item)" ng-model="item.product" required readonly style="background:#FFF;">
				<input type="hidden" name="pid[]" class="invoice-item-id" ng-value="item.pid">
			</td>
			<td>
				<input type="text" class="form-control input-sm numeric" name="qty[]" disabled="true" ng-model="item.qty" disabled ng-disabled="!item.product" ng-blur="inv.checkQuantity(item)" required autocomplete="off">
			</td>
			<td>
				<input type="text" class="form-control input-sm numeric" name="price[]" disabled="true" ng-model="item.price" disabled ng-disabled="!item.product" ng-blur="inv.computeSubtotal(item)" required>
				
			</td>
			<td>
				<input type="text" class="form-control input-sm numeric" name="wholesale_price[]" disabled="true" ng-model="item.wholesale_price" disabled ng-disabled="!item.product" ng-blur="inv.computeSubtotal(item)" required>
			</td>
			<td>
				<span>{{item.current_stock}}</span>
			</td>
			<td class="text-right"><span class="subtotal">{{item.subtotal | currency:''}}</span></td>
			<td><i class="fa fa-close pointer text-danger" ng-click="inv.removeRow(item)"></i></td>
		</tr>
	</tbody>
</table>
<table class="table">
	<tbody>
		<tr>
			<td colspan="5" style="width:80%">
				<strong class="text-info pointer" ng-click="inv.addRow()">Add Item</strong>
				<input type="number" class="form-control input-sm numeric invoice-total-row" ng-model="inv.total_row" placeholder="rows">
			</td>
			<td><strong class="pull-right">Total:<span class="items-total">{{inv.total | currency:"&#8369"}}</span></strong></td>
			<td></td>
		</tr>
	</tbody>
</table>

<button type="submit" class="btn btn-success" ng-disabled="inv.validate()">Create Invoice</button>
<div id="invoice-product-choices" style="display:none;">
	<div class="row">
		<div class="col-md-12">
			<select size="10" class="form-control" ng-model="inv.categoryList" ng-options="item.id as item.cname for item in inv.categoryOptions" ng-click="inv.showItems()">
				<option value="">Please select a category</option>
			</select>
			<!-- {!! Form::select('cat_id', $category,null,array('class'=>'form-control','size'=>10,'id'=>'invoice-product-dropdown','ng-click'=>'showItems()','ng-model'=>'categoryId')) !!}-->
		</div>
		<div class="col-md-12">
			<!--<select size="10" class="form-control" id="invoice-product-list" ng-model="productList" ng-options="item as item.maker+' '+item.pcode for item in productOptions">-->
			<!--ng-options="item.id as item.pcode for item in productOptions"-->
			<select size="10" class="form-control" id="invoice-product-list" ng-model="inv.productList" ng-options="item as (item | concatInvoiceVal) for item in inv.productOptions">
				<option value="">Please select a product</option>

			</select>
		</div>
	</div>
</div>