(function(){
	
	'use strict';
	
	angular.module('main')
		.controller('DashboardController',DashboardController);
	
	function DashboardController($http){
		
		var vm = this;

			vm.daily_sales = {

				labels : ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday",""],
				series : ['Sales Revenue','Gross Sales'],
				data : [],
				colors : ['#3498DB','#E74C3C'],
				init : function(){

					$http({
						method: 'GET',
							url: 'dashboard/dailySales',
					}).then(function successCallback(response) {
								vm.daily_sales.data = response.data;
								//$scope.data.push(response.data);
					}, function errorCallback(response) {

					});
				}

			};

			vm.monthly_sales = {

				labels : ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December",""],
				series : ['Sales Revenue','Gross Sales'],
				data : [],
				colors : [{
						fillColor: '#3498DB',
						strokeColor: '#3498DB',
				},{
						fillColor: '#E74C3C',
						strokeColor: '#E74C3C',
				}],
				init : function(){

					$http({
							method: 'GET',
							url: 'dashboard/monthlySales',
					}).then(function successCallback(response) {
							vm.monthly_sales.data = response.data;
						 //$scope.data.push(response.data);
					}, function errorCallback(response) {

					});
				}
			},

			vm.run = function(){

				vm.daily_sales.init();
				vm.monthly_sales.init();
			}

			vm.run();
		
	}
	
})();



