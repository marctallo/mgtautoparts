(function(){
	
	'use strict';
	
	angular.module('main')
		.controller('UserController',UserController);
	
	function UserController($scope,ngTableParams, tableData,modalFactory){
		
		
		
		var vm = this;
		
			vm.tableParams = new ngTableParams(
				{
					page: 1,            // show first page
					count: 10,           // count per page
					sorting: {name:'asc'}
				},
				{
					counts:[10,20],
					total: 0, // length of data
					getData: function($defer, params) {

						tableData.getData($defer,params,vm.filter);

					}
			});
		
		vm.showModal = modalFactory.confirm;
		
		$scope.$watch("user.filter", function () { //used user instead of vm because scope.watch must us the exact alias which in this case in user(UserController as user)

           vm.tableParams.reload();      

    });
	}
})();