(function(){

'use strict';
	
angular.module('main')
	.controller("CarModelController",CarModelController);
	
	function CarModelController($scope,ngTableParams, tableData,modalFactory){
		
		
		
		var vm = this;
		
			vm.tableParams = new ngTableParams(
				{
					page: 1,            // show first page
					count: 10,           // count per page
					sorting: {name:'asc'}
				},
				{
					counts:[10,20],
					total: 0, // length of data
					getData: function($defer, params) {

						tableData.getData($defer,params,vm.filter);

					}
			});
		
		vm.showModal = modalFactory.confirm;
		
		$scope.$watch("cmodel.filter", function () { 
           vm.tableParams.reload();      

    });
	}
	
})();