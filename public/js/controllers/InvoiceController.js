(function () {

	'use strict';

	angular.module('main')
		.controller('InvoiceController', InvoiceController);

	function InvoiceController($scope, invoiceFactory, $compile) {

		var inv = this;
		var invoice = invoiceFactory;

		inv.items = [];
		inv.total = 0;
		inv.productList = null;
		inv.categoryList = null;
		inv.productOptions = [];
		inv.categoryOptions = [];
		inv.invoiceId = [];
		inv.date = '';
		inv.invoice_number = '';
		inv.addRow = addRow;
		inv.removeRow = removeRow;
		inv.computeTotal = computeTotal;
		inv.computeSubtotal = computeSubtotal;
		inv.validate = validate;
		inv.showProducts = showProducts;
		inv.showItems = showItems;
		inv.checkQuantity = checkQuantity;


		function addRow() {

			
			var item = function(){
				
				this.pid = '';
				this.product = '';
				this.qty = '';
				this.price = '';
				this.subtotal = '';
				this.valid = '';
				this.wholesale_price = '';
				this.current_stock = '';
			}
			

			//add item object to the inv.items collection
			if(inv.total_row > 1){
				
				for(var i =0; i < inv.total_row ; i++){

					inv.items.push(new item);
				}
				
				return;
			}
			
			inv.items.push(new item);

		}
		
		

		function removeRow(item) {

			//check if the item id exists
			if (item.pid != '') {

				//if exists remove id from the invoiceId collection
				var index = inv.invoiceId.indexOf(item.pid);
				inv.invoiceId.splice(index, 1);
			}

			//remove from the items collection
			inv.items.splice(inv.items.indexOf(item), 1);

			//recompute total
			inv.computeTotal();
		}

		function computeTotal() {

			//check if there are items in the table
			if (inv.items.length > 0) {
				inv.total = 0;

				//loop through the items and add each subtotal
				$.each(inv.items, function (key, value) {

					inv.total += parseFloat(value.subtotal)


				});

			}
		}
		
		
		function checkQuantity(item){
			
			/*if(item.qty > item.current_stock){
				
				bootbox.alert({
						title : "Alert",
						size : "small",
						message: "Quantity is greater than our current stock",
				});
				
				item.qty = 0;
			}*/ //removed to allow negative values for late restocking
			
			
			computeSubtotal(item);
			
		}

		function computeSubtotal(item) {

			var total = item.qty * item.price;

			if (isNaN(total)) {
				total = 0;
			}
			item.subtotal = total.toFixed(2);
			inv.computeTotal();

		}

		function validate() {

			var ctr = 0;

			//check if the invoice has items
			if (inv.items.length == 0) {

				ctr++;
			} else {

				$.each(inv.items, function (k, v) {
					//check if the items has price and total
					if (v.subtotal == '' || v.subtotal == 0 || v.wholesale_price == '' || v.wholesale_price == 0 || v.wholesale_price == undefined) {

						ctr++;

						return false;
					}
				});
			}

			//check if the required date and invoice number is not empty
			if (inv.date == '' || inv.invoice_number == '') {

				ctr++;
			}
			


			//check if there is no error
			if (ctr > 0) {

				return true;
			}
		}

		function showProducts(item) {


			//check if the list is empty
			if (inv.categoryList == null) {

				// if empty call the invoice factory and set the options 
				var category = invoice.getCategory();


				category.then(function successCallback(response) {
					inv.categoryOptions = response.data;
				});

			}

			//call the modal
			bootbox.confirm({
				title: 'Choose Product',
				message: $compile($('#invoice-product-choices').html())($scope), //bind the scope to the modal
				callback: function (result) {
					//if ok button is clicked
					if (result) {
						//check if the product list is not empty
						if (inv.productList != null) {
							//if not set the values of the item
							item.product = inv.productList.maker + " " + inv.productList.pcode;
							item.price = inv.productList.price;
							item.wholesale_price = inv.productList.wholesale_price;
							item.current_stock = inv.productList.current_stock;

							$(".numeric").numeric();
							//compute the subtotal 
							inv.computeSubtotal(item);

							//if id does not exist 
							if (item.pid == '') {

								item.pid = inv.productList.id;


							} else {

								//if id does exist check if it is in the invoiceId
								if ($.inArray((item.pid), inv.invoiceId) != -1) {

									//remove it from the invoice id and set the new id
									var index = inv.invoiceId.indexOf(item.pid);
									inv.invoiceId.splice(index, 1);
									item.pid = inv.productList.id;

								}
							}
							//add the id to the invoiceId collection
							inv.invoiceId.push(inv.productList.id);
						} else {
							//if product is not empty reset values
							if ($.inArray((item.pid), inv.invoiceId) != -1) {

								var index = inv.invoiceId.indexOf(item.pid);
								inv.invoiceId.splice(index, 1);

							}

							item.pid = '';
							item.qty = '';
							item.price = '';
							item.product = '';
							item.wholesale_price = '';
							item.current_stock = '';
						}

						inv.productOptions = [];
						$scope.$apply(); //apply the scope variables

					}
				}
			});
		}

		function showItems() {

			var list = $('#invoice-product-list');
			var products = invoice.getItems(inv.categoryList);

			inv.productOptions = [];
			//set the product option values
			products.then(function successCallback(response) {

				if (response.data != 0) {

					$.each(response.data, function (key, value) {
						
						
						inv.productOptions.push({
								'id': value.id,
								'maker': value.maker,
								'pcode': value.pcode,
								'price': value.retail_price,
								'wholesale_price': value.wholesale_price,
								current_stock: value.current_stock,
								'is_updated': value.is_updated
							});
					/*	if ($.inArray((value.id), inv.invoiceId) == -1) {


							inv.productOptions.push({
								'id': value.id,
								'maker': value.maker,
								'pcode': value.pcode,
								'price': value.retail_price,
								'wholesale_price': value.wholesale_price,
								current_stock: value.current_stock,
								'is_updated': value.is_updated
							});
						}*/

					});


				}
			});
		}

	}
})();