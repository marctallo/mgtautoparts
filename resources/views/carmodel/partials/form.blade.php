<div class="form-group">
<label for="cname">Car Model</label>


{!! Form::text('cmodel',null,['id'=>'cmodel','class'=>'form-control','placeholder'=>'Car Model']) !!}

@if($errors->first('cmodel'))


    <span class="text-danger"><small>{{ $errors->first('cmodel')}}</small></span>

@endif
</div>
<div class="form-group">
<label for="engine">Engine</label>


{!! Form::text('engine',null,['id'=>'engine','class'=>'form-control','placeholder'=>'Engine']) !!}


</div>
<div class="form-group">
<label for="chasis_type">Chasis Type</label>


{!! Form::text('chasis_type',null,['id'=>'chasis_type','class'=>'form-control','placeholder'=>'Chasis Type']) !!}


</div>
<div class="form-group">
<label for="year">Year</label>


{!! Form::text('year',null,['id'=>'year','class'=>'form-control','placeholder'=>'Year']) !!}


</div>
<div class="form-group">
  <label for="Desciption">Description</label>
  {!! Form::textarea('description',null,['class'=>'summernote'])!!}
</div>