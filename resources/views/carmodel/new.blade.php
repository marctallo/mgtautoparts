@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-bus"></i> New Car Model</h3>
   </div>
</div>
<div class="inner-box"> 

   {!! Form::open(['action'=>'CarModelController@store']) !!}
      @include('carmodel.partials.form')
      <button type="submit" class="btn btn-success">Save</button>
   {!! Form::close() !!}
</div>


@stop