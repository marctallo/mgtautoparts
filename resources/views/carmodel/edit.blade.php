@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-bus"></i> Update {{ $carmodel->cmodel }}</h3>
   </div>
</div>
<div class="inner-box"> 

   {!! Form::model($carmodel,['method'=>'patch','action'=>['CarModelController@update',$carmodel->id]]) !!}
      @include('carmodel.partials.form')
      <button type="submit" class="btn btn-success">Update</button>
   {!! Form::close() !!}
</div>


@stop