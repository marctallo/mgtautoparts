@include('layout.partials.header')

<body>
    <div class="main-wrapper">
        <header class="header">
           <nav class="navbar navbar-default navbar-fixed-top"  style="{{ (Config::get('database.connections.mysql.database') == 'autoparts_dummy') ? 'border-top:solid 6px pink' : '' }}" >
             
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#">AUTOPARTS</a>
                </div>
                <div class="collapse navbar-collapse" id="main-menu">
                 @if(showItem([config('role.admin'),config('role.dev'),config('role.encoder')]))
                     <ul class="nav navbar-nav navbar-left" title="quick add">
                          <li role="presentation" class="dropdown">
                             <a class="dropdown-toggle pointer" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus fa-lg"></i></a>
                              <ul class="dropdown-menu dropdown-menu-horizontal">
                                 @if(showItem([config('role.admin'),config('role.dev')]))
                                 <li><a href="{{action('UserController@create')}}"><i class="fa fa-user fa-fw"></i> User</a></li>
                                 @endif
                                 <li><a href="{{action('SupplierController@create')}}"><i class="fa fa-truck fa-fw"></i> Supplier</a></li>
                                 <li><a href="{{action('CategoryController@create')}}"><i class="fa fa-server fa-fw"></i> Category</a></li>
                                 <li><a href="{{action('CarModelController@create')}}"><i class="fa fa-bus fa-fw"></i> Car Model</a></li>
                                 <li><a href="{{action('ProductController@create')}}"><i class="fa fa-cog fa-fw"></i> Product</a></li>
                                  <li><a href="{{action('ProductRequestController@create')}}"><i class="fa fa-send-o fa-fw"></i> Requests</a></li>
                                 @if(showItem([config('role.admin'),config('role.dev')]))
                                 <li><a href="{{action('InvoiceController@create')}}"><i class="fa fa-line-chart fa-fw"></i> Invoice</a></li>
                                 <li><a href="{{action('ExpenseController@create')}}"><i class="fa fa-calendar fa-fw"></i> Expense</a></li>
                                 @endif
                              </ul>
                         </li>

                      </ul>
                 @endif
                  
                  
                  <ul class="nav navbar-nav navbar-right">
                      <!--<li role="presentation" class="dropdown">
                         <a class="dropdown-toggle pointer" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-envelope-o fa-lg"></i> <span class="badge">4</span></a>
                          <ul class="dropdown-menu">
                             <li><a>fdasfas</a></li>
                             <li><a>fdasfas</a></li>
                             <li><a>fdasfas</a></li>
                             <li><a>fdasfas</a></li>
                          </ul>
                     </li>-->
                      <li><a href="{{ action('LoginController@logout')}}"><i class="fa fa-sign-out fa-lg"></i> logout</a></li>
                   
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
            
            
        </header>
        <div class="main-content-wrapper container-fluid" ng-app="main" ng-cloak>
          <div class="row">
               @include('layout.partials.sidebar')
               <div class="main-content col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
                  @if(Session::has('alertmessage'))
                    
                        {!! Session::get('alertmessage')!!}
                   

                  @endif	
                   @yield('content')
               </div>
           </div>    
        </div>
        
    </div>
 @include('layout.partials.footer')