<aside class="sidebar col-md-2 col-sm-3">
   <div class="info-box">
       <!--{!! Html::image('img/defaults/1365608073_Akuma.png','Primary',['class'=>'img-circle'])!!}-->
       <h5>{{ \Auth::user()->fname." ".\Auth::user()->lname}}</h5>
       <em>last login: {{ \Auth::user()->last_login }}</em>
   </div>
   <div class="list-group">
      <a class="list-group-item {{ (\Request::segment(1)=='dashboard')?'active':''}}" href="{{ action('DashboardController@index')}}"><i class="fa fa-home fa-fw"></i>&nbsp; Dashboard</a>
      <a class="list-group-item {{ (\Request::segment(1)=='myaccount')?'active':''}}" href="{{ action('UserController@myAccount')}}"><i class="fa fa-user fa-fw"></i>&nbsp; My Account</a>
      @if(showItem([config('role.admin'),config('role.dev')]))
          <a class="list-group-item {{ (\Request::segment(1)=='users')?'active':''}}" href="{{ action('UserController@index')}}"><i class="fa fa-users fa-fw"></i>&nbsp; Users</a>
      @endif
      <a class="list-group-item {{ (\Request::segment(1)=='suppliers')?'active':''}}" href="{{ action('SupplierController@index')}}"><i class="fa fa-truck fa-fw"></i>&nbsp; Suppliers</a>
      <a class="list-group-item list-group-dropdow {{ (\Request::segment(1)=='catalog')?'active':''}}" href="#" data-target="catalog-dropdown"><i class="fa fa-car fa-fw"></i>&nbsp; Catalog <i class="fa fa-caret-down fa-fw pull-right dropdown-arrow"></i></a>
      <div class="sub-item {{ (\Request::segment(1)=='catalog')?'active':''}}" id="catalog-dropdown">
          <a class="list-group-item {{ (\Request::segment(2)=='categories')?'active':''}}" href="{{ action('CategoryController@index')}}">&nbsp; Categories</a>
          <a class="list-group-item {{ (\Request::segment(2)=='carmodel')?'active':''}}" href="{{ action('CarModelController@index')}}">&nbsp; Car Model</a>
          <a class="list-group-item {{ (\Request::segment(2)=='products')?'active':''}}" href="{{ action('ProductController@index')}}">&nbsp; Products</a>
          <a class="list-group-item {{ (\Request::segment(2)=='request')?'active':''}}" href="{{ action('ProductRequestController@index')}}">&nbsp; Requests</a>
      </div>
      
      @if(showItem([config('role.admin'),config('role.dev')]))
      <a class="list-group-item list-group-dropdow {{ (\Request::segment(1)=='sales')?'active':''}}" href="#" data-target="sales-dropdown"><i class="fa fa-line-chart fa-fw"></i>&nbsp; Sales <i class="fa fa-caret-down fa-fw pull-right dropdown-arrow"></i></a>
      <div class="sub-item {{ (\Request::segment(1)=='sales')?'active':''}}" id="sales-dropdown">
          <a class="list-group-item {{ (\Request::segment(2)=='invoice')?'active':''}}" href="{{ action('InvoiceController@index')}}">&nbsp; Invoice</a>
          <a class="list-group-item {{ (\Request::segment(2)=='returns')?'active':''}}" href="{{ action('ItemReturnController@index')}}">&nbsp; Item Return</a>
          <a class="list-group-item {{ (\Request::segment(2)=='invoice')?'active':''}}" href="{{ action('InvoiceController@index')}}">&nbsp; Orders</a>
          <!--<a class="list-group-item {{ (\Request::segment(2)=='categories')?'active':''}}" href="{{ action('CategoryController@index')}}">&nbsp; Orders</a>-->
      </div>
      <a class="list-group-item {{ (\Request::segment(1)=='expenses')?'active':''}}" href="{{ action('ExpenseController@index')}}"><i class="fa fa-calendar fa-fw"></i>&nbsp; Expenses</a>
      <a class="list-group-item list-group-dropdow {{ (\Request::segment(1)=='reports')?'active':''}}" href="#" data-target="report-dropdown"><i class="fa fa-bar-chart fa-fw"></i>&nbsp; Reports <i class="fa fa-caret-down fa-fw pull-right dropdown-arrow"></i></a>
      <div class="sub-item {{ (\Request::segment(1)=='reports')?'active':''}}" id="report-dropdown">
          <a class="list-group-item {{ (\Request::segment(2)=='income-statement')?'active':''}}" href="{{ action('IncomeStatementController@index')}}">&nbsp; Income Statement</a>
      </div>
      <a class="list-group-item" href="#"><i class="fa fa-book fa-fw"></i>&nbsp; Reference</a>
      <a class="list-group-item {{ (\Request::segment(1)=='settings')?'active':''}}" href="{{ action('SettingsController@index')}}"><i class="fa fa-cog fa-fw"></i>&nbsp; Settings</a>
      @endif
    </div>
</aside>
<div class="clearfix"></div>