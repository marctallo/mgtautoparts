  <!-- discount calculator template -->
  <script type="text/template" id="discount-calculator-form">
	<div class="calculator-form">
		 {!! Form::input('number','original_price',null,['id'=>'original_price','placeholder'=>'Original Price','class'=>'form-control currency' ,'onchange'=>'computeDiscount()'])!!}
		 <br>
		 {!! Form::input('number','first_discount',null,['id'=>'first_discount','placeholder'=>'1st Discount','class'=>'form-control currency discount-calc-field' , 'onchange'=>'computeDiscount()'])!!}
		 <br>
		 {!! Form::input('number','second_discount',null,['id'=>'second_discount','placeholder'=>'2nd Discount','class'=>'form-control currency discount-calc-field' ,'onchange'=>'computeDiscount()'])!!}
		 <br>
		 {!! Form::input('number','new_price',null,['id'=>'new_price','placeholder'=>'Price','class'=>'form-control currency'])!!}
	</div>
</script>
   <!-- end discount calculator template -->
   <footer class="footer">
        
    </footer>
     {!! Html::script('js/main.js') !!}
     {!! Html::script('js/services/invoiceService.js')!!}
     {!! Html::script('js/controllers/DashboardController.js')!!}
     {!! Html::script('js/controllers/UserController.js')!!}
     {!! Html::script('js/controllers/CategoryController.js')!!}
     {!! Html::script('js/controllers/CarModelController.js')!!}
     {!! Html::script('js/controllers/ProductRequestController.js')!!}
     {!! Html::script('js/controllers/InvoiceController.js')!!}
     {!! Html::script('js/directives/directives.js')!!}
     {!! Html::script('js/filters/filters.js')!!}
</body>

</html>