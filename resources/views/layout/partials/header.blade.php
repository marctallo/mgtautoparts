<!DOCTYPE html>
<html class="no-js" lang="">

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ ucwords(\Request::segment(1))}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
   
    {!! Html::style('bower_components/bootstrap/dist/css/bootstrap.css') !!}
    {!! Html::style('bower_components/summernote/dist/summernote.css') !!}
    {!! Html::style('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('css/font-awesome/css/font-awesome.css') !!}
    {!! Html::style('bower_components/angular-chart.js/dist/angular-chart.css') !!}
    {!! Html::style('css/fullcalendar.min.css') !!}
    {!! Html::style('css/jquery.autocomplete.css') !!}
    {!! Html::style('css/main.css') !!}
    {!! Html::script('bower_components/jquery/dist/jquery.min.js') !!}
    {!! Html::script('bower_components/angular/angular.min.js') !!}
    {!! Html::script('bower_components/angular-sanitize/angular-sanitize.min.js') !!}
    {!! Html::script('bower_components/angular/angular-resource.min.js') !!}
    {!! Html::script('bower_components/angular/angular-mocks.js') !!}
    {!! Html::script('bower_components/angular/ng-table.min.js') !!}
    {!! Html::script('bower_components/bootbox/bootbox.min.js') !!}
    {!! Html::script('bower_components/angular-strap/dist/angular-strap.min.js') !!}
    {!! Html::script('bower_components/bootstrap/dist/js/bootstrap.js') !!}
    {!! Html::script('bower_components/summernote/dist/summernote.js') !!}
    {!! Html::script('bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('js/jquery.numeric.min.js') !!}
    {!! Html::script('bower_components/Chart.js/Chart.min.js') !!}
    {!! Html::script('bower_components/angular-chart.js/dist/angular-chart.min.js') !!}
    {!! Html::script('js/jquery.autocomplete.js') !!}
    {!! Html::script('js/fullcalendar.min.js') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>