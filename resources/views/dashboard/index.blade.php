@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6">
       <h3 class="content-header"><i class="fa fa-home"></i> Dashboard</h3>
        <input type='hidden' id='url' value="{{\Request::url()}}">
   </div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading primary-color">
					<h3 class="panel-title primary-text"><i class="fa fa-calendar"></i> Reminders</h3>
			</div>
			<div id="reminder"></div>
		</div>
		
	</div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default primary-color dashboard-panel">
            <div class="panel-body">
                <h4>Today</h4>
                <h2>₱ {{number_format($sales['today']->total,2)}}</h2>
                <h5>Yesterday</h5>
                <h6>₱ {{number_format($sales['yesterday']->total,2)}}</h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-default success-color dashboard-panel">
            <div class="panel-body">
                <h4>This Week</h4>
                <h2>₱ {{number_format($sales['this_week']->total,2)}}</h2>
                <h5>Last Week</h5>
                <h6>₱ {{number_format($sales['last_week']->total,2)}}</h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-default warning-color dashboard-panel">
            <div class="panel-body">
                <h4>This Month</h4>
                <h2>₱ {{number_format($sales['this_month']->total,2)}}</h2>
                <h5>Last Month</h5>
                <h6>₱ {{number_format($sales['last_month']->total,2)}}</h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-default danger-color dashboard-panel">
            <div class="panel-body">
                <h4>This Year</h4>
                <h2>₱ {{number_format($sales['this_year']->total,2)}}</h2>
                <h5>Last Year</h5>
                <h6>₱ {{number_format($sales['last_year']->total,2)}}</h6>
            </div>
        </div>
    </div>  
</div>
<div class="row" ng-controller="DashboardController as dashboard">
    <div class="col-md-9">
        <div class="row">
            @{{ dashboard.test}}
            <div class="col-md-12">
               	
                  <div class="panel panel-default">
                      <div class="panel-heading primary-color">
                        <h3 class="panel-title primary-text"> <i class="fa fa-line-chart"></i> Daily Sales</h3>
                      </div>
                      <div class="panel-body">
                        <canvas id="line" class="chart chart-line" chart-data="dashboard.daily_sales.data" chart-labels="dashboard.daily_sales.labels" chart-legend="true"  chart-click="onClick" chart-series="dashboard.daily_sales.series" chart-colours="dashboard.daily_sales.colors">
                           </canvas> 
                        </div>
                 </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                      <div class="panel-heading primary-color">
                        <h3 class="panel-title primary-text"><i class="fa fa-bar-chart"></i> Monthly Sales</h3>
                      </div>
                      <div class="panel-body">
                        <canvas id="bar" class="chart chart-bar"
                          chart-data="dashboard.monthly_sales.data" chart-labels="dashboard.monthly_sales.labels" chart-legend="true" chart-series="dashboard.monthly_sales.series" chart-colours="dashboard.monthly_sales.colors">
                        </canvas>
                        </div>
                     </div>

               </div>
            </div>
        </div>
   
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading success-color">
                      <h3 class="panel-title primary-text"><i class="fa fa-toggle-up"></i> Top Selling Items</h3>
                  </div>
                  <ul class="list-group">
                  @if(count($top_items) > 0)

                          @foreach($top_items as $item)

                              <li class="list-group-item">
                                 <span class="badge">{{$item->qty}}</span>
                                  <a href="{{action('ProductController@getDetail',$item->id)}}">{{ $item->maker}} {{ $item->pcode}}</a>  
                              </li>
                          @endforeach

                  @else
                      <li class="list-group-item">
                          No data available
                      </li>
                  @endif
                  </ul> 
             </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading success-color">
                    <h3 class="panel-title primary-text"><i class="fa fa-toggle-down"></i> Low Stock Items</h3>
                  </div>
                  <ul class="list-group">
                  @if(count($critical_items) > 0)

                          @foreach($critical_items as $citem)

                              <li class="list-group-item">
                                 <span class="badge">{{$citem->current_stock}}</span>
                                  <a href="{{action('ProductController@getDetail',$citem->id)}}">{{ $citem->maker}} {{ $citem->pcode}}</a> 
                              </li>
                          @endforeach

                  @else
                      <li class="list-group-item">
                          No data available
                      </li>
                  @endif
                  </ul>
             </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                      <div class="panel-heading success-color">
                        <h3 class="panel-title primary-text">Item Request</h3>
                      </div>
                      <ul class="list-group">
                          @if(count($requested_items) > 0)

                                  @foreach($requested_items as $ri)

                                      <li class="list-group-item">
                                          <span class="badge">{{$ri->total}}</span>
                                          <a href="{{action('ProductRequestController@edit',$ri->id)}}">{{ $ri->product}}</a> 
                                      </li>
                                  @endforeach

                          @else
                              <li class="list-group-item">
                                  No data available
                              </li>
                          @endif
                     </ul>
                 </div>
            </div>
        </div>
      
    </div>
</div>

@stop