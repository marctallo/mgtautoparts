@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6">
       <h3 class="content-header"><i class="fa fa-server"></i> Requested Products</h3>
        <input type='hidden' id='url' value="{{\Request::url()}}">
        <input type='hidden' id='is_trashed' value="1">
   </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
       @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
           <a href="{{action('ProductRequestController@create')}}" class="btn btn-success new-btn btn-sm inner-btn" title="New Product Request">
             <i class="fa fa-plus tb-icon"></i>
           </a>
       @endif
        
        
    </div>
</div>
<div class="inner-box" ng-controller="ProductRequestController as prequest"> 
   <div class="row">
       <div class="col-md-4 col-md-offset-8 search-box">
           <div class="input-group">
              <input type="text" class="form-control" placeholder="search" ng-model="filter.$">

              <span class="input-group-btn">
                <button class="btn btn-default" type="submit">

                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </button>
              </span>
           </div>
       </div>
   </div>
    <table ng-table="prequest.tableParams"  class="table table-striped main-table table-hover">
      <thead class="pointer">
            <tr>
               <th style="width:5%;" class="sortable" ng-class="{
                    'sort-asc': prequest.tableParams.isSortBy('id', 'asc'),
                    'sort-desc': prequest.tableParams.isSortBy('id', 'desc')
                  }"
                    ng-click="prequest.tableParams.sorting({'id' : prequest.tableParams.isSortBy('id', 'asc') ? 'desc' : 'asc'})">
                    ID
                </th>
                <th style="width:85%;" class="sortable" ng-class="{
                    'sort-asc': prequest.tableParams.isSortBy('product', 'asc'),
                    'sort-desc': prequest.tableParams.isSortBy('product', 'desc')
                  }"
                    ng-click="prequest.tableParams.sorting({'product' : prequest.tableParams.isSortBy('product', 'asc') ? 'desc' : 'asc'})">
                    Category
                </th>
                <th style="width:10%;">Option</th>
            </tr>
       </thead>
       <tbody>
           <tr>
               <td colspan="3" class="text-center table-loader tableLoading" ng-hide="$data.length == 0">{!! Html::image('img/defaults/reload.gif')!!}</td>
           </tr>
            <tr class="tableLoading" ng-show="$data.length == 0">
                <td colspan="3" ><div class="text-center">no result found</div></td>
            </tr>
            <tr ng-repeat="request in $data" table-loading>
               <td data-title="'ID'" sortable="'id'">
                    @{{request.id}}
                </td>
                <td data-title="'Product'" sortable="'product'">
                    @{{request.product}}
                </td>
                <td>
                  <form method="POST" action="@{{'request/'+request.id}}" accept-charset="UTF-8" class="form-delete">
                   <input name="_method" type="hidden" value="DELETE">
                   <input name="_token" type="hidden" value="{{ csrf_token() }}">
                   @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
                       <a href="@{{'request/'+request.id+'/edit'}}" title="Edit"><i class="fa fa-edit tb-icon"></i></a>
                       <i class="fa fa-trash-o tb-icon delete-btn" ng-click="prequest.showModal('Delete this request?',$event)" title="Move to thrash"></i>
                   @endif
                  </form>   
                   
               </td>
            </tr>
           
       </tbody>
        
   </table>

</div>


@stop