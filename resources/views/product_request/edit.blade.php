@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-truck"></i> Update {{ $request->product }}</h3>
   </div>
</div>
<div class="inner-box"> 

   {!! Form::model($request,['method'=>'patch','action'=>['ProductRequestController@update',$request->id]]) !!}
      @include('product_request.partials.form')
      <button type="submit" class="btn btn-success">Update</button>
   {!! Form::close() !!}
</div>


@stop