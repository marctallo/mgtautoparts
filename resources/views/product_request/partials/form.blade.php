<div class="form-group">
<label for="cname">Product</label>


{!! Form::text('product',null,['id'=>'product','class'=>'form-control','placeholder'=>'Product']) !!}

@if($errors->first('product'))


    <span class="text-danger"><small>{{ $errors->first('product')}}</small></span>

@endif
</div>
<div class="form-group">
  <label for="Desciption">Description</label>
  {!! Form::textarea('description',null,['class'=>'summernote'])!!}
</div>