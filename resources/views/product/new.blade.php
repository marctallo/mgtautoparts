@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-cog"></i> New Product</h3>
   </div>
</div>
<div class="inner-box"> 

   {!! Form::open(['action'=>'ProductController@store','files'=>true]) !!}
      @include('product.partials.form')
      <button type="submit" class="btn btn-success">Save</button>
   {!! Form::close() !!}
</div>


@stop