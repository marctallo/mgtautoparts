@extends('layout.main')




@section('content')


<div class="inner-header row"> 
   <div class="col-md-6">
       <h3 class="content-header"><i class="fa fa-cog"></i> Low Stock Items</h3>
       <input type='hidden' id='url' value="{{\Request::url()}}">
   </div>
   <div class="col-md-6">
   		<a href="{{action('ProductController@printLowStockItems')}}" class="btn btn-success new-btn btn-sm inner-btn" title="Print Page" target="_blank">
             <i class="fa fa-print tb-icon"></i>
       </a>
	 </div>
</div>
<div class="inner-box"> 
    {!! Form::open(['method'=>'get']) !!}
    <div class="row">
    		<div class="col-md-3">
            <div class="form-group">
                <label for="category">Product Type</label>
                {!! Form::select('cat_id', $category,\Request::input('cat_id'),array('class'=>'form-control clear-field')) !!}
            </div>

        </div><div class="col-md-3">
            <div class="form-group">
                <label for="category">Supplier</label>
                {!! Form::select('supplier_id', $supplier,\Request::input('supplier_id'),array('class'=>'form-control clear-field')) !!}
            </div>

        </div>
        
        <div class="col-md-2">
            <div class="form-group">
                <label for="pcode">Maker</label>
                {!! Form::text('maker',\Request::input('maker'),['id'=>'maker','class'=>'form-control clear-field','placeholder'=>'Maker']) !!}
            </div>

        </div>
        <div class="col-md-4">
               <button class="btn btn-info" id="item-search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search Item</button>
               
               <button class="btn" id="item-clear" type="button"> Clear Search</button>
        
        </div>
    </div>
    {!! Form::close() !!}
    <hr>
    <table class="table table-striped main-table table-hover product-table">
        <thead>
            <tr>
                <!--<th style="width:10%">Image</th>-->
                <th style="width:20%">{!! sort_header('cat_id','Type',$sort_fields,'ProductController@showLowStockItems') !!}</th>
                <th style="width:20%">{!! sort_header('pcode','Product Code',$sort_fields,'ProductController@showLowStockItems') !!}</th>
                <th style="width:20%">{!! sort_header('maker','Maker',$sort_fields,'ProductController@showLowStockItems') !!}</th>
                <th style="width:20%">{!! sort_header('supplier_name','Supplier',$sort_fields,'ProductController@showLowStockItems') !!}</th>
                <th style="width:10%">{!! sort_header('current_stock','Stock',$sort_fields,'ProductController@showLowStockItems') !!}</th>
            </tr>
        </thead>
        <tbody>
           
            
            @if(count($products) > 0)
                @foreach($products as $product)
                   <tr>
                        <td>{{ $product->cname}}</td>
                        <td>
                        	<a href="#" data-toggle="popover" title="Application" data-content="{{ $product->application }}" data-trigger="focus">{{ $product->pcode}}</a>
                        	@if($product->is_updated == 1)
                        		<i class="fa fa-check updated"></i>
                        	@endif	
                        </td>
                        <td>{{ $product->maker}}</td>
                        <td>{{ $product->supplier_name }}</td>
                        <td>{!! display_stock_status($product->current_stock,$product->reorder_point) !!}</td>
                   </tr>
                @endforeach
             @else
             
                 <tr>
                     <td colspan="5">
                         <p style="text-align:center;">no results found</p>
                     </td>
                 </tr>
             @endif             
        </tbody>
    </table>
    <p>{{ $item_count }} item(s) found</p>
    {!! show_pagination_links($products,$pagination_fields) !!}

</div>




@stop