@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6 {{ (\Auth::user()->role == 3)?'product-header':''}}">
       <h3 class="content-header"><i class="fa fa-cog"></i> Products</h3>
   </div>
    <div class="col-md-6 col-sm-6 col-xs-6 {{ (\Auth::user()->role == 3)?'product-button':''}}">
       
       @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
           <a href="{{action('ProductController@showLowStockItems')}}" class="btn btn-warning new-btn btn-sm inner-btn" title="Low Stock Items">
             <i class="fa fa-exclamation-circle tb-icon"></i>
           </a>
       @endif
       @if(showItem([config('role.dev')]))
           <a href="{{action('ProductController@showTrash')}}" class="btn btn-danger new-btn btn-sm inner-btn" title="View Trash">
               <i class="fa fa-trash tb-icon"></i>
           </a>
           
           @if(\Request::input('cat_id') || \Request::input('cat_id') != 0)
           <a href="{{action('ProductController@downloadCsvList')}}?cat_id={{\Request::input('cat_id')}}&pcode={{\Request::input('pcode')}}&application={{\Request::input('application')}}&maker={{\Request::input('maker')}}&year={{\Request::input('year')}}" class="btn btn-success new-btn btn-sm inner-btn" title="Download Price Guide">
               <i class="fa fa-download tb-icon"></i>
           </a>
           @endif
           <button type="button" class="btn btn-success new-btn btn-sm inner-btn" id="upload-csv" onclick="uploadCsv()" title="Upload CSV">
               <i class="fa fa-upload tb-icon"></i>
           </button>
           
        @endif    
        @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
           <a href="{{action('ProductController@create')}}" class="btn btn-success new-btn btn-sm inner-btn" title="New Product">
             <i class="fa fa-plus tb-icon"></i>
           </a>
        @endif
        <input type='hidden' id='url' value="{{\Request::url()}}">
    </div>
</div>
<div class="inner-box"> 
    {!! Form::open(['method'=>'get']) !!}
    <div class="row">
    
        <div class="col-md-4">
            <div class="form-group">
                <label for="category">Product Code</label>
                {!! Form::text('pcode',\Request::input('pcode'),['id'=>'pcode','class'=>'form-control clear-field','placeholder'=>'Product Code','auto-complete','autocomplete'=>'off']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="category">Product Type</label>
                {!! Form::select('cat_id', $category,\Request::input('cat_id'),array('class'=>'form-control clear-field')) !!}
            </div>

        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="pcode">Application</label>
                {!! Form::text('application',\Request::input('application'),['id'=>'application','class'=>'form-control clear-field','placeholder'=>'Application']) !!}
            </div>

        </div>
    </div>
    <div class="row">
    
        <div class="col-md-4">
            <div class="form-group">
                <label for="maker">Maker</label>
                {!! Form::text('maker',\Request::input('maker'),['id'=>'maker','class'=>'form-control clear-field','placeholder'=>'Maker']) !!}
            </div>
        </div>
      <!--  <div class="col-md-4">
            <div class="form-group">
                <label for="maker">Year</label>
                {!! Form::text('year',\Request::input('year'),['id'=>'year','class'=>'form-control clear-field','placeholder'=>'Year']) !!}
            </div>

        </div>-->
        <div class="col-md-4">
               <button class="btn btn-info" id="item-search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search Item</button>
               
               <button class="btn" id="item-clear" type="button"> Clear Search</button>
        
        </div>
    </div>
    {!! Form::close() !!}
    <hr>
    <table class="table table-striped main-table table-hover product-table">
        <thead>
            <tr>
                <!--<th style="width:10%">Image</th>-->
                <th style="width:13%">{!! sort_header('cat_id','Type',$sort_fields,'ProductController@index') !!}</th>
                <th style="width:10%">{!! sort_header('pname','Name',$sort_fields,'ProductController@index') !!}</th>
                <th style="width:12%">{!! sort_header('pcode','Product Code',$sort_fields,'ProductController@index') !!}</th>
                <th style="width:10%">{!! sort_header('maker','Maker',$sort_fields,'ProductController@index') !!}</th>
                <th style="width:7.5%">{!! sort_header('retail_price','Price',$sort_fields,'ProductController@index') !!}</th>
                <th style="width:7.5%">{!! sort_header('wholesale_price','Wholesale Price',$sort_fields,'ProductController@index') !!}</th>
                <th style="width:15%">{!! sort_header('application','Application',$sort_fields,'ProductController@index') !!}</th>
                <!--<th style="width:10%">{!! sort_header('year','Year',$sort_fields,'ProductController@index') !!}</th>-->
                <th style="width:5%">{!! sort_header('current_stock','Stock',$sort_fields,'ProductController@index') !!}</th>
                <th style="width:10%">Option</th>
            </tr>
        </thead>
        <tbody>
           
            
            @if(count($products) > 0)
                @foreach($products as $product)
                   <tr>
                        <!--<td>
                           @if($product->image_file != null)
                             {!! Html::image('img/products/'.$product->image_file)!!}
                           @else
                             {!! Html::image('img/defaults/product.jpg')!!}
                           @endif        
                        </td>-->
                        <td>{{ $product->cname}}</td>
                        <td>{{ $product->pname}}</td>
                        <td>
                        	{{ $product->pcode}}
                        	@if($product->is_updated == 1)
                        		<i class="fa fa-check updated"></i>
                        	@endif
                        
                        </td>
                        <td>{{ $product->maker}}</td>
                        <td>{{ number_format($product->retail_price,2,'.',',') }}</td>
                        <td>{{ number_format($product->wholesale_price,2,'.',',') }}</td>
                        <td>{{ $product->application }}</td>
               <!--         <td>{{ $product->year}}</td>-->
                        <td>
                        	{!! display_stock_status($product->current_stock,$product->reorder_point) !!}
                        	
                        </td>
                        <td>
                            <form method="POST" action="{{'products/'.$product->id}}" accept-charset="UTF-8" class="form-delete">
                               <input name="_method" type="hidden" value="DELETE">
                               <input name="_token" type="hidden" value="{{ csrf_token() }}">
                               @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
                                   <a href="{{ action('ProductController@edit',$product->id)}}" title="Edit"><i class="fa fa-edit tb-icon"></i></a>
                                   <a href="{{ action('ProductController@addStock',$product->id)}}" title="Restock"><i class="fa fa-refresh tb-icon"></i></a>
                                   <i class="fa fa-trash-o tb-icon delete-btn" onclick="confirmModal('Delete this product? ',this)" title="Move to thrash"></i>
                               @endif             
                               
                               <a href="{{ action('ProductController@getDetail',$product->id)}}" title="View Details"><i class="fa fa-info tb-icon"></i></a>
                               <i class="fa fa-file-text-o tb-icon" data-toggle="popover" data-trigger="focus" tabindex="0" title="Note" data-content="{{ $product->description }}" data-placement="top"></i>
                              </form>  
                        </td>

                   </tr>
                @endforeach
             @else
             
                 <tr>
                     <td colspan="11">
                         <p style="text-align:center;">no results found</p>
                     </td>
                 </tr>
             @endif             
        </tbody>
    </table>
    <p>{{ $item_count }} item(s) found</p>
    {!! show_pagination_links($products,$pagination_fields) !!}
   
   
</div>
<script type="text/template" id="upload-csv-form">
    <form action="{{action('ProductController@uploadCsv')}}" method="post" enctype="multipart/form-data" id="upload-csv-frm">
        <label>Please choose a csv file to upload</label>
        <br>
        <input type="file" name="csvfile" id="csvfile">
        {!! Form::token() !!}
        
    </form>

</script>



@stop