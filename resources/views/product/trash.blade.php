@extends('layout.main')




@section('content')


<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-cog"></i> Trashed</h3>
       <input type='hidden' id='url' value="{{\Request::url()}}">
   </div>
</div>
<div class="inner-box"> 
    {!! Form::open(['method'=>'get']) !!}
    <div class="row">
    
        <div class="col-md-4">
            <div class="form-group">
                <label for="category">Product Code</label>
                {!! Form::text('pcode',\Request::input('pcode'),['id'=>'pcode','class'=>'form-control clear-field','placeholder'=>'Product Code']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="category">Product Type</label>
                {!! Form::select('cat_id', $category,\Request::input('cat_id'),array('class'=>'form-control clear-field')) !!}
            </div>

        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="pcode">Application</label>
                {!! Form::text('application',\Request::input('application'),['id'=>'application','class'=>'form-control clear-field','placeholder'=>'Application']) !!}
            </div>

        </div>
    </div>
    <div class="row">
    
        <div class="col-md-4">
            <div class="form-group">
                <label for="maker">Maker</label>
                {!! Form::text('maker',\Request::input('maker'),['id'=>'maker','class'=>'form-control clear-field','placeholder'=>'Maker']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="maker">Year</label>
                {!! Form::text('year',\Request::input('year'),['id'=>'year','class'=>'form-control clear-field','placeholder'=>'Year']) !!}
            </div>

        </div>
        <div class="col-md-4">
               <button class="btn btn-info" id="item-search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search Item</button>
               
               <button class="btn" id="item-clear" type="button"> Clear Search</button>
        
        </div>
    </div>
    {!! Form::close() !!}
    <hr>
    <table class="table table-striped main-table table-hover product-table">
        <thead>
            <tr>
                <!--<th style="width:10%">Image</th>-->
                <th style="width:13%">{!! sort_header('cat_id','Type',$sort_fields,'ProductController@showTrash') !!}</th>
                <th style="width:10%">{!! sort_header('pname','Name',$sort_fields,'ProductController@showTrash') !!}</th>
                <th style="width:12%">{!! sort_header('pcode','Product Code',$sort_fields,'ProductController@showTrash') !!}</th>
                <th style="width:10%">{!! sort_header('maker','Maker',$sort_fields,'ProductController@showTrash') !!}</th>
                <th style="width:7.5%">{!! sort_header('retail_price','Price',$sort_fields,'ProductController@showTrash') !!}</th>
                <th style="width:7.5%">{!! sort_header('wholesale_price','Wholesale Price',$sort_fields,'ProductController@showTrash') !!}</th>
                <th style="width:15%">{!! sort_header('application','Application',$sort_fields,'ProductController@showTrash') !!}</th>
                <th style="width:10%">{!! sort_header('year','Year',$sort_fields,'ProductController@showTrash') !!}</th>
                <th style="width:5%">{!! sort_header('current_stock','Stock',$sort_fields,'ProductController@showTrash') !!}</th>
                <th style="width:10%">Option</th>
            </tr>
        </thead>
        <tbody>
           
            
            @if(count($products) > 0)
                @foreach($products as $product)
                   <tr>
                        <!--<td>
                           @if($product->image_file != null)
                             {!! Html::image('img/products/'.$product->image_file)!!}
                           @else
                             {!! Html::image('img/defaults/product.jpg')!!}
                           @endif        
                        </td>-->
                        <td>{{ $product->cname}}</td>
                        <td>{{ $product->pname}}</td>
                        <td>{{ $product->pcode}}</td>
                        <td>{{ $product->maker}}</td>
                        <td>{{ number_format($product->retail_price,2,'.',',') }}</td>
                        <td>{{ number_format($product->wholesale_price,2,'.',',') }}</td>
                        <td>{{ $product->application}}</td>
                        <td>{{ $product->year}}</td>
                        <td>{!! display_stock_status($product->current_stock,$product->reorder_point) !!}</td>
                        <td>
                            <form method="POST" action="{{ action('ProductController@restoreTrashed',$product->id )}}" accept-charset="UTF-8" class="form-delete inline-blocked">
                               <input name="_method" type="hidden" value="PUT">
                               <input name="_token" type="hidden" value="{{ csrf_token() }}">   
                               <i class="fa fa-rotate-right tb-icon" onclick="confirmModal('Restore this product?',this)" title="Restore Product"></i>
                            </form>
                            <form method="POST" action="{{ action('ProductController@permanentDelete',$product->id )}}" accept-charset="UTF-8" class="form-delete inline-blocked left-margin-5">
                               <input name="_method" type="hidden" value="DELETE">
                               <input name="_token" type="hidden" value="{{ csrf_token() }}">   
                               <i class="fa fa-trash tb-icon delete-btn" onclick="confirmModal('Permanently delete this product?',this)" title="Permanent Delete"></i>
                            </form>    
                        </td>

                   </tr>
                @endforeach
             @else
             
                 <tr>
                     <td colspan="10">
                         <p style="text-align:center;">no results found</p>
                     </td>
                 </tr>
             @endif             
        </tbody>
    </table>
    
    {!! show_pagination_links($products,$pagination_fields) !!}

</div>




@stop