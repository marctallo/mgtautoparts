@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-cog"></i> Restock Product : {{ $product->pcode }}</h3>
   </div>
</div>
<div class="inner-box"> 
   
   {!! Form::open(['action'=>'ProductController@saveStock','files'=>true]) !!}
   {!! Form::hidden('product_id',$id,['id'=>'product_id','class'=>'form-control'])!!}     
     <div class="row">
          <div class="col-md-2">
              <div class="form-group">
                <label for="unit">Quantity</label>
                {!! Form::input('number','qty',null,['id'=>'qty','placeholder'=>'Qty','class'=>'form-control number','min'=>'0'])!!}
                @if($errors->first('qty'))


                    <span class="text-danger"><small>{{ $errors->first('qty')}}</small></span>

                @endif
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                <label for="unit">Price</label>
                <div class="input-group">
                	<div class="input-group-addon">
										<span class="double-calc">%</span>
									</div>
                {!! Form::input('number','price',null,['placeholder'=>'Price','class'=>'form-control number double-calc-val','min'=>'0'])!!}
								</div>
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                <label for="unit">Date Purchased</label>
                {!! Form::text('pdate',null,['id'=>'pdate','class'=>'form-control date-input'])!!}
                @if($errors->first('pdate'))


                    <span class="text-danger"><small>{{ $errors->first('pdate')}}</small></span>

                @endif
              </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                <label for="unit">Supplier</label>
                {!! Form::select('supplier_id', $supplier,null,array('class'=>'form-control')) !!} 
                @if($errors->first('supplier_id'))


                    <span class="text-danger"><small>{{ $errors->first('supplier_id')}}</small></span>

                @endif
              </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                <label for="unit">Order Number</label>
                {!! Form::text('ornumber',null,['id'=>'ornumber','placeholder'=>'Order Number','class'=>'form-control'])!!}
                @if($errors->first('ornumber'))


                    <span class="text-danger"><small>{{ $errors->first('ornumber')}}</small></span>

                @endif
              </div>
          </div>

    </div>
    <div class="form-group">
      <label for="Desciption">Note</label>
      {!! Form::textarea('note',null,['class'=>'summernote'])!!}
    </div>
    <div class="row">
         <div class="col-md-4">
              <div class="form-group">
                    <label for="price">Scanned Order Slip <small class="text-success">(jpg,bmp,png,gif only)</small></label>
                    {!! Form::file('order_img_file'); !!}
                    @if($errors->first('order_img_file'))


                            <span class="text-danger"><small>{{ $errors->first('order_img_file')}}</small></span>

                    @endif

                </div>
          </div>
          <div class="col-md-2">
						 <div class="checkbox">
							<label>{!! Form::checkbox('update_price',null,false,['id' => 'update_price','class'=>'checkbox-styled']) !!} <span> Update Price</span></label>
						</div>
				</div>
				<div class="col-md-2">
				 <div class="checkbox">
					<label>{!! Form::checkbox('update_supplier',null,false,['id' => 'update_supplier','class'=>'checkbox-styled']) !!} <span> Update Supplier</span></label>
				</div>
				</div>    
    </div>     
      <button type="button" class="btn btn-success" onclick="confirmModal('Add stock to this product?',this)">Restock</button>
   {!! Form::close() !!}
</div>


@stop