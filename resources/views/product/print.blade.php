<!DOCTYPE html>
<html lang="en">
	<head>

		<title>Low Stock Items</title>

		<style>
		
			body {
				font: normal 12px/150% Arial, Helvetica, sans-serif;
			}
			table{
				
				width: 100%;
			}
			
			table img {
				
				width: 248px;
				height: 46px;
					
			}
			.datagrid table {
				border-collapse: collapse;
				text-align: left;
				width: 100%;
			}
			.datagrid {
				font: normal 12px/150% Arial, Helvetica, sans-serif;
				background: #fff;
				overflow: hidden;
				border: 1px solid #999999;
			}
			.datagrid table td, .datagrid table th {
				padding: 6px 10px;
				text-align: left;
			}
			.datagrid table thead th {
				background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );
				background: -moz-linear-gradient( center top, #006699 5%, #00557F 100% );
				filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');
				background-color: #ef5031;
				color: #FFFFFF;
				font-size: 12px;
				font-weight: bold;

			}
	
			.datagrid table tbody td {
				color: #00496B;
				border: 1px solid #E1EEF4;
				font-size: 12px;
				font-weight: normal;
			}
	
			
			.footer{
				
				 position: fixed;
    			 bottom: 40;
    			 width: 100%;
			}
			
			.invoice-company-info {
				
				margin:0;
				padding:0;
				font-size: 12px;
				font-weight: normal;
				
				color: #09001C;
			}
			.footer-message {
				
				font-weight: bold;
				font-size: 15px;
				color:#7C959C;
			}
			
			.top-border {
				
				border-top: solid 1px #E1EEF4;
			}

		</style>

	</head>

	<body>
		<div class="invoice-template">
			<table>
				<thead>
					<tr>
						<td>
							{!! Html::image('img/defaults/logo.png')!!}
							
						</td>
						<td>&nbsp;</td>
						<td align="right" style="color: #333333"><h2>LOW STOCK ITEMS</h2>
						</td>
					</tr>
					<tr>
						<td>
							
							<p class="invoice-company-info">{{ $settings->company_name }}</p>
							<p class="invoice-company-info">{{ $settings->address }}l</p>
							<p class="invoice-company-info">{{ $settings->number }}</p>
							<p class="invoice-company-info">{{ $settings->email }}</p>
						</td>
						<td>
                            
						</td>
						<td align="right" style="color: #333333">
						<p>
							<strong>Date</strong> {{ date("m-d-Y") }}
						</p></td>
					</tr>
					<tr>
						<td colspan="3">
						<hr style="color: #cccccc">
						</td>
					</tr>
				</thead>

			</table>
			<br>
			<div class="datagrid">
				<table>
					<thead>
						<tr>
							<th style="width: 20%">Type</th>
							<th style="width: 20%">Product Code</th>
							<th style="width: 20%">Maker</th>
							<th style="width: 20%">Supplier</th>
							<th style="width: 10%">Stock</th>
						</tr>
					</thead>
					<tbody>
						
							@if(count($products) > 0)
								@foreach($products as $product)
										<tr>
											<td>{{ $product->cname}}</td>
											<td>{{ $product->pcode}}</td>
											<td>{{ $product->maker }}</td>
											<td>{{ $product->supplier_name }}</td>
											<td>{{ $product->current_stock }}</td>
										</tr>
								@endforeach
									<tr>
										<td colspan="5"><p>Total Items: {{ count($products) }}</p></td>
									</tr>
							
							@else
							
								<tr>
									<p style="text-align:center;">no results found</p>
								</tr>
							
							@endif
						
							
					</tbody>
				</table>
				
			</div>
			
		</div>
		<div class="footer">
			<table>
				<tr>
					<td>
						
					</td>
					<td align="right">
						
						<p class="footer-message"></p>
					</td>
				</tr>
			</table>
			
		</div>
	</body>

</html>
