@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6">
       <h3 class="content-header"><i class="fa fa-cog"></i> Product Details</h3>
        <input type='hidden' id='url' value="{{\Request::url()}}">
    
   </div>
</div>
<div class="inner-box product-detail-view" ng-controller="pagingCtrl">
   <div class="row">
       <div class="col-md-4">
          @if($product->image_file != null)
             {!! Html::image('img/products/'.$product->image_file,'product image',['class'=>'product-image'])!!}
           @else
             {!! Html::image('img/defaults/product.jpg','product image',['class'=>'product-image'])!!}
           @endif  
           
       </div>
       <div class="col-md-6">
           <h3>
               @if($product->pname != "" || $product->pname != null)
                   {{ $product->pname }}
               @else
                   {{ $product->cname }}
               @endif             
               
           </h3>
           <p class="label label-info">{{ $product->cname }}</p>
           <p>{{ $product->pcode }}</p>
           <small>Updated last:: {!! $product->updated_at !!}</small>
           <br>
           <br>
           <form method="POST" action="{{ action('ProductController@destroy',$product->id) }}" accept-charset="UTF-8" class="form-delete">
               <input name="_method" type="hidden" value="DELETE">
               <input name="_token" type="hidden" value="{{ csrf_token() }}">
               
               @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
                   <a class="btn btn-success btn-sm" href=" {{ action('ProductController@edit',$product->id) }}" title="Edit">Edit <i class="fa fa-edit"></i></a>
                   <a class="btn btn-info btn-sm" href=" {{ action('ProductController@addStock',$product->id) }}" title="Restock">Restock <i class="fa fa-refresh"></i></a>
                   <button type="button" class="btn btn-danger btn-sm" onclick="confirmModal('Delete this product?',this)" title="Move to thrash">Delete <i class="fa fa-trash-o"></i></button>
               @endif
                
          </form>            
       </div>


   </div>
   <hr>     
   <div class="row">
       <div class="col-md-12">
           <div>

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#product-info" aria-controls="product-info" role="tab" data-toggle="tab">Product Info</a></li>
                <li role="presentation"><a href="#stock-info" aria-controls="stock-info" role="tab" data-toggle="tab">Stock Info</a></li>
                <li role="presentation"><a href="#pricing" aria-controls="pricing" role="tab" data-toggle="tab">Pricing</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="product-info">
                    <label class="pull-left">Maker: </label> <p class="product-data">{!! $product->maker !!} &nbsp;</p>
                    <label class="pull-left">Application: </label> <p class="product-data">{!! $product->application !!} &nbsp;</p>
                    <label >Description </label> <p>{!! $product->description !!}</p>
                    <label >Added </label> <p>{!! $product->created_at !!} </p>
                    <label >Modified </label> <p>{!! $product->updated_at !!} </p>
                    


                </div>
                <div role="tabpanel" class="tab-pane fade row" id="stock-info">
                    <div class="col-md-4">
                         <label class="pull-left">Unit: </label> <p class="product-data">{!! $product->unit !!} &nbsp;</p>
                         <label class="pull-left">Section: </label> <p class="product-data">{!! $product->section !!} &nbsp;</p>
                         <label class="pull-left">Shelf: </label> <p class="product-data">{!! $product->shelf !!} &nbsp;</p>
                         <label class="pull-left">Bin Location: </label> <p class="product-data">{!! $product->binloc !!} &nbsp;</p>
                    </div>
                    <div class="col-md-4">
                         <label class="pull-left">Initial Stock </label> <p class="product-data">{!! $product->initial_stock !!} &nbsp;</p>
                         <label class="pull-left">Min Stock </label> <p class="product-data">{!! $product->min_stock !!} &nbsp;</p>
                         <label class="pull-left">Max Stock </label> <p class="product-data">{!! $product->max_stock !!} &nbsp;</p>
                         <label class="pull-left">Current Stock </label> <p class="product-data">{!! $product->current_stock !!} &nbsp;</p>
                         <label class="pull-left">Reorder Point </label> <p class="product-data">{!! $product->reorder_point !!} &nbsp;</p>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="pricing">
                    <label class="pull-left">Supplier: </label> <p class="product-data">{!! $product->supplier_name !!} &nbsp;</p>
                    <label class="pull-left">Wholesale: </label> <p class="product-data">{!! $product->wholesale_price !!} &nbsp;</p>
                    <label class="pull-left">Retail: </label> <p class="product-data">{!! $product->retail_price !!} &nbsp;</p>
                </div>
              </div>

            </div>
       </div>
   </div>
   <hr>
   <div class="row">
       <div class="col-sm-12">
           <span class="label label-info">Restock History</span>
           <br>
           <br>
       </div>
   </div>
   <div class="row">
			 <div class="col-md-4 search-box">
					 <div class="input-group">
							<input type="text" class="form-control" placeholder="search" ng-model="filter.$">

							<span class="input-group-btn">
								<button class="btn btn-default" type="submit">

										<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</button>
							</span>
					 </div>
			 </div>
	 </div>
	 <div class="row">
	 	<div class="col-sm-12">
	 		<table ng-table="tableParams"  class="table table-striped main-table table-hover">
					<thead class="pointer">
								<tr>

										<th style="width:20%;" class="sortable" ng-class="{
												'sort-asc': tableParams.isSortBy('pdate', 'asc'),
												'sort-desc': tableParams.isSortBy('pdate', 'desc')
											}"
												ng-click="tableParams.sorting({'pdate' : tableParams.isSortBy('pdate', 'asc') ? 'desc' : 'asc'})">
												Purchase Date
										</th>
										<th style="width:10%;" class="sortable" ng-class="{
												'sort-asc': tableParams.isSortBy('qty', 'asc'),
												'sort-desc': tableParams.isSortBy('qty', 'desc')
											}"
												ng-click="tableParams.sorting({'qty' : tableParams.isSortBy('qty', 'asc') ? 'desc' : 'asc'})">
												Quantity
										</th>
										<th style="width:20%;" class="sortable" ng-class="{
												'sort-asc': tableParams.isSortBy('ornumber', 'asc'),
												'sort-desc': tableParams.isSortBy('ornumber', 'desc')
											}"
												ng-click="tableParams.sorting({'ornumber' : tableParams.isSortBy('ornumber', 'asc') ? 'desc' : 'asc'})">
												Order Number
										</th>
										<th style="width:30%;" class="sortable" ng-class="{
												'sort-asc': tableParams.isSortBy('supplier_name', 'asc'),
												'sort-desc': tableParams.isSortBy('supplier_name', 'desc')
											}"
												ng-click="tableParams.sorting({'supplier_name' : tableParams.isSortBy('supplier_name', 'asc') ? 'desc' : 'asc'})">
												Supplier
										</th>
										<th style="width:20%;" class="sortable" ng-class="{
												'sort-asc': tableParams.isSortBy('price', 'asc'),
												'sort-desc': tableParams.isSortBy('price', 'desc')
											}"
												ng-click="tableParams.sorting({'price' : tableParams.isSortBy('price', 'asc') ? 'desc' : 'asc'})">
												Price
										</th>
										<th>Option</th>
								</tr>
					 </thead>
					 <tbody>
							 <tr class="tableLoading" ng-show="$data.length == 0">
									 <td colspan="6" ><div class="text-center">no result found</div></td>
							 </tr>
								<tr ng-repeat="stock in $data" table-loading>
										<td data-title="'Purchase Date'" sortable="'pdate'">
												@{{ (stock.pdate).substring(0,10)}}
										</td>
										<td data-title="'Quantity'" sortable="'qty'">
												@{{stock.qty}}
										</td>
										<td data-title="'Order Number'" sortable="'ornumber'">
												@{{stock.ornumber}}
										</td>
										<td data-title="'Supplier'" sortable="'supplier_name'">
												@{{stock.supplier_name}}
										</td>
										<td data-title="'Price'" sortable="'price'">
												@{{stock.price}}
										</td>
										<td>
												<div ng-if="stock.order_img == null">
														<i class="fa fa-file-text tb-icon" title="No Order Slip file"></i>
														<i class="fa fa-money tb-icon" ng-click="updateCurrentProductPrice({{$product->id}}, stock.price)" title="Update with current price"></i>
														<i class="fa fa-truck tb-icon" ng-click="updateCurrentProductSupplier({{$product->id}}, stock.supplier_id)" title="Update with current supplier"></i>
												</div>
												<div ng-if="stock.order_img != null">
														<i class="fa fa-file-text-o tb-icon" onclick="showOrderSlip(this)" data-id="@{{stock.id}}" title="View Order Slip"></i>
												</div>

									 </td>
								</tr>

					 </tbody>

			 </table>
	 		
	 	</div>
	 	
	 </div>
			 
   <div class="row">
       <div class="col-sm-12">
           <span class="label label-info">Item History</span><br/><br/>        
       </div>
   </div>
   <div class="row">
			<div class="col-sm-12">
				<table class="table table-striped main-table table-hover">
					<thead>
						<tr>
							<th>Date Sold</th>
							<th>Quantity</th>
							<th>Price Sold</th>
						</tr>
					</thead>
					<tbody>
						@if(count($list)>0)
							@foreach($list as $product)
							<tr>
								<td>{{$product->date}}</td>
								<td>{{$product->qty}}</td>
								<td>{{$product->unit_price}}</td>
							</tr>
							
							@endforeach
						@endif
					</tbody>
				</table>
				{!! str_replace('/?', '?', $list->render()) !!} <!-- use this to solve pagination problem with slashes -->
			</div>
	 </div>
</div>


@stop