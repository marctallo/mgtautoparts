<fieldset>
    <legend>
      Product Info
      
       @if(isset($product)) 
        <button type="submit" class="btn btn-xs btn-success quick-update">update</button>
      @endif
    </legend>  

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
               <label for="category">Product Type <small class="text-success">(required)</small></label>
               {!! Form::select('cat_id', $category,null,array('class'=>'form-control')) !!}
               @if($errors->first('cat_id'))


                    <span class="text-danger"><small>{{ $errors->first('cat_id')}}</small></span>

                @endif 

            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="pname">Product Name</label>


                {!! Form::text('pname',null,['id'=>'pcode','class'=>'form-control','placeholder'=>'Product Name']) !!}

            
            </div>
        </div>
    </div>
    
    <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                <label for="pcode">Product Code <small class="text-success">(required)</small></label>


                {!! Form::text('pcode',null,['id'=>'pcode','class'=>'form-control','placeholder'=>'Product Code']) !!}

                @if($errors->first('pcode'))


                    <span class="text-danger"><small>{{ $errors->first('pcode')}}</small></span>

                @endif

              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label for="maker">Maker <small class="text-success">(required)</small></label>
                {!! Form::text('maker',null,['id'=>'maker','class'=>'form-control','placeholder'=>'Maker']) !!}
                @if($errors->first('maker'))


                    <span class="text-danger"><small>{{ $errors->first('maker')}}</small></span>

                @endif 
              </div>
          </div>

    </div>
    <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                <label for="application">Application <small class="text-success">(must be separated by comma)</small></label>
                {!! Form::text('application',null,['id'=>'application','class'=>'form-control','placeholder'=>'Application']) !!}

              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label for="maker">Year <small class="text-success">(must be separated by comma)</small></label>
                {!! Form::text('year',null,['id'=>'year','class'=>'form-control','placeholder'=>'Year']) !!}
                
                @if($errors->first('year'))


                    <span class="text-danger"><small>{{ $errors->first('year')}}</small></span>

                @endif 
              </div>
          </div>

    </div>
    <div class="form-group">
      <label for="Desciption">Description</label>
      {!! Form::textarea('description',null,['class'=>'summernote'])!!}
    </div>
</fieldset>
<fieldset>
    <legend>Stock Info</legend>
    <div class="row">
          <div class="col-md-3">
              <div class="form-group">
                <label for="unit">Unit</label>
                {!! Form::text('unit',null,['id'=>'unit','name'=>'unit','placeholder'=>'Unit','class'=>'form-control'])!!}
              </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                <label for="initial_stock">Initial Stock</label>
                {!! Form::input('number','initial_stock',null,['id'=>'initial_stock','name'=>'initial_stock','placeholder'=>'Initial Stock','class'=>'form-control number','min'=>'0'])!!}
              	@if($errors->first('initial_stock'))


										<span class="text-danger"><small>{{ $errors->first('initial_stock')}}</small></span>

								@endif
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                <label for="unit">Section</label>
                {!! Form::text('section',null,['id'=>'section','name'=>'section','placeholder'=>'Section','class'=>'form-control'])!!}
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                <label for="unit">Shelf</label>
                {!! Form::text('shelf',null,['id'=>'shelf','name'=>'shelf','placeholder'=>'Shelf','class'=>'form-control'])!!}
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                <label for="unit">Bin Location</label>
                {!! Form::text('binloc',null,['id'=>'binloc','name'=>'binloc','placeholder'=>'Bin Location','class'=>'form-control'])!!}
              </div>
          </div>

    </div>
    <div class="row">
          <div class="col-md-3">
              <div class="form-group">
                <label for="min_stock">Min Stock</label>
                {!! Form::input('number','min_stock',null,['id'=>'min_stock','name'=>'min_stock','placeholder'=>'Minimum Stock','class'=>'form-control number','min'=>'0'])!!}
                  @if($errors->first('min_stock'))


                <span class="text-danger"><small>{{ $errors->first('min_stock')}}</small></span>

            @endif
              </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                <label for="max_stock">Max Stock</label>
                {!! Form::input('number','max_stock',null,['id'=>'max_stock','name'=>'max_stock','placeholder'=>'Maximum Stock','class'=>'form-control number','min'=>'0'])!!}
                  @if($errors->first('max_stock'))


                    <span class="text-danger"><small>{{ $errors->first('max_stock')}}</small></span>

                @endif
              </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                <label for="current_stock">Current Stock</label>
                {!! Form::input('number','current_stock',null,['id'=>'current_stock','name'=>'current_stock','placeholder'=>'Current Stock','class'=>'form-control number','min'=>'0'])!!}
                  @if($errors->first('current_stock'))


                    <span class="text-danger"><small>{{ $errors->first('current_stock')}}</small></span>

                @endif
              </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                <label for="reorder_point">Reorder Point</label>
                {!! Form::input('number','reorder_point',null,['id'=>'reorder_point','name'=>'reorder_point','placeholder'=>'Reorder Point','class'=>'form-control number','min'=>'0'])!!}
                  @if($errors->first('reorder_point'))


                    <span class="text-danger"><small>{{ $errors->first('reorder_point')}}</small></span>

                @endif
              </div>
          </div>

    </div>
</fieldset>
<fieldset>
    <legend>Price Info</legend>
    <div class="row">
         <div class="col-md-3">
              <div class="form-group">
                <label for="price">Supplier</label>
                {!! Form::select('supplier_id', $supplier,null,array('class'=>'form-control')) !!} 
              
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                <label for="price">Wholesale Price</label>
                <div class="input-group">
                	<div class="input-group-addon">
										<span class="double-calc">%</span>
									</div>
									{!! Form::text('wholesale_price',null,['id'=>'wholesale_price','name'=>'wholesale_price','placeholder'=>'Wholesale Price','class'=>'form-control currency double-calc-val'])!!} 
								</div>
                
                  @if($errors->first('wholesale_price'))


                    <span class="text-danger"><small>{{ $errors->first('wholesale_price')}}</small></span>

                @endif
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                <label for="price">Retail Price</label>
                {!! Form::text('retail_price',null,['id'=>'retail_price','name'=>'retail_price','placeholder'=>'Retail Price','class'=>'form-control currency'])!!}
                  @if($errors->first('retail_price'))


                    <span class="text-danger"><small>{{ $errors->first('retail_price')}}</small></span>

                @endif
            
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                  <label for="price">Markup</label>
                  <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-primary active btn-sm markup">
                        {!! Form::radio('markup',.05,false,['class'=>'markup-value']) !!} 5%
                      </label>
                      <label class="btn btn-success btn-sm markup">
                        {!! Form::radio('markup',.10,false,['class'=>'markup-value']) !!} 10% 
                      </label>
                      <label class="btn btn-info btn-sm markup">
                        {!! Form::radio('markup',.15,false,['class'=>'markup-value']) !!} 15% 
                      </label>
                      <label class="btn btn-warning btn-sm markup">
                        {!! Form::radio('markup',.20,false,['class'=>'markup-value']) !!} 20% 
                      </label>
                      <label class="btn btn-default btn-sm markup">
                        {!! Form::radio('markup',.25,false,['class'=>'markup-value']) !!} 25% 
                      </label>
                      <label class="btn btn-danger btn-sm markup">
                        {!! Form::radio('markup',.30,false,['class'=>'markup-value']) !!} 30% 
                      </label>
                 </div>
              </div>
              
          </div>
          


    </div>
</fieldset>
<div class="row">
 <div class="col-md-6">
      <div class="form-group">
            <label for="price">Product Image <small class="text-success">(jpg,bmp,png,gif only)</small></label>
            {!! Form::file('image'); !!}
            @if($errors->first('image'))


                    <span class="text-danger"><small>{{ $errors->first('image')}}</small></span>

            @endif
            
        </div>
  </div>    
</div>
<div class="row">
      <div class="col-md-2">
          <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary active">
                {!! Form::radio('is_active',1,true) !!} Active
              </label>
              <label class="btn btn-primary">
                {!! Form::radio('is_active',0) !!} Inactive 
              </label>
          </div>
      </div>
      
</div>
<div class="row">
  <div class="col-md-2">
     <div class="checkbox">
      <label>{!! Form::checkbox('is_updated',null,false,['id' => 'is-updated','class'=>'checkbox-styled']) !!} <span> Updated</span></label>
    </div>
    </div>
</div>
<br>


