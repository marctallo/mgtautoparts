@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-history"></i> New Item Return</h3>
   </div>
</div>
<div class="inner-box"> 

   {!! Form::open(['action'=>'ItemReturnController@store']) !!}
      @include('item_return.partials.form')
      <button type="submit" class="btn btn-success">Save</button>
   {!! Form::close() !!}
</div>


@stop