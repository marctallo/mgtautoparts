@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6 {{ (\Auth::user()->role == 3)?'product-header':''}}">
       <h3 class="content-header"><i class="fa fa-history"></i> Item Returns</h3>
   </div>
    <div class="col-md-6 col-sm-6 col-xs-6 {{ (\Auth::user()->role == 3)?'product-button':''}}">
        @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
           <a href="{{action('ItemReturnController@create')}}" class="btn btn-success new-btn btn-sm inner-btn" title="New Item Return">
             <i class="fa fa-plus tb-icon"></i>
           </a>
        @endif
        <input type='hidden' id='url' value="{{\Request::url()}}">
    </div>
</div>
<div class="inner-box"> 
    {!! Form::open(['method'=>'get']) !!}
    <div class="row">
    
        <div class="col-md-4">
            <div class="form-group">
                <label for="date">From</label>
               {!! Form::text('date_from',null,['id'=>'date_from','class'=>'form-control date-input toggle-field'])!!}
            </div>
        </div>
       
       <div class="col-md-4">
            <div class="form-group">
                <label for="month">To</label>
                {!! Form::text('date_to',null,['id'=>'date_to','class'=>'form-control date-input toggle-field'])!!}
            </div>
        </div>
       <div class="col-md-4">
           <button class="btn pull-right" id="item-clear" type="button" > Clear Search</button>
           <button class="btn btn-info pull-right" id="item-search" style="margin-right:5px;"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search Item</button>

           

        </div>
    </div>
    
  
    {!! Form::close() !!}
    <hr>
   	<table class="table table-striped main-table table-hover">
       <thead>
           <tr>
                <th style="width:15%">{!! sort_header('pcode','Product Code',$sort_fields,'ItemReturnController@index') !!}</th>
                <th style="width:10%">{!! sort_header('qty','Quantity',$sort_fields,'ItemReturnController@index') !!}</th>
                <th style="width:15%">{!! sort_header('price','Total Price',$sort_fields,'ItemReturnController@index') !!}</th>
                <th style="width:15.5%">{!! sort_header('return_date','Return Date',$sort_fields,'ItemReturnController@index') !!}</th>
                <th style="width:15.5%">{!! sort_header('purchase_date','Purchase Date',$sort_fields,'ItemReturnController@index') !!}</th>
                <th style="width:35%">Return Reason</th>
                <th style="width:10%;" class="text-center">Option</th>
            </tr>
       </thead>
        <tbody>
          @if(count($returns) > 0)
           @foreach($returns as $return)
            <tr>
                <td>{{ $return->pcode }}</td>
                <td>{{ $return->qty }}</td>
                <td>{{ $return->price }}</td>
                <td>{{ $return->return_date }}</td>
                <td>{{ $return->purchase_date }}</td>
                <td>{{ $return->return_reason }}</td>
                <td class="text-center">
                     <form method="POST" action="{{'returns/'.$return->id}}" accept-charset="UTF-8" class="form-delete">
                       <input name="_method" type="hidden" value="DELETE">
                       <input name="_token" type="hidden" value="{{ csrf_token() }}">
                       @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
                           <i class="fa fa-trash-o tb-icon delete-btn" onclick="confirmModal('Delete this item? ',this)" title="Delete item"></i>
                       
                       @endif             

                       

                      </form> 
                </td>
            </tr>
            @endforeach
           @else
               <tr>
                <td colspan="6" class="text-center">No results found</td>
              </tr>
           @endif           
        </tbody>
        
    </table>
    {!! show_pagination_links($returns,$sort_fields) !!}
</div>
@stop