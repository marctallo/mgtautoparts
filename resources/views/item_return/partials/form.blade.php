<div class="row">
	<div class="col-xs-12 col-md-3">
		<div class="form-group">
		<label for="engine">Item Code <small class="text-success">(required)</small></label>


		{!! Form::text('pcode',null,['id'=>'pcode','class'=>'form-control','placeholder'=>'Product Code','auto-complete','autocomplete'=>'off']) !!}
		@if($errors->first('pcode'))

			<span class="text-danger"><small>{{ $errors->first('pcode')}}</small></span>

		@endif 

		</div>
	</div>
	<div class="col-xs-12 col-md-3">
		<div class="form-group">
		<label for="engine">Quantity <small class="text-success">(required)</small></label>


		{!! Form::text('qty',null,['id'=>'qty','class'=>'form-control','placeholder'=>'Quantity']) !!}
		
		@if($errors->first('qty'))

			<span class="text-danger"><small>{{ $errors->first('qty')}}</small></span>

		@endif 

		</div>
	</div>
	<div class="col-xs-12 col-md-3">
		<div class="form-group">
		<label for="chasis_type">Total Price <small class="text-success">(required)</small></label>


		{!! Form::text('price',null,['id'=>'price','class'=>'form-control','placeholder'=>'Total Price']) !!}
		@if($errors->first('price'))

			<span class="text-danger"><small>{{ $errors->first('price')}}</small></span>

		@endif 

		</div>
	</div>
	<div class="col-md-3">
          <div class="form-group">
            <label for="return_date">Return Date <small class="text-success">(required)</small></label>
            {!! Form::text('return_date',null,['class'=>'form-control date-input','id' => 'return_date','datepicker'])!!}
             
              @if($errors->first('return_date'))


                <span class="text-danger"><small>{{ $errors->first('return_date')}}</small></span>

             @endif
          </div>
     </div>
</div>
<div class="row">
	<div class="col-md-3">
          <div class="form-group">
            <label for="return_date">Date Purchased <small class="text-success">(required)</small></label>
            {!! Form::text('purchase_date',null,['class'=>'form-control date-input','id' => 'purchase_date','datepicker'])!!}
             
              @if($errors->first('purchase_date'))


                <span class="text-danger"><small>{{ $errors->first('purchase_date')}}</small></span>

             @endif
          </div>
     </div>
	
	<div class="col-md-9">
		<div class="form-group">
		  <label for="Desciption">Return Reason <small class="text-success">(required)</small></label>
		
		  {!! Form::text('return_reason',null,['id'=>'return_reason','class'=>'form-control']) !!}
		  @if($errors->first('return_reason'))

			<span class="text-danger"><small>{{ $errors->first('return_reason')}}</small></span>

		  @endif 
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		 <div class="checkbox">
		  <label>{!! Form::checkbox('add_to_stock',null,false,['id' => 'add_to_stock','class'=>'checkbox-styled']) !!} <span> Add to current stock</span></label>
		</div>
    </div>
</div>