<!DOCTYPE html>
<html lang="en">
	<head>

		<title>Sales Invoice</title>

		<style>
		
			body {
				font: normal 12px/150% Arial, Helvetica, sans-serif;
			}
			table{
				
				width: 100%;
			}
			
			table img {
				
				width: 248px;
				height: 46px;
					
			}
			.datagrid table {
				border-collapse: collapse;
				text-align: left;
				width: 100%;
			}
			.datagrid {
				font: normal 12px/150% Arial, Helvetica, sans-serif;
				background: #fff;
				overflow: hidden;
				border: 1px solid #999999;
			}
			.datagrid table td, .datagrid table th {
				padding: 6px 10px;
			}
			.datagrid table thead th {
				background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );
				background: -moz-linear-gradient( center top, #006699 5%, #00557F 100% );
				filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');
				background-color: #ef5031;
				color: #FFFFFF;
				font-size: 12px;
				font-weight: bold;
			}
			.datagrid table thead th:first-child {
				border: none;
			}
			.datagrid table tbody td {
				color: #00496B;
				border-left: 1px solid #E1EEF4;
				font-size: 12px;
				font-weight: normal;
			}
			.datagrid table tbody td:first-child {
				border-left: none;
			}
			.datagrid table tbody tr:last-child td {
				border-bottom: none;
			}
			
			.footer{
				
				 position: fixed;
    			 bottom: 40;
    			 width: 100%;
			}
			
			.invoice-company-info {
				
				margin:0;
				padding:0;
				font-size: 12px;
				font-weight: normal;
				
				color: #09001C;
			}
			.footer-message {
				
				font-weight: bold;
				font-size: 15px;
				color:#7C959C;
			}
			
			.top-border {
				
				border-top: solid 1px #E1EEF4;
			}

		</style>

	</head>

	<body>
		<div class="invoice-template">
			<table>
				<thead>
					<tr>
						<td>
							{!! Html::image('img/defaults/logo.png')!!}
							
						</td>
						<td>&nbsp;</td>
						<td align="right" style="color: #333333"><h2>SALES INVOICE</h2>
						</td>
					</tr>
					<tr>
						<td>
							
							<p class="invoice-company-info">{{ $settings->company_name }}</p>
							<p class="invoice-company-info">{{ $settings->address }}</p>
							<p class="invoice-company-info">{{ $settings->number }}</p>
							<p class="invoice-company-info">{{ $settings->email }}</p>
						</td>
						<td>
                            
						</td>
						<td align="right" style="color: #333333">
						<p>
							<strong>Customer</strong> {{ $invoice->customer}}
						</p>
						<p>
							<strong>Invoice #</strong> {{ $invoice->invoice_number}}
						</p>
						<p>
							<strong>Date</strong> {{ $invoice->date}}
						</p></td>
					</tr>
					<tr>
						<td colspan="3">
						<hr style="color: #cccccc">
						</td>
					</tr>
				</thead>

			</table>
			<br>
			<div class="datagrid">
				<table>
					<thead>
						<tr>
							<th style="width: 20%">Product Code</th>
							<th style="width: 30%">Description</th>
							<th style="width: 12.5%">Unit Price</th>
							<th style="width: 12.5%">Quantity</th>
							<th style="width: 12.5%">Subtotal</th>
						</tr>
					</thead>
					<tbody>
						
					    <?php $total = 0; ?>
                    
				        @foreach($items as $item)
				        
				            <?php $total = $total + getTotal($item->unit_price,$item->qty,true)  ?>
							<tr>
								<td>{{ $item->pcode }}</td>
								<td>{{ $item->pname }}</td>
								<td align="right">{{ $item->unit_price }}</td>
								<td align="center">{{ $item->qty }}</td>
								<td align="right">{{ getTotal($item->unit_price,$item->qty)}}</td>
							</tr>
					    @endforeach
						<tr >
							<td class="top-border" colspan="3" align="right"></td>
							<td class="top-border"></td>
							<td class="top-border"></td>
                        </tr>
						<tr >
							<td colspan="4" align="right" class="top-border">Total</td>
							<td class="top-border" align="right">{{ number_format($total,2) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			
		</div>
		<div class="footer">
			<table>
				<tr>
					<td>
						
					</td>
					<td align="right">
						
						<p class="footer-message"></p>
					</td>
				</tr>
			</table>
		</div>
	</body>

</html>
