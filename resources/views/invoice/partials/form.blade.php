<div class="row">
      <div class="col-md-4">
          <div class="form-group">
            <label for="customer">Customer</label>
            {!! Form::text('customer',null,['class'=>'form-control'])!!}
          </div>
      </div>
      <div class="col-md-4">
          <div class="form-group">
            <label for="expense_date">Date <small class="text-success">(required)</small></label>
            {!! Form::text('date',null,['class'=>'form-control date-input','id' => 'invoice-date','required','ng-model'=>'inv.date','datepicker'])!!}
             
              @if($errors->first('date'))


                <span class="text-danger"><small>{{ $errors->first('date')}}</small></span>

             @endif
          </div>
      </div>
      <div class="col-md-4">
          <div class="form-group">
            <label for="invoice_number">Invoice Number <small class="text-success">(required)</small></label>
            {!! Form::text('invoice_number',null,['class'=>'form-control','id' => 'invoice-number','required','ng-model'=>'inv.invoice_number'])!!}
              @if($errors->first('invoice_number'))


                <span class="text-danger"><small>{{ $errors->first('invoice_number')}}</small></span>

             @endif
          </div>
      </div>
</div>
<hr>

<div>
   <invoice-table invoice-model="inv"></invoice-table>
    <!--<table class="table invoice-table">
        <thead>
            <tr>
                <th style="width:25%">Product</th>
                <th style="width:10%">Qty</th>
                <th style="width:15%">Unit Price</th>
                <th style="width:15%">Wholesale Price</th>
                <th class="text-right">Sub Total</th>
                <th style="width:5%"></th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="item in items">
            	<td>
								<input type="text" class="form-control input-sm" ng-click="showProducts(item)" ng-model="item.product" required readonly style="background:#FFF;">
								<input type="hidden" name="pid[]" class="invoice-item-id" ng-value="item.pid">
							</td>
							<td>
								<input type="text" class="form-control input-sm numeric" name="qty[]" disabled="true" ng-model="item.qty" disabled ng-disabled="!item.product" ng-blur="computeSubtotal(item)" required autocomplete="off">
							</td>
							<td>
								<input type="text" class="form-control input-sm numeric" name="price[]" disabled="true" ng-model="item.price" disabled ng-disabled="!item.product" ng-blur="computeSubtotal(item)" required>
								<input type="hidden" name="wholesale_price[]" class="invoice-wholesale-price" ng-value="item.wholesale_price">
							</td>
							<td>
								<span>@{{item.wholesale_price}}</span>
							</td>
							<td class="text-right"><span class="subtotal">@{{item.subtotal | currency:''}}</span></td>
							<td><i class="fa fa-close pointer text-danger" ng-click="removeRow(item)"></i></td>
            </tr>
        </tbody>
    </table>
    <table class="table">
        <tbody>
            <tr>
                <td colspan="5" style="width:80%"><strong class="text-info pointer" ng-click="addRow()">Add Item</strong>
                    @if($errors->first('items'))


                        <span class="text-danger"><small>{{ $errors->first('items')}}</small></span>

                    @endif
                </td>
                <td><strong class="pull-right">Total:<span class="items-total">@{{total | currency:"&#8369"}}</span></strong></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    
    <button type="submit" class="btn btn-success" ng-disabled="validate()">Create Invoice</button>-->
   
    
    
</div>
