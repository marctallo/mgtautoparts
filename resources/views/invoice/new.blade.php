@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-line-chart"></i> New Invoice</h3>
   </div>
</div>
<div class="inner-box" ng-controller="InvoiceController as inv"> 

   {!! Form::open(['action'=>'InvoiceController@store','id'=>'new-invoice-frm','name'=>'invoiceFrm','novalidate']) !!}
      @include('invoice.partials.form')
      
   {!! Form::close() !!}
</div>


@stop