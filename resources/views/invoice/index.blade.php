@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6 {{ (\Auth::user()->role == 3)?'product-header':''}}">
       <h3 class="content-header"><i class="fa fa-line-chart"></i> Invoice</h3>
   </div>
    <div class="col-md-6 col-sm-6 col-xs-6 {{ (\Auth::user()->role == 3)?'product-button':''}}">
       
       @if(showItem([config('role.dev')]))
           <a href="{{action('ProductController@showTrash')}}" class="btn btn-danger new-btn btn-sm inner-btn" title="View Trash">
               <i class="fa fa-trash tb-icon"></i>
           </a>
        @endif    
        @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
           <a href="{{action('InvoiceController@create')}}" class="btn btn-success new-btn btn-sm inner-btn" title="New Invoice">
             <i class="fa fa-plus tb-icon"></i>
           </a>
        @endif
        <input type='hidden' id='url' value="{{\Request::url()}}">
    </div>
</div>
<div class="inner-box"> 
    {!! Form::open(['method'=>'get']) !!}
    <div class="row">
    
        <div class="col-md-4">
            <div class="form-group">
                <label for="invoice_number">Invoice Number</label>
                {!! Form::text('invoice_number',\Request::input('invoice_number'),['id'=>'invoice_number','class'=>'form-control toggle-field','placeholder'=>'Invoice Number']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="date">From</label>
               {!! Form::text('date_from',null,['id'=>'date_from','class'=>'form-control date-input toggle-field'])!!}
            </div>
        </div>
       
       <div class="col-md-4">
            <div class="form-group">
                <label for="month">To</label>
                {!! Form::text('date_to',null,['id'=>'date_to','class'=>'form-control date-input toggle-field'])!!}
                 <!--{!! Form::select('month', monthList(),null,array('class'=>'form-control toggle-field')) !!}-->
            </div>
        </div>
       
    </div>
    <div class="row">
        <div class="col-md-4">
           <button class="btn btn-info" id="item-search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search Item</button>

           <button class="btn" id="item-clear" type="button"> Clear Search</button>

        </div>
    </div>
    {!! Form::close() !!}
    <hr>
    <table class="table table-striped main-table table-hover">
       <thead>
           <tr>
                <th style="width:30%">{!! sort_header('invoice_number','Invoice Number',$sort_fields,'InvoiceController@index') !!}</th>
                <th style="width:50%">{!! sort_header('date','Date',$sort_fields,'InvoiceController@index') !!}</th>
                <!--<th>Total Amount</th>-->
                <th style="width:10%;">Option</th>
            </tr>
       </thead>
        <tbody>
          @if(count($invoices) > 0)
           @foreach($invoices as $invoice)
            <tr>
                <td>{{ $invoice->invoice_number }}</td>
                <td>{{ $invoice->date }}</td>
                <td>
                     <form method="POST" action="{{'invoice/'.$invoice->id}}" accept-charset="UTF-8" class="form-delete">
                       <input name="_method" type="hidden" value="DELETE">
                       <input name="_token" type="hidden" value="{{ csrf_token() }}">
                       @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
                           <i class="fa fa-trash-o tb-icon delete-btn" onclick="confirmModal('Delete this invoice? ',this)" title="Move to thrash"></i>
                           <a href="{{ action('InvoiceController@printInvoice',$invoice->id)}}" target="_blank"><i class="fa fa-print tb-icon delete-btn" title="Print Invoice"></i></a>
                          <!-- <a href="{{ action('InvoiceController@downloadInvoice',$invoice->id)}}"><i class="fa fa-download tb-icon delete-btn" title="Download Invoice"></i></a>-->
                       @endif             

                       

                      </form> 
                </td>
            </tr>
            @endforeach
           @else
               <tr>
                <td colspan="3" class="text-center">No results found</td>
              </tr>
           @endif           
        </tbody>
        
    </table>
    {!! show_pagination_links($invoices,$sort_fields) !!}
</div>
@stop