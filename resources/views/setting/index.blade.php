@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6">
       <h3 class="content-header"><i class="fa fa-cog"></i> Settings</h3>
        <input type='hidden' id='url' value="{{\Request::url()}}">
   </div>
</div>
<div class="inner-box">
	{!! Form::model($settings,['method'=>'patch','action'=>['SettingsController@update',$settings->id]]) !!}
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group">
					<label for="cname">Company Name</label>


					{!! Form::text('company_name',null,['class'=>'form-control','placeholder'=>'Company Name']) !!}

					@if($errors->first('company_name'))


							<span class="text-danger"><small>{{ $errors->first('company_name')}}</small></span>

					@endif
			</div>
		</div>
	</div>
   <div class="row">
   	<div class="col-sm-12">
   		<div class="form-group">
					<label for="address">Address</label>


					{!! Form::text('address',null,['class'=>'form-control','placeholder'=>'Address']) !!}

			</div>
   	</div>
   </div>
   <div class="row">
   	<div class="col-sm-12">
   		<div class="form-group">
					<label for="cnumber">Contact Number</label>


					{!! Form::text('number',null,['class'=>'form-control','placeholder'=>'Contact Number']) !!}


			</div>
   	</div>
   </div>
   <div class="row">
   	<div class="col-sm-12">
   		<div class="form-group">
					<label for="email">Email</label>


					{!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Email']) !!}

					@if($errors->first('email'))


							<span class="text-danger"><small>{{ $errors->first('email')}}</small></span>

					@endif
			</div>
   	</div>
	</div>
	<div class="row">
		<div class="col-md-2 col-sm-12">
			<div class="form-group">
					<label for="email">Base Markup</label>


					{!! Form::input('number','base_markup',null,['class'=>'form-control','placeholder'=>'%','min'=>'0']) !!}

					@if($errors->first('base_markup'))


							<span class="text-danger"><small>{{ $errors->first('base_markup')}}</small></span>

					@endif
			</div>
		</div>
		<div class="col-md-2 col-sm-12">
			<div class="form-group">
					<label for="email">Products Per Page</label>


					{!! Form::input('number','per_page',null,['class'=>'form-control','min'=>'0']) !!}

					@if($errors->first('per_page'))


							<span class="text-danger"><small>{{ $errors->first('per_page')}}</small></span>

					@endif
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2 col-sm-12">
			<div class="checkbox">
      		<label>{!! Form::checkbox('show_inactive',null,false,['id' => 'show_inactive','class'=>'checkbox-styled']) !!} <span> Show Inactive</span></label>
    	</div>
		</div>
	</div>	 
			
      <button type="submit" class="btn btn-success">Update</button>
   {!! Form::close() !!}
</div>


@stop