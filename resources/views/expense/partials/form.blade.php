<div class="row">
      <div class="col-md-6">
          <div class="form-group">
            <label for="expense_category">Expense Type</label>
            <input type='hidden' id='url' value="{{base_path()}}">
            {!! Form::select('expense_category', $type,null,array('class'=>'form-control','id'=>'expense-category')) !!}
            <button type="button" class="btn btn-info" id="add-expense-category">+</button>
            @if($errors->first('expense_category'))


                <span class="text-danger"><small>{{ $errors->first('expense_category')}}</small></span>

            @endif
          </div>
      </div>
    
      <div class="col-md-6">
          <div class="form-group">
            <label for="expense_date">Date</label>
            {!! Form::text('expense_date',null,['id'=>'expense_date','class'=>'form-control date-input'])!!}
              @if($errors->first('expense_date'))


                <span class="text-danger"><small>{{ $errors->first('expense_date')}}</small></span>

            @endif
          </div>
      </div>


</div>
<div class="form-group">
<label for="amount">Amount</label>


{!! Form::text('amount',null,['id'=>'amount','class'=>'form-control','placeholder'=>'Amount']) !!}

@if($errors->first('amount'))


    <span class="text-danger"><small>{{ $errors->first('amount')}}</small></span>

@endif
</div>

<div class="form-group">
  <label for="note">Note</label>
  {!! Form::textarea('note',null,['class'=>'summernote'])!!}
</div>
