@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-calendar"></i> Update Expense</h3>
   </div>
</div>
<div class="inner-box"> 

   {!! Form::model($expense,['method'=>'patch','action'=>['ExpenseController@update',$expense->id]]) !!}
      @include('expense.partials.form')
      <button type="submit" class="btn btn-success">Update</button>
   {!! Form::close() !!}
</div>


@stop