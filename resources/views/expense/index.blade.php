@extends('layout.main')




@section('content')


<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6">
       <h3 class="content-header"><i class="fa fa-calendar"></i> Expenses</h3>
        <input type='hidden' id='url' value="{{\Request::url()}}">
   </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
       @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
           <a href="{{action('ExpenseController@create')}}" class="btn btn-success new-btn btn-sm inner-btn" title="New Expense">
             <i class="fa fa-plus tb-icon"></i>
           </a>
       @endif
        
        
    </div>
</div>
<div class="inner-box"> 
   <div class="row">
       <div class="col-md-4 col-md-offset-8 search-box">
           {!! Form::open(['action'=>'ExpenseController@index','id'=>'expense-form','method'=>'get']) !!}
           {!! Form::select('month', $month,\Request::input('filter'),array('class'=>'form-control','id'=>'expense-month','name'=>'filter')) !!}
           {!! Form::close() !!}
       </div>
   </div>
    <table class="table table-striped main-table table-hover">
    <thead>
        <th style="width:20%;">Expense </th>
        <th style="width:20%;">Amount</th>
        <th style="width:40%;">Note</th>
        <th class="text-center">Option</th>
    </thead>
    <tbody>
        
        @if(count($expenses))
        
        <?php (double)$total = 0; ?>
        @foreach($expenses as $expense)
           <?php $total = (double)$total + (double)$expense->amount; ?>
            <tr>
                <td>{{ $expense->cname}}</td>
                <td>{{ number_format($expense->amount, 2, '.', ',') }}</td>
                <td>{{ $expense->note }}</td>
                <td class="text-center">
                    {!! Form::open(['action'=>['ExpenseController@destroy',$expense->id],'method'=>'DELETE']) !!}
                    
                     
                       <a href="{{ action('ExpenseController@edit',$expense->id)}}" title="Edit">
                           <i class="fa fa-edit tb-icon"></i>
                       </a>       
                       <i class="fa fa-trash-o tb-icon delete-btn" onclick="confirmModal('Delete this expense?',this)" title="Move to thrash"></i>
             
             
                    {!! Form::close() !!}   
                </td>
            </tr>
            
        @endforeach
        @else
            <tr>
                <td colspan="4" class="text-center">
                    <span class="muted">no results found </span>
                </td>
            </tr>
            
        @endif
    </tbody>

    </table>
    @if(count($expenses))
        <strong>Total Expense for the month of <?php 
            $monthNum  = (\Request::input('filter') == null) ? date('n'): \Request::input('filter');
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');?>{{$monthName}}: </strong>{{ number_format($total, 2, '.', ',') }}<br>
    @endif
   
   
</div>


@stop