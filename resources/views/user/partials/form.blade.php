<div class="row">
      <div class="col-md-6">
          <div class="form-group">
            <label for="username">Username</label>
            {!! Form::text('username',null,['id'=>'username','name'=>'username','placeholder'=>'Username','class'=>'form-control'])!!}
            @if($errors->first('username'))


             <span class="text-danger"><small>{{ $errors->first('username')}}</small></span>

            @endif
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
            <label for="email">Email</label>
            {!! Form::email('email',null,['id'=>'email','name'=>'email','placeholder'=>'Email','class'=>'form-control'])!!}
            @if($errors->first('email'))


             <span class="text-danger"><small>{{ $errors->first('email')}}</small></span>

            @endif

          </div>
      </div>

    </div>
    <div class="row">
      <div class="col-md-6">
          <div class="form-group">
            <label for="fname">First Name</label>
            {!! Form::text('fname',null,['id'=>'fname','name'=>'fname','placeholder'=>'First Name','class'=>'form-control'])!!}
            @if($errors->first('fname'))


             <span class="text-danger"><small>{{ $errors->first('fname')}}</small></span>

            @endif
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
            <label for="lname">Last Name</label>
            {!! Form::text('lname',null,['id'=>'lname','name'=>'lname','placeholder'=>'Last Name','class'=>'form-control'])!!}
            @if($errors->first('lname'))


             <span class="text-danger"><small>{{ $errors->first('lname')}}</small></span>

            @endif
          </div>
      </div>

    </div>
    <div class="row">
      <div class="col-md-6">
          <div class="form-group">
            <label for="password">Password</label>
            {!! Form::password('password',['id'=>'password','name'=>'password','placeholder'=>'Password','class'=>'form-control'])!!}
            @if($errors->first('password'))


             <span class="text-danger"><small>{{ $errors->first('password')}}</small></span>

            @endif
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
            <label for="rpassword">Re-type Password</label>
            {!! Form::password('rpassword',['id'=>'rpassword','name'=>'rpassword','placeholder'=>'Re-type Password','class'=>'form-control'])!!}
            @if($errors->first('rpassword'))


             <span class="text-danger"><small>{{ $errors->first('rpassword')}}</small></span>

            @endif   
          </div>
      </div>

    </div>

    @if(\Request::segment(1) != 'myaccount')
    <div class="row">
      <div class="col-md-6">
          <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary {{ (isset($user->role))? ($user->role == '0'?'active':''):''}}">
               
                {!! Form::radio('role',0) !!} Admin
              </label>
              <label class="btn btn-primary {{ (isset($user->role))? ($user->role == '1'?'active':''):''}}">
                {!! Form::radio('role',1) !!} User 
              </label>
              <label class="btn btn-primary {{ (isset($user->role))? ($user->role == '2'?'active':''):''}}">
                {!! Form::radio('role',2) !!} Encoder 
              </label>
            </div>
      </div>
      <div class="col-md-6">

      </div>

    </div>
    @endif
    <br/>
