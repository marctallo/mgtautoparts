@extends('layout.main')




@section('content')


<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6">
       <h3 class="content-header"><i class="fa fa-users"></i> Users</h3>
        <input type='hidden' id='url' value="{{\Request::url()}}">
   </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
       <a href="{{action('UserController@showTrash')}}" class="btn btn-danger new-btn btn-sm inner-btn" title="View Trash">
               <i class="fa fa-trash tb-icon"></i>
        </a>
        <a href="{{ action('UserController@create') }}" class="btn btn-success new-btn btn-sm inner-btn" title="New User">
            <i class="fa fa-plus tb-icon"></i>
        </a>
        
    </div>
</div>
<div class="inner-box" ng-controller="UserController as user"> 
   <div class="row">
       <div class="col-md-4 col-md-offset-8 search-box">
           <div class="input-group">
              <input type="text" class="form-control" placeholder="search" ng-model="user.filter">

              <span class="input-group-btn">
                <button class="btn btn-default" type="submit">

                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </button>
              </span>
           </div>
       </div>
   </div>
    <table ng-table="user.tableParams"  class="table table-striped main-table table-hover">
      <thead class="pointer">
            <tr>
                <th style="width:18%;" class="sortable" ng-class="{
                    'sort-asc': user.tableParams.isSortBy('username', 'asc'),
                    'sort-desc': user.tableParams.isSortBy('username', 'desc')
                  }"
                    ng-click="user.tableParams.sorting({'username' : user.tableParams.isSortBy('username', 'asc') ? 'desc' : 'asc'})">
                    Username
                </th>
                <th style="width:15%;" class="sortable" ng-class="{
                    'sort-asc': user.tableParams.isSortBy('fname', 'asc'),
                    'sort-desc': user.tableParams.isSortBy('fname', 'desc')
                  }"
                    ng-click="user.tableParams.sorting({'fname' : user.tableParams.isSortBy('fname', 'asc') ? 'desc' : 'asc'})">
                    First Name
                </th>
                <th style="width:15%;" class="sortable" ng-class="{
                    'sort-asc': user.tableParams.isSortBy('lname', 'asc'),
                    'sort-desc': user.tableParams.isSortBy('lname', 'desc')
                  }"
                    ng-click="user.tableParams.sorting({'lname' : user.tableParams.isSortBy('lname', 'asc') ? 'desc' : 'asc'})">
                    Last Name
                </th>
                <th style="width:20%;" class="sortable" ng-class="{
                    'sort-asc': user.tableParams.isSortBy('email', 'asc'),
                    'sort-desc': user.tableParams.isSortBy('email', 'desc')
                  }"
                    ng-click="user.tableParams.sorting({'email' : user.tableParams.isSortBy('email', 'asc') ? 'desc' : 'asc'})">
                    Email
                </th>
                <th style="width:15%;" class="sortable" ng-class="{
                    'sort-asc': user.tableParams.isSortBy('role', 'asc'),
                    'sort-desc': user.tableParams.isSortBy('role', 'desc')
                  }"
                    ng-click="user.tableParams.sorting({'role' : user.tableParams.isSortBy('role', 'asc') ? 'desc' : 'asc'})">
                    Role
                </th>
                <th>Option</th>
            </tr>
       </thead>
       <tbody>
           <tr>
               <td colspan="6" class="text-center table-loader tableLoading" ng-hide="$data.length == 0">{!! Html::image('img/defaults/reload.gif')!!}</td>
           </tr>
           <tr class="tableLoading" ng-show="$data.length == 0">
                <td colspan="6" ><div class="text-center">no result found</div></td>
            </tr>
            <tr ng-repeat="user_data in $data" table-loading>
                <td data-title="'Username'" sortable="'username'">
                    @{{user_data.username}}
                </td>
                <td data-title="'First Name'" sortable="'fname'">
                    @{{user_data.fname}}
                </td>
                <td data-title="'Lastname'" sortable="'lname'">
                    @{{user_data.lname}}
                </td>
                <td data-title="'Email'" sortable="'email'">
                    @{{user_data.email}}
                </td>
                <td data-title="'Role'" sortable="'role'">
                    @{{(user_data.role == '0')?'Admin' : (user_data.role == '1')? 'User' : 'Encoder'}}
                </td>
                <td>
                  <form method="POST" action="@{{'users/'+user_data.id}}" accept-charset="UTF-8" class="form-delete">
                   <input name="_method" type="hidden" value="DELETE">
                   <input name="_token" type="hidden" value="{{ csrf_token() }}">
                   <a href="@{{'users/'+user_data.id+'/edit'}}" title="Edit">
                       <i class="fa fa-edit tb-icon"></i>
                   </a>       
                   <i class="fa fa-trash-o tb-icon delete-btn tb-icon" ng-click="user.showModal('Delete this user?',$event)" title="Move to thrash"></i>
                   <i class="fa fa-envelope-o tb-icon "></i>
                   @if(showItem([config('role.dev')]))
                   <a href="@{{'users/'+user_data.id}}" title="View">
                       <i class="fa fa-eye tb-icon"></i>
                   </a>
                   @endif
                  </form>   
                   
               </td>
            </tr>
           
       </tbody>
        
   </table>
</div>


@stop