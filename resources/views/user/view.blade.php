@extends('layout.main')


@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-user"></i> User Profile</h3>
   </div>
</div>
<div class="inner-box"> 

 <div class="profile-box">
     <div class="image-holder pull-left">
         {!! Html::image('img/defaults/1365608073_Akuma.png','Primary',['class'=>'img-circle'])!!}
     </div>
     <div class="name-holder pull-left">
         <h3>{{ $user->fname }} {{ $user->lname}}</h3>
         <h4>{{ $user->email}}</h4>
         <a href="{{ action('UserController@edit',$user->id) }}"class="btn btn-info btn-sm">Update</a>
         
         @if($user->id != \Auth::user()->id)
         
         {!! Form::open(['action'=>['UserController@destroy',$user->id],'method'=>'delete']) !!}
             <button type="submit" class="btn btn-danger btn-sm ">Delete</button>
         {!! Form::close() !!}
         
         @endif
     </div>
 </div>


 <div class="clearfix"></div>
 <hr>
 <div class="trail-holder">
    <h2>Audit Trail</h2>
     <table class="table table-striped table-hover main-table">
         <thead>
             <tr>
                 <th style="width:25%;">Action</th>
                 <th style="width:25%;">Item ID</th>
                 <th style="width:25%;">Section</th>
                 <th style="width:25%;">Date</th>
             </tr>
         </thead>
         <tbody>
           @if(count($trails))
            @foreach($trails as $trail)
                <tr>
                     <td>{{ $trail->action }}</td>
                     <td>{{$trail->item_id}}</td>
                     <td>{{ $trail->section }}</td>
                     <td>{{ $trail->created_at }}</td>
                 </tr>
            @endforeach
            @else
                <tr>
                    <td colspan="4">
                        <span class="muted">No Results found</span>
                    </td>
                </tr>
            @endif        
         </tbody>
     </table>
     {!! str_replace('/?', '?', $trails->render()) !!}
     
     
 </div>     
</div>

@stop


