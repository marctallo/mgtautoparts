@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-user"></i> Update {{ $user->username }}</h3>
   </div>
</div>
<div class="inner-box"> 

   {!! Form::model($user,['method'=>'patch','action'=>['UserController@update',$user->id]]) !!}
      @include('user.partials.form')
      <button type="submit" class="btn btn-success">Update</button>
   {!! Form::close() !!}
</div>


@stop