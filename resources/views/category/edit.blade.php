@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-12">
       <h3 class="content-header"><i class="fa fa-truck"></i> Update {{ $category->cname }}</h3>
   </div>
</div>
<div class="inner-box"> 

   {!! Form::model($category,['method'=>'patch','action'=>['CategoryController@update',$category->id]]) !!}
      @include('category.partials.form')
      <button type="submit" class="btn btn-success">Update</button>
   {!! Form::close() !!}
</div>


@stop