@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6">
       <h3 class="content-header"><i class="fa fa-server"></i> Categories</h3>
        <input type='hidden' id='url' value="{{\Request::url()}}">
        <input type='hidden' id='is_trashed' value="0">
   </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
       @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
          <a href="{{action('CategoryController@showTrash')}}" class="btn btn-danger new-btn btn-sm inner-btn" title="View Trash">
               <i class="fa fa-trash tb-icon"></i>
          </a>
           <a href="{{action('CategoryController@create')}}" class="btn btn-success new-btn btn-sm inner-btn" title="New Category">
             <i class="fa fa-plus tb-icon"></i>
           </a>
       @endif
        
        
    </div>
</div>
<div class="inner-box" ng-controller="CategoryController as cat"> 
   <div class="row">
       <div class="col-md-4 col-md-offset-8 search-box">
           <div class="input-group">
              <input type="text" class="form-control" placeholder="search" ng-model="cat.filter">

              <span class="input-group-btn">
                <button class="btn btn-default" type="submit">

                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </button>
              </span>
           </div>
       </div>
   </div>
    <table ng-table="cat.tableParams"  class="table table-striped main-table table-hover">
      <thead class="pointer">
            <tr>
               <th style="width:10%;" class="sortable" ng-class="{
                    'sort-asc': cat.tableParams.isSortBy('id', 'asc'),
                    'sort-desc': cat.tableParams.isSortBy('id', 'desc')
                  }"
                    ng-click="cat.tableParams.sorting({'id' : cat.tableParams.isSortBy('id', 'asc') ? 'desc' : 'asc'})">
                    ID
                </th>
                <th style="width:40%;" class="sortable" ng-class="{
                    'sort-asc': cat.tableParams.isSortBy('cname', 'asc'),
                    'sort-desc': cat.tableParams.isSortBy('cname', 'desc')
                  }"
                    ng-click="cat.tableParams.sorting({'cname' : cat.tableParams.isSortBy('cname', 'asc') ? 'desc' : 'asc'})">
                    Category
                </th>
                <th style="width:20%;" class="sortable" ng-class="{
                    'sort-asc': cat.tableParams.isSortBy('shelf', 'asc'),
                    'sort-desc': cat.tableParams.isSortBy('shelf', 'desc')
                  }"
                    ng-click="cat.tableParams.sorting({'shelf' : cat.tableParams.isSortBy('shelf', 'asc') ? 'desc' : 'asc'})">
                    Shelf
                </th>
                <th style="width:20%;" class="sortable" ng-class="{
                    'sort-asc': cat.tableParams.isSortBy('cname', 'asc'),
                    'sort-desc': cat.tableParams.isSortBy('cname', 'desc')
                  }"
                    ng-click="cat.tableParams.sorting({'cname' : cat.tableParams.isSortBy('cname', 'asc') ? 'desc' : 'asc'})">
                    Location
                </th>
                
                <th style="width:10%;" class="text-center">Option</th>
            </tr>
       </thead>
       <tbody>
           <tr>
               <td colspan="5" class="text-center table-loader tableLoading" ng-hide="$data.length == 0">{!! Html::image('img/defaults/reload.gif')!!}</td>
           </tr>
            <tr class="tableLoading" ng-show="$data.length == 0">
                <td colspan="5" ><div class="text-center">no result found</div></td>
            </tr>
            <tr ng-repeat="category in $data" table-loading>
               <td data-title="'ID'" sortable="'id'">
                    @{{category.id}}
                </td>
                <td data-title="'Username'" sortable="'username'">
                    @{{category.cname}}
                </td>
                <td data-title="'Shelf'" sortable="'shelf'">
                    @{{category.shelf}}
                </td>
                <td data-title="'Section'" sortable="'section'">
                    @{{category.section}}
                </td>
                <td class="text-center">
                  <form method="POST" action="@{{'categories/'+category.id}}" accept-charset="UTF-8" class="form-delete">
                   <input name="_method" type="hidden" value="DELETE">
                   <input name="_token" type="hidden" value="{{ csrf_token() }}">
                   @if(showItem([config('role.admin'),config('role.encoder'),config('role.dev')]))
                       <a href="@{{'categories/'+category.id+'/edit'}}" title="Edit"><i class="fa fa-edit tb-icon"></i></a>
                       <i class="fa fa-trash-o tb-icon delete-btn" ng-click="cat.showModal('Delete this category?',$event)" title="Move to thrash"></i>
                   @endif
                  </form>   
                   
               </td>
            </tr>
           
       </tbody>
        
   </table>

</div>


@stop