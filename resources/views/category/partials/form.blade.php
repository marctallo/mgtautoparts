<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<label for="cname">Category Name</label>


			{!! Form::text('cname',null,['id'=>'cname','class'=>'form-control','placeholder'=>'Category Name']) !!}

			@if($errors->first('cname'))


				<span class="text-danger"><small>{{ $errors->first('cname')}}</small></span>

			@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
		  <label for="shelf">Shelf</label>
		  {!! Form::text('shelf',null,['id'=>'shelf','class'=>'form-control','placeholder'=>'Shelf']) !!}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
		  <label for="section">Section</label>
		  {!! Form::text('section',null,['id'=>'section','class'=>'form-control','placeholder'=>'Section']) !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
		  <label for="Desciption">Description</label>
		  {!! Form::textarea('description',null,['class'=>'summernote'])!!}
		</div>
	</div>
</div>
