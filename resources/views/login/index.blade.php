<!DOCTYPE html>
<html class="no-js" lang="">

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    {!! Html::style('bower_components/bootstrap/dist/css/bootstrap.css') !!}
    {!! Html::style('css/login.css') !!}
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div class="login-wrapper container">
      
        @if (count($errors) > 0)
            <div class="alert alert-danger login-error">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
       
        <div class="login-box">
            <h2>Login</h2>
            <br/>
            {!! Form::open(['post'=>'POST','class'=>'form-signin','action'=>'LoginController@auth']) !!}
            <div class="form-group">
                {!! Form::text('username',null,['class'=>'form-control input-lg','placeholder' => 'Username'])!!}
                
            </div>
            <div class="form-group">
               {!! Form::password('password',['class'=>'form-control input-lg','placeholder' => 'Password'])!!}
            </div>
            
            <button class="btn btn-success pull-right btn-lg">Login</button>
            {!! Form::close() !!}
        </div>
    </div>
    {!! Html::script('bower_components/jquery/dist/jquery.min.js') !!}
    
    <script>
        $(document).ready(function(){
            
           var height = $(document).height();
            
            $('.login-wrapper').height(height);
        });
    </script>
</body>

</html>