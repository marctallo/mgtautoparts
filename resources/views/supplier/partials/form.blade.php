<div class="form-group">
    <label for="cname">Supplier Name</label>


    {!! Form::text('supplier_name',null,['class'=>'form-control','placeholder'=>'Supplier Name']) !!}

    @if($errors->first('supplier_name'))


        <span class="text-danger"><small>{{ $errors->first('supplier_name')}}</small></span>

    @endif
</div>
<div class="form-group">
    <label for="address">Address</label>


    {!! Form::text('address',null,['class'=>'form-control','placeholder'=>'Address']) !!}

</div>


<div class="form-group">
    <label for="cnumber">Contact Number</label>


    {!! Form::text('cnumber',null,['class'=>'form-control','placeholder'=>'Contact Number']) !!}


</div>

<div class="form-group">
    <label for="fnumber">Fax Number</label>


    {!! Form::text('fnumber',null,['class'=>'form-control','placeholder'=>'Fax Number']) !!}


</div>
<div class="form-group">
    <label for="email">Email</label>


    {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Email']) !!}

    @if($errors->first('email'))


        <span class="text-danger"><small>{{ $errors->first('email')}}</small></span>

    @endif
</div>
<div class="form-group">
    <label for="email">Webiste</label>


    {!! Form::text('website',null,['class'=>'form-control','placeholder'=>'Website']) !!}

    @if($errors->first('website'))


        <span class="text-danger"><small>{{ $errors->first('website')}}</small></span>

    @endif
</div>

<div class="form-group">
    <label for="email">Contact Person</label>


    {!! Form::text('contact_person',null,['class'=>'form-control','placeholder'=>'Contact Person']) !!}

</div>
<div class="form-group">
      <label for="Desciption">Note</label>
      {!! Form::textarea('note',null,['class'=>'summernote'])!!}
</div>