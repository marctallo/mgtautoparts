@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6">
       <h3 class="content-header"><i class="fa fa-truck"></i> Edit {{ $supplier->supplier_name }}</h3>
        <input type='hidden' id='url' value="{{\Request::url()}}">
   </div>
</div>
<div class="inner-box"> 

   {!! Form::model($supplier,['method'=>'patch','action'=>['SupplierController@update',$supplier->id]]) !!}
      @include('supplier.partials.form')
      <button type="submit" class="btn btn-success">Update</button>
   {!! Form::close() !!}
</div>


@stop