@extends('layout.main')




@section('content')

<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6">
       <h3 class="content-header"><i class="fa fa-truck"></i> New Supplier</h3>
        <input type='hidden' id='url' value="{{\Request::url()}}">
   </div>
</div>
<div class="inner-box"> 

   {!! Form::open(['action'=>'SupplierController@store']) !!}
      @include('supplier.partials.form')
      <button type="submit" class="btn btn-success">Save</button>
   {!! Form::close() !!}
</div>


@stop