@extends('layout.main')




@section('content')


<div class="inner-header row"> 
   <div class="col-md-6 col-sm-6 col-xs-6">
       <h3 class="content-header"><i class="fa fa-truck"></i> Suppliers</h3>
        <input type='hidden' id='url' value="{{url('suppliers')}}">
        <input type='hidden' id='is_trashed' value="1">
   </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        
    </div>
</div>
<input type='hidden' id='url' value="{{\Request::url()}}">
<div class="inner-box" ng-controller="pagingCtrl"> 
   <div class="row">
       <div class="col-md-4 col-md-offset-8 search-box">
           <div class="input-group">
              <input type="text" class="form-control" placeholder="search" ng-model="filter.$">

              <span class="input-group-btn">
                <button class="btn btn-default" type="submit">

                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </button>
              </span>
           </div>
       </div>
   </div>
    <table ng-table="tableParams"  class="table table-striped main-table table-hover">
      <thead class="pointer">
            <tr>
                <th style="width:5%" class="sortable" ng-class="{
                    'sort-asc': tableParams.isSortBy('id', 'asc'),
                    'sort-desc': tableParams.isSortBy('id', 'desc')
                  }"
                    ng-click="tableParams.sorting({'id' : tableParams.isSortBy('id', 'asc') ? 'desc' : 'asc'})">
                    ID
                </th>
                <th style="width:20%" class="sortable" ng-class="{
                    'sort-asc': tableParams.isSortBy('supplier_name', 'asc'),
                    'sort-desc': tableParams.isSortBy('supplier_name', 'desc')
                  }"
                    ng-click="tableParams.sorting({'supplier_name' : tableParams.isSortBy('supplier_name', 'asc') ? 'desc' : 'asc'})">
                    Supplier Name
                </th>
                <th style="width:20%" class="sortable" ng-class="{
                    'sort-asc': tableParams.isSortBy('cnumber', 'asc'),
                    'sort-desc': tableParams.isSortBy('cnumber', 'desc')
                  }"
                    ng-click="tableParams.sorting({'cnumber' : tableParams.isSortBy('cnumber', 'asc') ? 'desc' : 'asc'})">
                    Contact Number
                </th>
                <th style="width:20%" class="sortable" ng-class="{
                    'sort-asc': tableParams.isSortBy('contact_person', 'asc'),
                    'sort-desc': tableParams.isSortBy('contact_person', 'desc')
                  }"
                    ng-click="tableParams.sorting({'contact_person' : tableParams.isSortBy('contact_person', 'asc') ? 'desc' : 'asc'})">
                    Contact Person
                </th>
                <th style="width:20%" class="sortable" ng-class="{
                    'sort-asc': tableParams.isSortBy('fnumber', 'asc'),
                    'sort-desc': tableParams.isSortBy('fnumber', 'desc')
                  }"
                    ng-click="tableParams.sorting({'fnumber' : tableParams.isSortBy('fnumber', 'asc') ? 'desc' : 'asc'})">
                    Fax Number
                </th>
                <th style="width:20%" class="sortable" ng-class="{
                    'sort-asc': tableParams.isSortBy('email', 'asc'),
                    'sort-desc': tableParams.isSortBy('email', 'desc')
                  }"
                    ng-click="tableParams.sorting({'email' : tableParams.isSortBy('email', 'asc') ? 'desc' : 'asc'})">
                    Email
                </th>
                <th style="width:20%" class="sortable" ng-class="{
                    'sort-asc': tableParams.isSortBy('website', 'asc'),
                    'sort-desc': tableParams.isSortBy('website', 'desc')
                  }"
                    ng-click="tableParams.sorting({'email' : tableParams.isSortBy('website', 'asc') ? 'desc' : 'asc'})">
                    Website
                </th>
               
                <th>Option</th>
            </tr>
       </thead>
       <tbody>
          <tr>
               <td colspan="8" class="text-center table-loader tableLoading" ng-hide="$data.length == 0">{!! Html::image('img/defaults/reload.gif')!!}</td>
           </tr>
           <tr class="tableLoading" ng-show="$data.length == 0">
                <td colspan="8" ><div class="text-center">no result found</div></td>
            </tr>
            <tr ng-repeat="supplier in $data" table-loading>
               <td data-title="'ID'" sortable="'id'">
                    @{{supplier.id}}
                </td>
                <td data-title="'Supplier Name'" sortable="'supplier_name'">
                    @{{supplier.supplier_name}}
                </td>
                <td data-title="'Contact Number'" sortable="'cnumber'">
                    @{{supplier.cnumber}}
                </td>
                <td data-title="'Contact Person'" sortable="'contact_person'">
                    @{{supplier.contact_person}}
                </td>
                <td data-title="'Fax Number'" sortable="'fnumber'">
                    @{{supplier.fnumber}}
                </td>
                <td data-title="'Email'" sortable="'email'">
                    @{{supplier.email}}
                </td>
                <td data-title="'Email'" sortable="'email'">
                    @{{supplier.website}}
                </td>
                <td>
                  <form method="POST" action="@{{'trashed/restore/'+supplier.id}}" accept-charset="UTF-8" class="form-delete inline-blocked">
                   <input name="_method" type="hidden" value="PUT">
                   <input name="_token" type="hidden" value="{{ csrf_token() }}">   
                   <i class="fa fa-rotate-right tb-icon" onclick="confirmModal('Restore this supplier?',this)" title="Restore Supplier"></i>
                </form>
                <form method="POST" action="@{{'trashed/delete/'+supplier.id}}" accept-charset="UTF-8" class="form-delete inline-blocked left-margin-5">
                   <input name="_method" type="hidden" value="DELETE">
                   <input name="_token" type="hidden" value="{{ csrf_token() }}">   
                   <i class="fa fa-trash tb-icon delete-btn" onclick="confirmModal('Permanently delete this supplier?',this)" title="Permanent Delete"></i>
                </form>   
                   
               </td>
            </tr>
           
       </tbody>
        
   </table>
   
</div>


@stop