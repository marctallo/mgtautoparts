<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowInactivePerPageBaseMarkupToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
          $table->integer('base_markup');
					$table->integer('per_page');
          $table->enum('show_inactive',array(0,1));
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('base_markup');
						$table->dropColumn('per_page');
						$table->dropColumn('show_inactive');
        });
    }
}
