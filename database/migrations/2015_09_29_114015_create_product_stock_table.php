<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_stock', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('qty');
            $table->integer('supplier_id');
            $table->dateTime('pdate');
            $table->string('ornumber');
            $table->string('order_image');
            $table->integer('product_id')->unsigned();
            $table->double('price');
            $table->text('note');
			$table->timestamps();
            $table->foreign('product_id')
				  ->references('id')
				  ->on('products')
				  ->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_stock');
    }
}
