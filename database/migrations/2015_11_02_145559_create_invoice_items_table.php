<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('product_id');
            $table->integer('invoice_id')->unsigned();
            $table->integer('qty');
            $table->double('unit_price');
            $table->double('tax');
            $table->double('discount');
            $table->timestamps();
            $table->foreign('invoice_id')
				  ->references('id')
				  ->on('invoices')
				  ->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoice_items');
    }
}
