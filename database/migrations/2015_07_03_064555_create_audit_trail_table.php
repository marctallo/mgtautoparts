<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTrailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_trails',function(Blueprint $table){
        
            $table->increments('id');
            $table->string('action');
            $table->integer('user_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->string('section');
			$table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_trails');
    }
}
