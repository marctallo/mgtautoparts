<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEngineAndChasisTypeToCarModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_model', function (Blueprint $table) {
            
          $table->string('engine');
          $table->string('chasis_type');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('car_model', function (Blueprint $table) {
            $table->dropColumn('engine');
            $table->dropColumn('chasis_type');
        });
    }
}
