<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('supplier_name');
			$table->text('address');
            $table->string('cnumber');
            $table->string('contact_person');
            $table->string('fnumber');
            $table->string('website');
            $table->string('email');
            $table->text('note');
            $table->softDeletes();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suppliers');
    }
}
