<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('pcode');
            $table->string('pname');
            $table->string('unit');
            $table->string('maker');
            $table->integer('model_id');
            $table->string('year');
            $table->string('section');
            $table->string('shelf');
            $table->string('binloc');
            $table->string('image_file');
			$table->text('description');
            $table->integer('cat_id');
            $table->integer('supplier_id');
            $table->integer('min_stock');
            $table->integer('current_stock');
            $table->integer('max_stock');
            $table->integer('reorder_point');
            $table->double('wholesale_price');
            $table->double('retail_price');
            $table->integer('is_active');
            $table->softDeletes();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
