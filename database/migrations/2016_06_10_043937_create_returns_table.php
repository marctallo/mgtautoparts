<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('item_returns', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('p_id');
			$table->double('price');
			$table->date('return_date');
			$table->text('return_reason');
			$table->integer('qty');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item_returns');
    }
}
