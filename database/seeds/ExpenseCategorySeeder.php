<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\ExpenseCategory;



class ExpenseCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        
        ExpenseCategory::create(['cname' => 'Electricity']);
        ExpenseCategory::create(['cname' => 'Salary']);
        ExpenseCategory::create(['cname' => 'Rent']);
        ExpenseCategory::create(['cname' => 'Transportation']);
        
		$this->command->info('Users table seeded!');

        Model::reguard();
    }
}

