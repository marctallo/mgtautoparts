<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'company_name' => "MGT Auto Supply and General Merchandise",
						'address' => 'Antipolo City',
						'number' => '1234567',
						'email'	=> 'mgtautosupplyshop@gmail.com'
        ]);
    }
}
