<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\User;



class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('UserTableSeeder');
		$this->command->info('Users table seeded!');

        Model::reguard();
    }
}


class UserTableSeeder extends Seeder {
    	
	private $username = 'admin';
	private $password = 'q1q1q1';
	private $role = 3;

    public function run()
    {
        
        User::create(['username' => $this->username,'password'=>$this->password,'role'=>$this->role]);
    }

}
