<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Reminder extends Model
{
    
	protected $table = "reminders";
	
	protected $fillable = [
		
		"user_id",
		"title",
		"start",
		"end"
	];
	
	
	public static function getReminders($id){
		
		$reminders = DB::table("reminders")
					->where("user_id","=",$id)
					->select(["title","start","end","id"])
					->get();
		
		return $reminders;
	}
}
