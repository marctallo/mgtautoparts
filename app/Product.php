<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Setting;

class Product extends BaseModel
{
	 use SoftDeletes;


	protected $table = 'products';


	protected $fillable = [
			'pcode', 
			'pname', 
			'unit',
			'maker',
			'section',
			'shelf',
			'binloc',
			'year',
			'description',
			'cat_id',
			'min_stock',
			'max_stock',
			'current_stock',
			'reorder_point',
			'wholesale_price',
			'retail_price',
			'supplier_id',
			'is_active',
			'image_file',
			'application',
			'is_updated',
			'initial_stock'
	];
    
    
    
	public function scopeGetProduct($query){


			$query->leftJoin('categories','products.cat_id','=','categories.id')
						->select([
								'categories.cname',
								'products.id',
								'products.pcode',
								'products.pname',
								'products.maker',
								'products.unit',
								'products.retail_price',
								'products.current_stock',
								'products.year',
								'products.is_updated'
							]);



	}
  
 
    
    public function scopeGetId($query){
    
         $query->where('id','=',1);
    
    }
    
    public function scopeGetProductByCategory($query,$cat_id){
    
        $query->where('products.cat_id','=',$cat_id)
              ->where('is_active','=',1);
    }
	
	public function scopeGetProductByCode($query,$pcode){
		
		$query->where('products.pcode','=',$pcode);
	}
    
	public static function getAll($trash = false){
		
			$settings = Setting::findOrFail(1);

			$q = DB::table('products')
						->leftJoin('categories','products.cat_id','=','categories.id')
						->select([
								'categories.cname',
								'products.id',
								'products.pcode',
								'products.pname',
								'products.description',
								'products.maker',
								'products.unit',
								'products.retail_price',
								'products.wholesale_price',
								'products.current_stock',
								'products.reorder_point',
								'products.year',
								'products.image_file',
								'products.shelf',
								'products.application',
								'products.is_updated'
			]);
		
			if($settings->show_inactive == 0){
				$q->where('is_active','=',1);
			}

			if($trash == true){

					$q->whereNotNull('products.deleted_at');
			}else{

					$q->whereNull('products.deleted_at');
			}
			return $q;

	}
	
	public static function getLowStockItems(){
		
		$q = DB::table('products')
					->leftJoin('categories','products.cat_id','=','categories.id')
					->leftJoin('suppliers','products.supplier_id','=','suppliers.id')
					->where('is_active','=',"1")
					->where('is_updated','=',"1")
					->whereRaw('current_stock <= reorder_point')
					->select([
								'categories.cname',
								'suppliers.supplier_name',
								'products.id',
								'products.pcode',
								'products.maker',
								'products.current_stock',
								'products.reorder_point',
								'products.is_updated',
								'products.application'
			]);
		
			
		return $q;		
	}
    
	public static function getSingle($id){

			$q = DB::table('products')
						->leftJoin('categories','products.cat_id','=','categories.id')
						->leftJoin('suppliers','products.supplier_id','=','suppliers.id')
						->where('products.id','=',$id)
						->select([
								'categories.cname',
								'products.id',
								'products.pcode',
								'products.pname',
								'products.maker',
								'products.section',
								'products.binloc',
								'products.shelf',
								'products.description',
								'products.unit',
								'products.min_stock',
								'products.max_stock',
								'products.wholesale_price',
								'products.retail_price',
								'products.current_stock',
								'products.reorder_point',
								'products.year',
								'products.updated_at',
								'products.image_file',
								'products.application',
								'suppliers.supplier_name',
								'products.initial_stock',
								'products.created_at',
								'products.updated_at'
			]);

			return $q->first();
	}

	public static function getStockList($id){

			$q = DB::table('product_stock')
							->leftJoin('suppliers','product_stock.supplier_id','=','suppliers.id')
							->where('product_stock.product_id','=',$id)
							->orderBy('pdate','desc')
							->select([
									'product_stock.id',
									'product_stock.pdate',
									'product_stock.qty',
									'product_stock.ornumber',
									'product_stock.order_image',
									'suppliers.supplier_name',
									'product_stock.price',
									'product_stock.supplier_id'
							])
							->get();

			return $q;
    }
    
	public static function getStockSingle($id){

			$q = DB::table('product_stock')
							->leftJoin('suppliers','product_stock.supplier_id','=','suppliers.id')
							->where('product_stock.id','=',$id)
							->select([
									'product_stock.id',
									'product_stock.pdate',
									'product_stock.qty',
									'product_stock.ornumber',
									'product_stock.order_img',
									'suppliers.supplier_name',
									'product_stock.price'
							])
							->first();

			return $q;    

	}



	public static function getCriticalItems($total = 5){

		$q = self::select('maker','pcode','current_stock','reorder_point','id')
				 ->whereRaw('current_stock <= reorder_point')
				 ->orderBy('current_stock','asc')
				 ->take($total);



		return $q->get();
  }
	
	public function scopeGetProductSaleHistory($query,$id){
		
		$query->leftJoin('invoice_items','products.id','=','invoice_items.product_id')
					->leftJoin('invoices','invoice_items.invoice_id','=','invoices.id')
					->where('products.id','=',$id)
					->orderBy('invoices.date','desc')
					->select([
						'invoices.date',
						'invoice_items.qty',
						'invoice_items.unit_price'
						
					]);
	}
	
}
