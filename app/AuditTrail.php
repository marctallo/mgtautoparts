<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTrail extends Model
{
    protected $table = 'audit_trails';
    
    
    protected $fillable = ['action', 'user_id', 'item_id','section'];
    
    private $table_reference = [
        'user'=>[
            'table'=>'users',
            'column'=>'username'
        ]
    ];
    
    
    public function scopeGetTrail($query,$id){
		
		$query->where('user_id','=',$id)->orderBy('created_at','desc');
	}
}
