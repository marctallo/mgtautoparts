<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    
    
    protected $fillable = [
        'customer', 
        'date', 
        'invoice_number'
    ];
    
    
    public static function getAll(){
    
        $q = self::select(
            [
                 'id',
                  'invoice_number',
                  'date'
            ]
        );
        
        
        return $q;
    
    }
   
    public function items(){
    
        return $this->hasMany('App\InvoiceItems');
    }
    
    
}
