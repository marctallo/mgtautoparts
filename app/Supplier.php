<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends BaseModel
{
    
    use SoftDeletes;
    
    protected $table = 'suppliers';

 
    protected $fillable = [
        'supplier_name', 
        'address', 
        'cnumber',
        'fnumber',
        'email',
        'contact_person',
        'note',
        'website'
    ];
}
