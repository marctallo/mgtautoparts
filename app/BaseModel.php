<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {



	public static function setToSelect($column,$default = 'Choose option')
    {
		
		$arr= array();
		
		$arr[0] = $default;
		
		foreach(self::all() as $k){
			
			$arr[$k->id] = $k->$column;
		}
        
        asort($arr);
        
        moveToTop($arr,0);//move default to first index
        
      // dd($arr);
        
        
        
		return $arr;
	}
    
    
    public static function getTrashedSingle($id){
    
        return self::onlyTrashed()->where('id','=',$id)->first();
    }

}
