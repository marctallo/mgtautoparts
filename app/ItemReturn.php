<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ItemReturn extends Model
{
    protected $table = 'item_returns';

 
    protected $fillable = [
        'p_id', 
        'price', 
        'return_date',
        'return_reason',
		'qty',
			'purchase_date'
    ];
	
	public static function getAll(){
    
        $q = self::leftJoin('products','item_returns.p_id','=','products.id')
			->select([
                 'item_returns.id',
                  'item_returns.p_id',
                  'item_returns.qty',
				  'item_returns.price',
				  'item_returns.return_date',
				  'item_returns.return_reason',
				  'products.pcode',
					'item_returns.purchase_date'
            ]
        );
        
        
        return $q;
    
    }
	
	public static function getDailyReturns(){
		
		$q = self::select(DB::raw('SUM(item_returns.price * item_returns.qty) as total'),DB::raw('WEEKDAY(item_returns.purchase_date)+1 as day'),DB::raw('SUM((item_returns.price - products.wholesale_price) * item_returns.qty) as gross'))
             ->leftJoin('products','item_returns.p_id','=','products.id')
             ->whereRaw('YEARWEEK(item_returns.purchase_date,1) = YEARWEEK(NOW(),1)')
             ->groupBy('item_returns.purchase_date');
        
    return $q->get();
	}
}
