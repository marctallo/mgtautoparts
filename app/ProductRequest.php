<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductRequest extends Model
{
     protected $table = 'product_request';

   
    protected $fillable = ['product', 'description'];
    
    public static function getMostRequestedProduct($total = 5){
        
        $q = self::select('product','id',DB::raw('COUNT(product) as total'))
                ->groupBy('product')
                ->take($total);
        
        return $q->get();
    }
}
