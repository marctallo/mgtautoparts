<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarModel extends BaseModel
{
    use SoftDeletes;
    
    protected $table = 'car_model';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cmodel', 'description','engine','chasis_type','year'];
}
