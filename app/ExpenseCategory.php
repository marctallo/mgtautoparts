<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseCategory extends BaseModel
{
    
    protected $table = 'expenses_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cname'];
}
