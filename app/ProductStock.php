<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends BaseModel
{
    protected $table = 'product_stock';

 
    protected $fillable = [
        'qty', 
        'supplier_id', 
        'product_id',
        'note',
        'ornumber',
        'pdate',
        'order_img',
        'price'
    ];
}
