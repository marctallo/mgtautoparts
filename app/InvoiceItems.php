<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class InvoiceItems extends Model
{
    protected $table = 'invoice_items';
    
    
    protected $fillable = [
        'product_id', 
        'qty', 
        'unit_price',
        'invoice_id',
        'wholesale_price'
    ];
    
    public static function getAll($id){
    
    
        $q = self::leftJoin('products','invoice_items.product_id','=','products.id')
             ->where('invoice_id','=',$id)
             ->select([
                'products.pcode',
                'products.pname',
                'invoice_items.qty',
                'invoice_items.unit_price'  
             ]);
        
        
        return $q->get();
    }
    
   
    public static function getTopProducts($total = 5){
        
        $q = self::select('products.maker','products.pcode', DB::raw('SUM(qty) as qty'),'products.id')
                ->leftJoin('products','invoice_items.product_id','=','products.id')
                ->groupBy('product_id')
                ->orderBy('qty', 'desc')
                ->take($total);
        
        return $q->get();
    }
    
    public static function getDailySales(){
    
        $q = self::select(DB::raw('SUM(invoice_items.unit_price * invoice_items.qty) as total'),DB::raw('WEEKDAY(invoices.date)+1 as day'),DB::raw('SUM((invoice_items.unit_price - invoice_items.wholesale_price) * invoice_items.qty) as gross'))
             ->leftJoin('invoices','invoice_items.invoice_id','=','invoices.id')
             ->leftJoin('products','invoice_items.product_id','=','products.id')
             ->whereRaw('YEARWEEK(invoices.date,1) = YEARWEEK(NOW(),1)')
             ->groupBy('invoices.date')
             ->orderBy('invoices.date');
        
        return $q->get();
    }
    
    public static function getMonthlySales(){
    
        /*$q = self::select(DB::raw('MONTH(invoices.date) as month'),DB::raw('SUM(invoice_items.unit_price * invoice_items.qty) as total'))
             ->leftJoin('invoices','invoice_items.invoice_id','=','invoices.id')
             ->leftJoin('products','invoice_items.product_id','=','products.id')
             ->whereRaw('YEAR(invoices.date) = YEAR(NOW())')
             ->groupBy(DB::raw('MONTH(invoices.date)'))
             ->orderBy('invoices.date');
        
        
        return $q->get();*/
        
        $q = self::select(
            DB::raw('SUM(IF(month(invoices.date) = 1,invoice_items.unit_price*invoice_items.qty,0)) as "january"'),
            DB::raw('SUM(IF(month(invoices.date) = 2,invoice_items.unit_price*invoice_items.qty,0)) as "february"'),
            DB::raw('SUM(IF(month(invoices.date) = 3,invoice_items.unit_price*invoice_items.qty,0)) as "march"'),
            DB::raw('SUM(IF(month(invoices.date) = 4,invoice_items.unit_price*invoice_items.qty,0)) as "april"'),
            DB::raw('SUM(IF(month(invoices.date) = 5,invoice_items.unit_price*invoice_items.qty,0)) as "may"'),
            DB::raw('SUM(IF(month(invoices.date) = 6,invoice_items.unit_price*invoice_items.qty,0)) as "june"'),
            DB::raw('SUM(IF(month(invoices.date) = 7,invoice_items.unit_price*invoice_items.qty,0)) as "july"'),
            DB::raw('SUM(IF(month(invoices.date) = 8,invoice_items.unit_price*invoice_items.qty,0)) as "august"'),
            DB::raw('SUM(IF(month(invoices.date) = 9,invoice_items.unit_price*invoice_items.qty,0)) as "september"'),
            DB::raw('SUM(IF(month(invoices.date) = 10,invoice_items.unit_price*invoice_items.qty,0)) as "october"'),
            DB::raw('SUM(IF(month(invoices.date) = 11,invoice_items.unit_price*invoice_items.qty,0)) as "november"'),
            DB::raw('SUM(IF(month(invoices.date) = 12,invoice_items.unit_price*invoice_items.qty,0)) as "december"'),
            DB::raw('SUM(IF(month(invoices.date) = 1,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "january_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 2,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "february_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 3,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "march_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 4,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "april_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 5,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "may_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 6,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "june_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 7,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "july_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 8,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "august_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 9,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "september_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 10,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "october_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 11,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "november_gross"'),
            DB::raw('SUM(IF(month(invoices.date) = 12,(invoice_items.unit_price-invoice_items.wholesale_price)*invoice_items.qty,0)) as "december_gross"') 
        )->leftJoin('invoices','invoice_items.invoice_id','=','invoices.id')
         ->leftJoin('products','invoice_items.product_id','=','products.id')
         ->whereRaw('YEAR(invoices.date) = YEAR(NOW())');
        
        return $q->first();    
    }
    
    
    public static function getTotalSales($type,$previous = false){
        
         /*SELECT
            SUM(ii.unit_price * ii.qty) as "total"
        FROM invoice_items ii
        LEFT JOIN 
            invoices i
        ON ii.invoice_id = i.id
        LEFT JOIN
            products p
        ON ii.product_id = p.id
        WHERE YEAR(i.date) = YEAR(NOW())*/
    
        
        $prev = ($previous == false) ? "" : "-1";
    
        
        
        $q = self::select(DB::raw('SUM(invoice_items.unit_price * invoice_items.qty) as total'))
             ->leftJoin('invoices','invoice_items.invoice_id','=','invoices.id')
             ->leftJoin('products','invoice_items.product_id','=','products.id');
        
        
        
        
        switch($type){
        
        
            case "day":
            
                $q->whereRaw("YEAR(invoices.date) = YEAR(NOW())")
                  ->whereRaw("MONTH(invoices.date) = MONTH(NOW())")
                  ->whereRaw("DAY(invoices.date) = DAY(NOW())$prev");
                
            break;
                
            case "week":    
            
                $q->whereRaw("YEAR(invoices.date) = YEAR(NOW())")
                  ->whereRaw("MONTH(invoices.date) = MONTH(NOW())")
                  ->whereRaw("YEARWEEK(invoices.date) = YEARWEEK(NOW())$prev");
            break;
            
            case "month":
            
                $q->whereRaw("YEAR(invoices.date) = YEAR(NOW())")
                  ->whereRaw("MONTH(invoices.date) = MONTH(NOW())$prev");
            
            break;
            
            
            case "year":
            
                  $q->whereRaw("YEAR(invoices.date) = YEAR(NOW())$prev");
            
            break;
            
            default:
        }
        
        return $q->first();
    }
 
    
}
