<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;
use App\User;
use App\Custom\Observer\UserObserver;
use App\Category;
use App\Custom\Observer\CategoryObserver;
use App\Product;
use App\Custom\Observer\ProductObserver;
use App\Supplier;
use App\Custom\Observer\SupplierObserver;
use App\Expense;
use App\Custom\Observer\ExpenseObserver;
use App\CarModel;
use App\Custom\Observer\CarModelObserver;
use App\ProductStock;
use App\Custom\Observer\ProductStockObserver;
use App\Invoice;
use App\Custom\Observer\InvoiceObserver;
use App\ProductRequest;
use App\Custom\Observer\ProductRequestObserver;
use App\ItemReturn;
use App\Custom\Observer\ItemReturnObserver;
use App\Setting;
use App\Custom\Observer\SettingsObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        Validator::extend('dropdown', function($attribute, $value, $parameters) {
            
            
            if($value == '0'){
                
                return false;
            }
            
            return true;
        });
        
        User::observe(new UserObserver());
        Category::observe(new CategoryObserver());
        Product::observe(new ProductObserver());
        Supplier::observe(new SupplierObserver());
        Expense::observe(new ExpenseObserver());
        CarModel::observe(new CarModelObserver());
        ProductStock::observe(new ProductStockObserver());
        Invoice::observe(new InvoiceObserver());
        ProductRequest::observe(new ProductRequestObserver());
				ItemReturn::observe(new ItemReturnObserver());
				Setting::observe(new SettingsObserver());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Custom\Repository\BaseRepositoryInterface','App\Custom\Repository\UserRepository');
    }
}
