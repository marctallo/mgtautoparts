<?php namespace App\Custom\Repository;

interface BaseRepositoryInterface {
	
	public function store($request);
	
	public function find($id);
	
	public function update($id,$request);
	
	public function destroy($id);
	
	public function getList($trash , $id);
	
	public function permanentDelete($id);
	
	public function restore($id);
}