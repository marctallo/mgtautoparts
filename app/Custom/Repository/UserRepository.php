<?php namespace App\Custom\Repository;

use App\User;

class UserRepository implements BaseRepositoryInterface {
	
	public function store($request){
		
		return User::create($request->all());
	}
	
	public function find($id){
		
		return User::findOrFail($id);
	}
	
	public function update($id, $request){
		
		$user = User::findOrFail($id);
		
		$user->update($request->all());
	}
	
	public function destroy($id){
		
		$user = User::findOrFail($id);
        
    $user->delete();
	}
	
	public function getList($trash , $id){
		
		if($trash == 0){
            
			return User::where('id','!=',$id)->orderBy('created_at','desc')->get();
    }
            
		return User::onlyTrashed()->get();
  
	}
	
	public function permanentDelete($id){
		
		$user = User::getTrashedSingle($id);
    
    $user->forceDelete();
	}
	
	public function restore($id){
		
		$user = User::getTrashedSingle($id);
        
    $user->restore();
	}
	
}
