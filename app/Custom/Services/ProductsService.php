<?php namespace App\Custom\Services;

use App\InvoiceItems;
use App\ItemReturn;

class ProductsService {
		
	public function getTopSelling(){
		
		return InvoiceItems::getTopProducts();
	}
	
}