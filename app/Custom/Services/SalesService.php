<?php namespace App\Custom\Services;

use App\InvoiceItems;
use App\ItemReturn;

class SalesService {
		
	
	public function getTotal(){
		
		return [
            
			'today' => InvoiceItems::getTotalSales('day'),
			'yesterday' => InvoiceItems::getTotalSales('day',true),
			'this_week' => InvoiceItems::getTotalSales('week'),
			'last_week' => InvoiceItems::getTotalSales('week',true),
			'this_month' => InvoiceItems::getTotalSales('month'),
			'last_month' => InvoiceItems::getTotalSales('month',true),
			'this_year' => InvoiceItems::getTotalSales('year'),
			'last_year' => InvoiceItems::getTotalSales('year',true)
            
     ];
	}
	
	
	public function getDaily(){
		
		$sales = InvoiceItems::getDailySales();
		$returns = ItemReturn::getDailyReturns();
		$sales_array = [];
		$gross_array = [];
		$return_price = [];
		$return_gross = [];

		$max = 7;

		for($i = 0; $i < count($sales); $i++){


				$sales_array[$sales[$i]['day'] -1] = $sales[$i]['total'];
				$gross_array[$sales[$i]['day'] -1] = $sales[$i]['gross'];

		}

		for($i = 0; $i < count($returns); $i++){


				$return_price[$returns[$i]['day'] -1] = $returns[$i]['total'];
				$return_gross[$returns[$i]['day'] -1] = $returns[$i]['gross'];

		}


		for($i = 0; $i<$max; $i++){


				 if (!array_key_exists($i,$sales_array)) {
						$sales_array[$i] = 0;
				 }

				 if (!array_key_exists($i,$gross_array)) {
						$gross_array[$i] = 0;
				 }

				if (!array_key_exists($i,$return_price)) {
						$return_price[$i] = 0;
				 }

				 if (!array_key_exists($i,$return_gross)) {
						$return_gross[$i] = 0;
				 }

		}


		ksort($sales_array);
		ksort($gross_array);
		ksort($return_price);
		ksort($return_gross);

		for($i = 0 ; $i < $max; $i++){

			$sales_array[$i] = $sales_array[$i] - $return_price[$i];
			$gross_array[$i] = $gross_array[$i] - $return_gross[$i];
		}
		
		return [$sales_array,$gross_array];
	}
	
	public function getMonthly(){
		
		$sales = InvoiceItems::getMonthlySales();
		
		$sales_array = array(
				$sales->january,
				$sales->february,
				$sales->march,
				$sales->april,
				$sales->may,
				$sales->june,
				$sales->july,
				$sales->august,
				$sales->september,
				$sales->october,
				$sales->november,
				$sales->december    
		);

		$gross_array =  array(
				$sales->january_gross,
				$sales->february_gross,
				$sales->march_gross,
				$sales->april_gross,
				$sales->may_gross,
				$sales->june_gross,
				$sales->july_gross,
				$sales->august_gross,
				$sales->september_gross,
				$sales->october_gross,
				$sales->november_gross,
				$sales->december_gross    
		);
		
		return [$sales_array,$gross_array];
	}
	
	
}