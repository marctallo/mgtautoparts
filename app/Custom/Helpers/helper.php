<?php

function show_pagination_links($data,$fields){
    
    

    return str_replace('/?', '?', $data->appends($fields)->render());

}

function sort_header($column,$text,$fields,$page){
  
  $fields['sort'] = $column;
   return link_to_action($page,$text,$fields);
  
}



function display_stock_status($current,$reorder){


    if($current <= $reorder){
    
        return "<span class='label label-danger'>".$current."</span>";
    }
    
    return "<span class='label label-info'>".$current."</span>";
}


function upload_file($request,$filename,$path){
    
    $file = $request->file($filename);

    if ($request->hasFile($filename)) {
            
            if ($file->isValid()){
               
                
                
                $destinationPath = $path;
                $filename = uniqid();
                $ext = $file->getClientOriginalExtension();
                $newfile = $filename.".".$ext;
           
                $file->move($destinationPath, $newfile);
                
                return $newfile;
                
            }
    }
    
    return null;
}

#shows the item depending on the user role


function showItem($role = array()){

    $user_role = \Auth::user()->role;
    
    if(in_array($user_role,$role)){
    
        return true;
    }
    
    return false;
}

function showAlertBox($message,$type = 'info'){

    
    
    
    $body = "<div class='alert alert-".$type." alert-dismissible message-box' role='alert' id='alert-message-box'>
                <span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>".$message."</div>";

    \Session::flash('alertmessage',$body);
    
    return true;

}

function moveToTop(&$array, $key) {
    $temp = array($key => $array[$key]);
    unset($array[$key]);
    $array = $temp + $array;
}

function monthList(){

    return [
        
            '0' => 'Select a month',
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        
        ];

}

function getTotal($price,$qty,$format = false){
	//dd($price);
	$total = $price * $qty;
	
	if($format == true){
		return number_format($total, 2, '.', '');
	}
	return number_format($total,2);
}

function setMarkup($price,$markup){
	
	$retail_price = ($price * $markup) + $price;
	
	
	return number_format($retail_price,2);
	
}
                        
                        
                  