<?php namespace Custom\Csv;

use App\Http\Requests\Request;

class Csv{
    
 
    
    public function toArray($filename='', $delimiter=','){
        
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = '';
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = array_map('trim',$row);
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }
    
    
    
    public function arrayDownload($array, $delimiter=","){
    
       header('Content-Type: text/csv; charset=utf-8');
       header('Content-Disposition: attachment; filename=errors-'.uniqid().'.csv');
        
        $f = fopen('php://output','w');
        
     
        $headers = array(
            'Row',
            'Product Code',
            'Maker',
            'Minimum Stock',
            'Maximum Stock',
            'Current Stock',
            'Wholesale Price',
            'Retail Price'
        
        );
        fputcsv($f, $headers, $delimiter);
        
        foreach ($array as $line) {
            fputcsv($f, $line, $delimiter);
        }
        
        fclose($f);

        echo $f;
    
    }
    
    

}