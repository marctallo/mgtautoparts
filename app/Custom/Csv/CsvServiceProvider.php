<?php namespace Custom\Csv;

use Illuminate\Support\ServiceProvider;

class CsvServiceProvider extends ServiceProvider
{
    
 
    public function register()
    {
        $this->app->bind('csv', function ($app) {
            return new Csv();
        });
    }
}
