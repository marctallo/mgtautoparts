<?php namespace Custom\Audit;

use Illuminate\Support\ServiceProvider;

class AuditServiceProvider extends ServiceProvider
{
    
 
    public function register()
    {
        $this->app->bind('audit', function ($app) {
            return new Audit();
        });
    }
}
