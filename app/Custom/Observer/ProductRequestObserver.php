<?php namespace App\Custom\Observer;

use Session;

class ProductRequestObserver{


    public function created($model)
    {
        showAlertBox('A new product request has been added.','success');
        
        $data = [
        
            'action'    => 'create',
            'item_id'   => $model->id,
            'section'   => 'product request'
        ];
        //dd($model);
        \Audit::log($data);
    }
    
    
    public function updated($model){
        
        showAlertBox('Product request has been updated','info');
        
        $data = [
        
            'action'    => 'update',
            'item_id'   => $model->id,
            'section'   => 'product request'
        ];
        //dd($model);
        \Audit::log($data);
    
    }
    
    public function deleted($model){
    
        showAlertBox('Product request has been deleted.','danger');
        
        $data = [
        
            'action'    => 'delete',
            'item_id'   => $model->id,
            'section'   => 'product request'
        ];
        //dd($model);
        \Audit::log($data);
    
    }

}