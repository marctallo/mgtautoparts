<?php namespace App\Custom\Observer;

use Session;

class ExpenseObserver{


    public function created($model)
    {
        showAlertBox('A new expense has been added.','success');
        
        $data = [
        
            'action'    => 'create',
            'item_id'   => $model->id,
            'section'   => 'expense'
        ];
        //dd($model);
        \Audit::log($data);
    }
    
    
    public function updated($model){
        
        showAlertBox('Expense has been updated','info');
    
        
        $data = [
        
            'action'    => 'update',
            'item_id'   => $model->id,
            'section'   => 'expense'
        ];
        //dd($model);
        \Audit::log($data);
    
    }
    
    public function deleted($model){
    
        showAlertBox('Expense has been deleted.','danger');
        
        $data = [
        
            'action'    => 'delete',
            'item_id'   => $model->id,
            'section'   => 'expense'
        ];
        //dd($model);
        \Audit::log($data);
    
    }

}