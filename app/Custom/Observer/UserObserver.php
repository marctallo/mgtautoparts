<?php namespace App\Custom\Observer;

use Session;

class UserObserver{


    public function created($model)
    {
        showAlertBox('A new user has been added.','success');
        
        $data = [
        
            'action'    => 'create',
            'item_id'   => $model->id,
            'section'   => 'user'
        ];
        //dd($model);
        \Audit::log($data);
    }
    
    
    public function updated($model){
        
        $old = $model->getOriginal();
        
     
        if($old['last_login'] == $model->last_login){
        
            showAlertBox('User has been updated','info');
        }
        
        
        $data = [
        
            'action'    => 'update',
            'item_id'   => $model->id,
            'section'   => 'user'
        ];
        //dd($model);
        \Audit::log($data);
    
    }
    
    public function deleted($model){
    
        showAlertBox('User has been deleted.','danger');
        
        $data = [
        
            'action'    => 'delete',
            'item_id'   => $model->id,
            'section'   => 'user'
        ];
        //dd($model);
        \Audit::log($data);
    
    }

}