<?php namespace App\Custom\Observer;

use Session;

class SupplierObserver{


    public function created($model)
    {
        showAlertBox('A new supplier has been added.','success');
        
        $data = [
        
            'action'    => 'create',
            'item_id'   => $model->id,
            'section'   => 'supplier'
        ];
        //dd($model);
        \Audit::log($data);
    }
    
    
    public function updated($model){
        
        showAlertBox('Supplier has been updated','info');
    
        
        $data = [
        
            'action'    => 'update',
            'item_id'   => $model->id,
            'section'   => 'supplier'
        ];
        //dd($model);
        \Audit::log($data);
    
    }
    
    public function deleted($model){
    
        showAlertBox('Supplier has been deleted.','danger');
        
        $data = [
        
            'action'    => 'delete',
            'item_id'   => $model->id,
            'section'   => 'supplier'
        ];
        //dd($model);
        \Audit::log($data);
    
    }

}