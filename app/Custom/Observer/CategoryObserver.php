<?php namespace App\Custom\Observer;

use Session;

class CategoryObserver{


    public function created($model)
    {
        showAlertBox('A new category has been added.','success');
        
        $data = [
        
            'action'    => 'create',
            'item_id'   => $model->id,
            'section'   => 'category'
        ];
        //dd($model);
        \Audit::log($data);
    }
    
    
    public function updated($model){
        
        showAlertBox('Category has been updated','info');
        
        $data = [
        
            'action'    => 'update',
            'item_id'   => $model->id,
            'section'   => 'category'
        ];
        //dd($model);
        \Audit::log($data);
    
    }
    
    public function deleted($model){
    
        showAlertBox('Category has been deleted.','danger');
        
        $data = [
        
            'action'    => 'delete',
            'item_id'   => $model->id,
            'section'   => 'category'
        ];
        //dd($model);
        \Audit::log($data);
    
    }

}