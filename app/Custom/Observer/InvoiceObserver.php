<?php namespace App\Custom\Observer;

use Session;

class InvoiceObserver{


    public function created($model)
    {
        showAlertBox('A new invoice has been added.','success');
        
        $data = [
        
            'action'    => 'create',
            'item_id'   => $model->id,
            'section'   => 'invoice'
        ];
        //dd($model);
        \Audit::log($data);
    }
    
    
    public function updated($model){
        
        showAlertBox('Invoice has been updated','info');
    
        
        $data = [
        
            'action'    => 'update',
            'item_id'   => $model->id,
            'section'   => 'invoice'
        ];
        //dd($model);
        \Audit::log($data);
    
    }
    
    public function deleted($model){
    
        showAlertBox('Invoice has been deleted.','danger');
        
        $data = [
        
            'action'    => 'delete',
            'item_id'   => $model->id,
            'section'   => 'invoice'
        ];
        //dd($model);
        \Audit::log($data);
    
    }

}