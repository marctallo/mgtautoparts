<?php namespace App\Custom\Observer;

use Session;

class ItemReturnObserver{


    public function created($model)
    {
        showAlertBox('A new item return has been added.','success');
        
        $data = [
        
            'action'    => 'create',
            'item_id'   => $model->id,
            'section'   => 'item return'
        ];
        //dd($model);
        \Audit::log($data);
    }
    

    
    public function deleted($model){
    
        showAlertBox('Item return has been deleted.','danger');
        
        $data = [
        
            'action'    => 'delete',
            'item_id'   => $model->id,
            'section'   => 'item return'
        ];
        //dd($model);
        \Audit::log($data);
    
    }

}