<?php namespace App\Custom\Observer;

use Session;

class CarModelObserver{


    public function created($model)
    {
        showAlertBox('A new car model has been added.','success');
        
        $data = [
        
            'action'    => 'create',
            'item_id'   => $model->id,
            'section'   => 'car model'
        ];
        //dd($model);
        \Audit::log($data);
    }
    
    
    public function updated($model){
        
        showAlertBox('Car Model has been updated.','info');
        
        $data = [
        
            'action'    => 'update',
            'item_id'   => $model->id,
            'section'   => 'car model'
        ];
        //dd($model);
        \Audit::log($data);
    
    }
    
    public function deleted($model){
    
        showAlertBox('Car Model has been deleted.','danger');
        
        $data = [
        
            'action'    => 'delete',
            'item_id'   => $model->id,
            'section'   => 'car model'
        ];
        //dd($model);
        \Audit::log($data);
    
    }

}