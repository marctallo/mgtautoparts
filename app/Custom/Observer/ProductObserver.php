<?php namespace App\Custom\Observer;

use Session;

class ProductObserver{


    public function created($model)
    {
        showAlertBox('A new product has been added.','success');
        
        $data = [
        
            'action'    => 'create',
            'item_id'   => $model->id,
            'section'   => 'product'
        ];
        //dd($model);
        \Audit::log($data);
    }
    
    
    public function updated($model){
        
        showAlertBox('Product has been updated','info');
    
        
        $data = [
        
            'action'    => 'update',
            'item_id'   => $model->id,
            'section'   => 'product'
        ];
        //dd($model);
        \Audit::log($data);
    
    }
    
    public function deleted($model){
    
        showAlertBox('Product has been deleted.','danger');
        
        $data = [
        
            'action'    => 'delete',
            'item_id'   => $model->id,
            'section'   => 'product'
        ];
        //dd($model);
        \Audit::log($data);
    
    }

}