<?php namespace App\Custom\Observer;

use Session;

class SettingsObserver{



    
    public function updated($model){
        
        showAlertBox('Settings has been updated','info');
    
        
        $data = [
        
            'action'    => 'update',
            'item_id'   => $model->id,
            'section'   => 'settings'
        ];
        //dd($model);
        \Audit::log($data);
    
    }

}