<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         $rules = [
			'cname'   => 'required|unique:categories,cname',
		
		];
        
        $id = Request::segment(3);
		

		if(Request::isMethod('patch')){
			
			
			$rules['cname'] = 'required|unique:categories,cname,'.$id;
       
			
		}
        
        return $rules;
            
    }
    
    public function messages()
    {
    
        return [
            'cname.required'    => 'Category name is required.',
            'cname.unique'      => 'Category already exist.'
        ];
    }
}
