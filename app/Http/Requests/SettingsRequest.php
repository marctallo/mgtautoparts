<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SettingsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
			
			$rules = [
				
				'company_name' => 'required',
				'address'	=> 'required',
				'email'	=> 'email',
				'base_markup' => 'required|numeric',
				'per_page'	=> 'required|numeric'
				
			];
			
			return $rules;
    }
	
		public function messages()
		{
			
			return [
	        'company_name.required'     => 'Company name is required.',
	        'address.unique'       => 'Address is required.',
          'email.email'   => 'You must enter a valid email.',
					'base_markup.required' => 'Base Markup is required.',
					'base_markup.number' => 'Base Markup must numeric',
					'per_page.required' => 'Products per page is required',
					'per_page.numeric'	=> 'Products per page must be numeric.'
     
    	];
		}
}
