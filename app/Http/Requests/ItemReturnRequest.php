<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ItemReturnRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'pcode' => 'required',
			'price'   => 'required',
            'return_date' => 'required',
			'return_reason' => 'required',
			'qty' => 'required',
			 'purchase_date' => 'required'

		];
        
        
        return $rules;
    }
    
    public function messages()
    {
    
        return [
        
            'pcode.required' => 'Product Code is required.',
            'price.required' => 'Price is required.',
            'return_date.required' => 'Return Date is required.',
			'return_reason.required' => 'Return Reason is required.',
			'qty.required' => 'Quantity is required.',
			'purchase_date.required' => 'Purchase date is required.'
        ];
    }
}
