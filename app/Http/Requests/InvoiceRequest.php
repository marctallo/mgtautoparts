<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'date' => 'required',
            'invoice_number' => 'required'
        
        ];
        
  
        if($this->request->get('pid') != null){
            foreach($this->request->get('pid') as $key => $val){
                $rules['pid.'.$key] = 'required';
                
                
            }
        }else{
            $rules['items'] = 'required';
        }
        
        
        return $rules;
    }
    
    public function messages(){
    
        return [
            'date.required' => 'Date is required. ',
            'invoice_number.required' => 'Invoice number is required.',
            'items.required' => 'Please add items.'
            
        ];
    }
}
