<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RestockRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'qty'   => 'required|numeric',
            'pdate' => 'required',
            'supplier_id'   => 'dropdown',
            'order_img_file' => 'mimes:jpeg,bmp,png|max:10000'
        
        ];
        
        
        
        return $rules;
    }
    
    public function messages()
    {
        return [
        
            'qty.required'  => 'Please add quantity of the product.',
            'qty.numeric'    => 'Quantity must be a number.',
            'pdate.required'=> 'Purchase date is required.',
            'supplier_id.dropdown' => 'Please select a supplier.',
            'order_img_file.mimes'          => 'Invalid image format.',
            'order_img_file.size'        =>'File must not exceed 10mb.'
        ];
    }
}
