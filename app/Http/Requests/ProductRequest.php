<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
			'pcode'   => 'required|unique:products,pcode',
            //'maker'  => 'required',
            //'year'   => 'required',
            'cat_id'   => 'dropdown',
            //'model_id' => 'dropdown',
            'min_stock' => 'integer',
            'max_stock' => 'integer',
            'current_stock' => 'integer',
            'reorder_point' => 'integer',
            'wholesale_price' => 'numeric',
            'retail_price' => 'numeric',
            'image' => 'mimes:jpeg,bmp,png|max:10000',
						'initial_stock' => 'required|integer'
		];
        
        
        $id = Request::segment(3);
		

		//dd($commentId);
		if(Request::isMethod('patch')){
			
			
			$rules['pcode'] = 'required|unique:products,pcode,'.$id;
    	
		}
        
        
        return $rules;
    }
    
    public function messages(){
    
    
        return [
        
            'pcode.required' => 'Product Code is required.',
            'maker.required' => 'Maker is required.',
            'year.required' => 'Year is required.',
            'cat_id.dropdown' => 'Please select a product type.',
            'model_id.dropdown' => 'Please select a car model.',
            'pcode.unique'  => 'Product Code already exist.',
            'pname.required' => 'Product name is required.',
            'min_stock.integer' => 'Minimum stock must be a number.',
            'max_stock.integer' => 'Maximum stock must be a number.',
            'current_stock.integer' => 'Current stock must be a number.',
            'reorder_point.integer' => 'Reorder Point must be a number.',
            'wholesale_price.numeric' => 'Wholesale price must be a number.',
            'retail_price.numeric' => 'Retail price must be a number.',
            'image.mimes'          => 'Invalid image format.',
            'image.size'        =>'File must not exceed 10mb.',
						'initial_stock.required' => 'Initial Stock is required.',
						'initial_stock.integer' => 'Initial Stock must be integer.'
        ];
    }
}
