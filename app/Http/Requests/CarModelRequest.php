<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CarModelRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         $rules = [
			'cmodel'   => 'required|unique:car_model,cmodel',
		
		];
        
        $id = Request::segment(3);
		

		if(Request::isMethod('patch')){
			
			
			$rules['cmodel'] = 'required|unique:car_model,cmodel,'.$id;
       
			
		}
        
        return $rules;
            
    }
    
    public function messages()
    {
    
        return [
            'cmodel.required'    => 'Car Model is required.',
            'cmodel.unique'      => 'Car Model already exist.'
        ];
    }
}
