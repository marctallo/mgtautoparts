<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExpenseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'expense_category' => 'dropdown',
			'expense_date'   => 'required',
            'amount' => 'required'

		];
        
        
        return $rules;
    }
    
    public function messages()
    {
    
        return [
        
            'expense_category.dropdown' => 'Please select a expense type.',
            'expense_date.required' => 'Date is required.',
            'amount.required' => 'Amount is required.'
        ];
    }
}
