<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
			'username'   => 'required|unique:users,username',
			'password'   => 'required|min:6|same:rpassword',
            'email'      => 'email|unique:users,email',
			'rpassword'  => 'required',
            'fname'      => 'required',
            'lname'      => 'required'
		];
        
        
        $id = Request::segment(2);
		
		
		
		//dd($commentId);
		if(Request::isMethod('patch')){
			
            $rules['username'] = 'required|unique:users,username,'.$id;
            $rules['email'] = 'email|unique:users,email,'.$id;
            
            if(Request::segment(1) == 'myaccount'){
            
                if(Request::input('updateBy') == 'update'){
                    unset($rules['password']);
                    unset($rules['rpassword']);
                }elseif(Request::input('updateBy') == 'password'){
                    unset($rules['username']);
                    unset($rules['email']);
                    unset($rules['lname']);
                    unset($rules['fname']);
                
                }
                
            }
			
			
			
		}
        
        
        return $rules;
    }
    
    public function messages(){
		
		return [
	        'username.required'     => 'Username is required.',
	        'username.unique'       => 'Username already exist.',
            'email.email'           => 'Your email is invalid.',
            'email.unique'          => 'Email already exist.',
	        'password.required'     => 'Your password is required.',
	        'password.min'          => 'Your password must be at least 6 characters long.',
	        'password.same'         => 'Both your password does not match.',
	        'rpassword.required'    => 'Please re-type your password.',
            'fname.required'        => 'Your first name is required.',
            'lname.required'        => 'Your last name is required.'
	        
	     
    	];
	}
}
