<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SupplierRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
			'supplier_name'   => 'required|unique:suppliers,supplier_name',
            'email'      => 'email|unique:suppliers,email',
            'website'   => 'active_url'
		];
        
        
        $id = Request::segment(2);
		

		if(Request::isMethod('patch')){
			
			
			$rules['supplier_name'] = 'required|unique:suppliers,supplier_name,'.$id;
            $rules['email'] = 'email|unique:suppliers,email,'.$id;
			
		}
        
        return $rules;
    }
    
    public function messages(){
		
		return [
	        'supplier_name.required'     => 'Supplier name is required.',
	        'supplier_name.unique'       => 'Supplier name already exist.',
            'email.email'   => 'You must enter a valid email.',
            'email.unique' => 'Email already exist.',
            'website.active_url' => 'Invalide website url.'
         
	     
    	];
	}
}
