<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/







Route::group(['middleware' => 'guest'],function(){
	
	Route::get('/','LoginController@index');
    Route::post('auth','LoginController@auth');

});




Route::group(['middleware' => 'auth'], function()
{
    
    
    //login controller
    Route::get('logout','LoginController@logout');
    
    
    //dashboard controller
    Route::get('dashboard/monthlySales','DashboardController@monthlySales');
    Route::get('dashboard/dailySales','DashboardController@dailySales');
    Route::get('dashboard','DashboardController@index');
    
    //user controller
    
    Route::get('users/getList/{trash}','UserController@getList');
    
    //category controller
    
    Route::get('catalog/categories/getList/{trash}','CategoryController@getList');
    
    //product controller
    
       
    Route::get('catalog/products/details/{id}','ProductController@getDetail');
    Route::post('catalog/products/savestock','ProductController@saveStock');
    Route::get('catalog/products/addstock/{id}','ProductController@addStock');
    Route::get('catalog/products/details/{id}/getList/{trash}','ProductController@getList');
    Route::get('catalog/products/details/{id}/getOrderSlip','ProductController@getOrderSlip');
    Route::put('catalog/products/details/updateCurrentProductPrice','ProductController@updateCurrentProductPrice');
		Route::put('catalog/products/details/updateCurrentProductSupplier','ProductController@updateCurrentProductSupplier');
    Route::get('catalog/products/getPcodeList','ProductController@getPcodeList');
	
    //car model controller
    
    Route::get('catalog/carmodel/getList/{trash}','CarModelController@getList');
    
    //supplier controller
    
    Route::get('suppliers/getList/{trash}','SupplierController@getList');
    
    //product request controller
    
    Route::get('catalog/request/getList/{trash}','ProductRequestController@getList');
    
    
    
    //my account route
    
    Route::get('myaccount','UserController@myAccount');
    Route::patch('myaccount/{id}','UserController@updateAccount');
	
		//my account route
	
		Route::get('dashboard/saveReminder','ReminderController@saveReminder');
		Route::get('dashboard/getReminders','ReminderController@getReminders');
		Route::get('dashboard/removeReminder','ReminderController@removeReminder');
    
    //admin only routes
    Route::group(['middleware' => 'admin'],function(){
	   
        Route::post('catalog/products/uploadcsv','ProductController@uploadCsv');
        Route::get('catalog/products/trashed','ProductController@showTrash');
        Route::delete('catalog/products/trashed/delete/{id}','ProductController@permanentDelete');
        Route::put('catalog/products/trashed/restore/{id}','ProductController@restoreTrashed');
				Route::get('catalog/products/download/list','ProductController@downloadCsvList');
				Route::get('catalog/products/lowstock','ProductController@showLowStockItems');
				Route::get('catalog/products/lowstock/print','ProductController@printLowStockItems');
			  Route::get('catalog/products/updater','ProductController@updaterIndex');
				Route::post('catalog/products/massUpdate','ProductController@massUpdate');
				Route::get('catalog/products/restock','ProductController@restockIndex');
				Route::post('catalog/products/restock/massRestock','ProductController@massRestock');
        
        //user resource
        Route::delete('users/trashed/delete/{id}','UserController@permanentDelete');
        Route::put('users/trashed/restore/{id}','UserController@restoreTrashed');
        Route::get('users/trashed','UserController@showTrash');
        Route::resource('users','UserController');
        
        //expense resource
        Route::post('expenses/saveType','ExpenseController@saveType');
        Route::resource('expenses','ExpenseController');
        
        
        Route::get('sales/invoice/download/{id}','InvoiceController@downloadInvoice');
        Route::get('sales/invoice/print/{id}','InvoiceController@printInvoice');
        Route::get('sales/invoice/getItems','InvoiceController@getItems');
        Route::get('sales/invoice/getCategory','InvoiceController@getCategory');
        Route::resource('sales/invoice','InvoiceController');
        
        
        Route::delete('suppliers/trashed/delete/{id}','SupplierController@permanentDelete');
        Route::put('suppliers/trashed/restore/{id}','SupplierController@restoreTrashed');
        Route::get('suppliers/trashed','SupplierController@showTrash');
        
        
        Route::delete('catalog/categories/trashed/delete/{id}','CategoryController@permanentDelete');
        Route::put('catalog/categories/trashed/restore/{id}','CategoryController@restoreTrashed');
        Route::get('catalog/categories/trashed','CategoryController@showTrash');
        
        
        Route::delete('catalog/carmodel/trashed/delete/{id}','CarModelController@permanentDelete');
        Route::put('catalog/carmodel/trashed/restore/{id}','CarModelController@restoreTrashed');
        Route::get('catalog/carmodel/trashed','CarModelController@showTrash');
        
        Route::get('catalog/request/trashed','ProductRequestController@showTrash');
        Route::delete('catalog/request/trashed/delete/{id}','ProductRequestController@permanentDelete');
        Route::put('catalog/request/trashed/restore/{id}','ProductRequestController@restoreTrashed');
			
				
        
    });
    
    
    /* resources section */
    
    
    Route::resource('catalog/categories','CategoryController');
    Route::resource('catalog/products','ProductController');
    Route::resource('catalog/carmodel','CarModelController');
    Route::resource('catalog/request','ProductRequestController');
    Route::resource('suppliers','SupplierController');
		Route::resource('sales/returns','ItemReturnController');
		Route::resource('reports/income-statement','IncomeStatementController');
		Route::resource('settings','SettingsController');
    
    
});

