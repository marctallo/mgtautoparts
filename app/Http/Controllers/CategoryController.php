<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CategoryRequest;
use App\Http\Controllers\Controller;
use App\Category;
use App\AuditTrail;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
       //$this->seedTable();
        return view('category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('category.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CategoryRequest $request)
    {
        Category::create($request->all());
        
        return redirect('catalog/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        
        return view('category.edit')->with('category',$category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,CategoryRequest $request)
    {
        $category = Category::findOrFail($id);
		
		$category->update($request->all());
        
        return redirect('catalog/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        
        $category->delete();
        
        return redirect('catalog/categories');
    }
    
    
    public function getList($trash){
        
        if($trash == 0){
            return Category::orderBy('created_at','desc')->get();
        }else{
            return Category::onlyTrashed()->get();
        }

    }
    
    public function showTrash(){
    
        return view('category.trash');
    }
    
    public function permanentDelete($id){
    
        $category = Category::getTrashedSingle($id);
    
        
        $category->forceDelete();
        
        return redirect('catalog/categories/trashed');
    }
    
    public function restoreTrashed($id){
    
        $category = Category::getTrashedSingle($id);
        
        $category->restore();
        
        return redirect('catalog/categories/trashed');
        
    }
    
    public function seedTable(){
    
        $categories = array(
          ['cname'=>'Gas Strut'],
          ['cname'=>'Glow Plug'],
          ['cname'=>'Spark Plug'],
          ['cname'=>'Timing Belt'],
          ['cname'=>'B/M Kit'],
          ['cname'=>'BD Boost Kit'],
          ['cname'=>'BMCK'],
          ['cname'=>'Break Shoe'],
          ['cname'=>'Break Disk Pad'],
          ['cname'=>'Engine Valve Guide'],
          ['cname'=>'Synchornizer Ring'],
          ['cname'=>'Clutch Fork'],
          ['cname'=>'Crank Shaft Pulley'],
          ['cname'=>'Repair Kit'],
          ['cname'=>'Cup Kit'],
          ['cname'=>'Fan Clutch'],
          ['cname'=>'Distributor Cap'],
          ['cname'=>'Ignition Coil'],
          ['cname'=>'Thermo Switch'],
          ['cname'=>'Temp Sending Unit'],
          ['cname'=>'Contract Post Copper'],
          ['cname'=>'Wheel Cylinder Assembly'],
          ['cname'=>'Brake Master'],
          ['cname'=>'Engine Support'],
          ['cname'=>'Boost with Clamp'],
          ['cname'=>'Brake Hose'],
          ['cname'=>'Clutch Hose'],
          ['cname'=>'Alternator Hose'],
          ['cname'=>'Fuel Filter'],
          ['cname'=>'Cogbelt'],
          ['cname'=>'Rib-Ace Belt'],
          ['cname'=>'Checkered Belt'],
          ['cname'=>'Oil Filter'],
          ['cname'=>'Air Freshner'],
          ['cname'=>'Vol Regulator'],
          ['cname'=>'Torque Rod Bushing'],
          ['cname'=>'Trunion Bushing'],
          ['cname'=>'Wiper Blade'],
          ['cname'=>'Bulb'],
          ['cname'=>'Bendix Drive'],
          ['cname'=>'Back Lamp Switch'],
          ['cname'=>'Contact Point'],
          ['cname'=>'Power Relay'],
          ['cname'=>'Condenser'],
          ['cname'=>'Socket with JP Wire'],
          ['cname'=>'Fuse Holder'],
          ['cname'=>'Halogen'],
          ['cname'=>'Fuel Tank Cap'],
          ['cname'=>'Stop Light Switch'],
          ['cname'=>'Solenoid Switch'],
          ['cname'=>'Oil Hose'],
          ['cname'=>'Silicone Oil'],
          ['cname'=>'Wiper Motor'],
          ['cname'=>'Marker Lamp'],
          ['cname'=>'Tail Lamp'],
          ['cname'=>'LED Side Lamp'],
          ['cname'=>'Penetrating Oil'],
          ['cname'=>'Early Warning Device'],
          ['cname'=>'Tire Black'],
          ['cname'=>'Protectant Oil'],
          ['cname'=>'Corner Lamp'],
          ['cname'=>'Backup Horn'],
          ['cname'=>'Electric Fuel Pump'],
          ['cname'=>'Air Horn Switch'],
          ['cname'=>'Push Pull Switch'],
          ['cname'=>'Horn Switch'],
          ['cname'=>'Fanfare Horn'],
          ['cname'=>'Wiper Arm'],
          ['cname'=>'Automotive Wire'],
          ['cname'=>'Starter Armature'],
          ['cname'=>'Air Horn'],
          ['cname'=>'Compartment Lock'],
          ['cname'=>'Fuel Float'],
          ['cname'=>'Cigarette Lighter'],
          ['cname'=>'Dimmer Switch'],
          ['cname'=>'Fuse Box'],
          ['cname'=>'Fuel Pump Assembly'],
          ['cname'=>'Automotive Fuse'],
          ['cname'=>'Ceramic HL Socket'],
          ['cname'=>'Bulb Socket'],
          ['cname'=>'Wire Terminal'],
          ['cname'=>'Flasher Relay'],
          ['cname'=>'Automotive Gauge'],
          ['cname'=>'Horn Relay'],
          ['cname'=>'Horn'],
          ['cname'=>'Headlight Switch'],
          ['cname'=>'IC Regulator'],
          ['cname'=>'Injection Priming Pump'],
          ['cname'=>'Injection Diaphragm'],
          ['cname'=>'Ignition Switch'],
          ['cname'=>'Oil Pressure Switch'],
          ['cname'=>'Radiator Cap'],
          ['cname'=>'Starter Bushing'],
          ['cname'=>'Starter Button Switch'],
          ['cname'=>'Shut Off Valve'],
          ['cname'=>'Static Switch'],
          ['cname'=>'Toggle Switch'],
          ['cname'=>'Car Shampoo'],
          ['cname'=>'Super Oil Treatment'],
          ['cname'=>'Electronic Cleaner'],
          ['cname'=>'Long Life Radiator Coolant'],
          ['cname'=>'Radiator and Cooling System'],
          ['cname'=>'Wheel Rim Cleaner'],
          ['cname'=>'Tire Shine'],
          ['cname'=>'Belt Dressing'],
          ['cname'=>'Leather Cleaner'],
          ['cname'=>'Form Cleaner'],
          ['cname'=>'Windshield Washer'],
          ['cname'=>'Engine Degreaser'],
          ['cname'=>'Wiper'],
          ['cname'=>'Mini Fuse'],
          ['cname'=>'Fuse'],
          ['cname'=>'C.V Joint'],
          ['cname'=>'Air Filter'],
          ['cname'=>'Socket'],
          ['cname'=>'Connector Wire'],
          ['cname'=>'Wiper Refill'],
          ['cname'=>'Grease'],
          ['cname'=>'Break Fluid'],
          ['cname'=>'Steering Fluid'],
          ['cname'=>'Motor Flush'],
          ['cname'=>'Gear Oil'],
          ['cname'=>'ATF'],
          ['cname'=>'Battery Terminal Protector'],
          ['cname'=>'Oil Treatment'],
          ['cname'=>'Flat Fix with Hose'],
          ['cname'=>'Engine Flush'],
          ['cname'=>'Smoke Eliminator'],
          ['cname'=>'Duster'],
          ['cname'=>'Octane Booster'],
          ['cname'=>'Fuel Conditioner'],
          ['cname'=>'Silicone Spray'],
          ['cname'=>'Glass Cleaner'],
          ['cname'=>'Windshield Wash'],
          ['cname'=>'Engine Oil'],
          ['cname'=>'Motor Cleaner'],
          ['cname'=>'Hand Cleaner'],
          ['cname'=>'Radiator Anti Rust'],
          ['cname'=>'Transmission Stop Leak'],
          ['cname'=>'Gas Treatment'],
          ['cname'=>'Break Parts Cleaner'],
          ['cname'=>'Cooling System Stop Leak'],
          ['cname'=>'Radiator Flush'],
          ['cname'=>'Tire Inflator'],
          ['cname'=>'Octane Boost'],
          ['cname'=>'Clutch Release Bearing'],
          ['cname'=>'Clutch Disk'],
          ['cname'=>'Press Plate']
        );
        
        foreach($categories as $category){
        
        
            Category::create($category);
        }
    
    }
}
