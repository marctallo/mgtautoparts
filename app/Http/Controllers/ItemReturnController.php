<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ItemReturn;
use App\Http\Requests\ItemReturnRequest;
use App\Product;

class ItemReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $returns = ItemReturn::getAll();
        
     
        $this->setSearch($request,$returns);
        

        
        $sort = ($request->input('sort')) ? $request->input('sort') : 'item_returns.created_at';
        $order = ($request->input('order')) ? $request->input('order') : 'desc';
        
        $sort_fields = [
            'order'=>((\Request::input('order'))=='desc' ? 'asc':'desc'),
            'pcode'=>\Request::input('pcode'),
            'qty'=>\Request::input('qty'),
            'price'=>\Request::input('price'),
            'return_date'=>\Request::input('return_date')
        ];  
      
        
        $items = $returns->orderBy($sort,$order)->simplePaginate(20);
        
        return view('item_return.index')->with(['returns' => $items,'sort_fields'=>$sort_fields]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
		
		return view('item_return.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemReturnRequest $request)
    {
        $data = $request->all();
        
     	$product = Product::getProductByCode($request->pcode)->first();

		$data['p_id'] = $product->id;
      
        if(ItemReturn::create($data)){
			if($request->add_to_stock != null){
          		
			
				if(count($product)){
					
					$current_stock = $product->current_stock + $request->qty;
					
					$product->update([
						'current_stock' => $current_stock
					]);
				}
        	}
			
			showAlertBox('Item return has been added.','success');
		}
		
		
        
        
        
        return redirect('sales/returns');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $return = ItemReturn::findOrFail($id);
        
        $return->delete();
        
        return redirect('sales/returns');
    }
	
	private function setSearch($request,$return){
    

        if($request->input('date_from') != '' && $request->input('date_from') != null && $request->input('date_to') != '' && $request->input('date_to') != null){
            $return->whereBetween('return_date',[$request->input('date_from'),$request->input('date_to')]); 
         
        }
        
    }
}
