<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SettingsRequest;
use App\Setting;
use App\AuditTrail;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = 1; // must have only 1 record in the database thus the id is 1;
				$settings = Setting::findOrFail($id);
			
				return view('setting.index')->with(['settings'=>$settings]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingsRequest $request, $id)
    {
        $settings = Setting::findOrFail($id);
			
				$data = $request->all();
			
				if($request->show_inactive != null){
          $data['show_inactive'] = 1;
        }else{
          
          $data['show_inactive'] = 0 ;
        }
		
				$settings->update($data);
        
        return redirect('settings');
    }

}
