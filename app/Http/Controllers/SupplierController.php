<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SupplierRequest;
use App\Supplier;
use App\AuditTrail;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('supplier.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('supplier.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SupplierRequest $request)
    {
        Supplier::create($request->all());
        
        
        return redirect('suppliers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $supplier = Supplier::findOrFail($id);
        
        
        return view('supplier.edit')->with('supplier',$supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,SupplierRequest $request)
    {
        $supplier = Supplier::findOrFail($id);
		
		$supplier->update($request->all());
        
        return redirect('suppliers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        
        $supplier->delete();
        
        return redirect('suppliers');
    }
    
    
    public function getList($trash){
    
        if($trash == 0){
            return Supplier::orderBy('created_at','desc')->get();
        }else{
            return Supplier::onlyTrashed()->get();
        }
        
    }
    
    
    public function showTrash(){
    
        return view('supplier.trash');
    }
    
    public function permanentDelete($id){
    
        $supplier = Supplier::getTrashedSingle($id);
    
        
        $supplier->forceDelete();
        
        return redirect('suppliers/trashed');
    }
    
    public function restoreTrashed($id){
    
        $supplier = Supplier::getTrashedSingle($id);
        
        $supplier->restore();
        
        return redirect('suppliers/trashed');
        
    }
}
