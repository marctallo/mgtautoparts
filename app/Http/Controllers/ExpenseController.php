<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ExpenseCategory;
use App\Expense;
use App\Http\Requests\ExpenseRequest;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
       $month = \Request::input('filter');
        
        
        $expenses = Expense::getExpense($month)->get();
        

        return view('expense.index')->with(['month'=>monthList(),'expenses'=>$expenses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
        $type = ExpenseCategory::setToSelect('cname','Select Expense Type');
        return view('expense.new')->with('type',$type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ExpenseRequest $request)
    {
        Expense::create($request->all());
        
        
        return redirect('expenses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $expense = Expense::findOrFail($id);
        $type = ExpenseCategory::setToSelect('cname','Select Expense Type');
        
        return view('expense.edit')->with(['expense'=>$expense,'type'=>$type]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,ExpenseRequest $request)
    {
        $expense = Expense::findOrFail($id);
        
        
        $expense->update($request->all());
        
        
        
        return redirect('expenses');
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $expense = Expense::findOrFail($id);
        

        $expense->delete();
        
        return redirect('expenses');
    }
    
    
    public function saveType(Request $request){
    
        
        ExpenseCategory::create(['cname'=>$request->input('type')]);
        
        $expense = ExpenseCategory::all();
        
        return $expense;
    }
}
