<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CarModelRequest;
use App\Http\Controllers\Controller;
use App\CarModel;
use App\AuditTrail;


class CarModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('carmodel.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('carmodel.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CarModelRequest $request)
    {
        CarModel::create($request->all());
        
        return redirect('catalog/carmodel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $carmodel = CarModel::findOrFail($id);
        
        return view('carmodel.edit')->with('carmodel',$carmodel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,CarModelRequest $request)
    {
        $carmodel = CarModel::findOrFail($id);
		
		$carmodel->update($request->all());
        
        return redirect('catalog/carmodel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $carmodel = CarModel::find($id);
        
        $carmodel->delete();
        
        return redirect('catalog/carmodel');
    }
    

    public function getList($trash){
        
        if($trash == 0){
            return CarModel::orderBy('cmodel','asc')->get();
        }else{
            return CarModel::onlyTrashed()->get();
        }
        
         

    }
    
    public function showTrash(){
    
        return view('carmodel.trash');
    }
    
    public function permanentDelete($id){
    
        $carmodel = CarModel::getTrashedSingle($id);
    
        
        $carmodel->forceDelete();
        
        return redirect('catalog/carmodel/trashed');
    }
    
    public function restoreTrashed($id){
    
        $carmodel = CarModel::getTrashedSingle($id);
        
        $carmodel->restore();
        
        return redirect('catalog/carmodel/trashed');
        
    }
}
