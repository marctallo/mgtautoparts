<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AuditTrail;
use App\Invoice;
use App\Category;
use App\Product;
use App\Setting;
use DB;
use App\InvoiceItems;
use App\Http\Requests\InvoiceRequest;
use PDF;



class InvoiceController extends Controller
{
    
    private $category;
    
    public function __construct(){
    
        $this->category = Category::setToSelect('cname','Select a product type');
    }
    
    public function index(Request $request)
    {
        
        $invoices = Invoice::getAll();
        
     
        $this->setSearch($request,$invoices);
        

        
        $sort = ($request->input('sort')) ? $request->input('sort') : 'created_at';
        $order = ($request->input('order')) ? $request->input('order') : 'desc';
        
        $sort_fields = [
            'order'=>((\Request::input('order'))=='desc' ? 'asc':'desc'),
            'invoice_number'=>\Request::input('invoice_number'),
            'date_from'=>\Request::input('date_from'),
            'date_to'=>\Request::input('date_to'),
            'page'=>\Request::input('page')
        ];  
      
        
        $items = $invoices->orderBy($sort,$order)->simplePaginate(20);
			
				
        
        return view('invoice.index')->with(['invoices' => $items,'sort_fields'=>$sort_fields]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('invoice.new')->with(['category'=>$this->category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InvoiceRequest $request)
    {
        //dd($request->all());
        DB::transaction(function ($request) use ($request){
            
            $invoice = Invoice::create($request->all());
            
            //dd($request->all());
            for($i = 0 ; $i < count($request->pid);$i++){
            
                
                
                $item = [
                    'product_id' => $request->pid[$i],
                    'qty'    => $request->qty[$i],
                    'unit_price' => $request->price[$i],
                    'invoice_id' => $invoice->id,
                    'wholesale_price' => $request->wholesale_price[$i]
                    
                ];
                
                InvoiceItems::create($item);
                
                $product = Product::findOrFail($request->pid[$i]);
			
								if(strtolower($product->unit) != 'roll'){
									$product->current_stock = (int)$product->current_stock - (int)$request->qty[$i];
                  $product->update();
									echo "x";
								}
								
                
                
                showAlertBox('A new invoice has been added.','success');
            }
        });
        
        return redirect('sales/invoice');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = Invoice::findOrFail($id);
        
        $invoice->delete();
        
        return redirect('sales/invoice');
    }
    
    public function getItems(Request $request){
    
        $cid = $request->cid;
        
        $products =  Product::getProductByCategory($cid)->get();
        
        if(count($products) > 0){
            return $products;
        }else{
            return 0;
        }
    }
    
    public function getCategory(){
    
        return Category::orderBy('cname','asc')->get();
    }
    
    private function setSearch($request,$invoice){
    
        if($request->input('invoice_number') != '' && $request->input('invoice_number') != null){
            $invoice->where('invoice_number','=',$request->input('invoice_number'));
        }else if($request->input('date_from') != '' && $request->input('date_from') != null && $request->input('date_to') != '' && $request->input('date_to') != null){
            $invoice->whereBetween('date',[$request->input('date_from'),$request->input('date_to')]); 
          // $invoice->where('date','=',$request->input('date'));
        }/*else if($request->input('month') != 0){
        
            $invoice->whereRaw('month(date) ='.$request->input('month'))
                    ->whereRaw('year(date) ='.date('Y'));
        }*/
        
    }
    
    public function printInvoice($id){
    
        $invoice = Invoice::findOrFail($id);
				$settings = Setting::findOrFail(1);
        
        $items = InvoiceItems::getAll($id);
        
        $pdf = PDF::loadView('invoice.print',['invoice'=>$invoice,'items'=>$items,'settings'=> $settings]);
        
        return $pdf->stream();
    
    }
    
    public function downloadInvoice($id){ // not used
    
        $invoice = Invoice::findOrFail($id);
        
        $items = InvoiceItems::getAll($id);
        
        $pdf = PDF::loadView('invoice.print',['invoice'=>$invoice,'items'=>$items]);
        
        return $pdf->download($invoice->invoice_number.'.pdf');
    
    }
}
