<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\RestockRequest;
use App\Product;
use App\CarModel;
use App\Supplier;
use App\ProductStock;
use App\Setting;
use DB;
use File;
use Html;
use Csv;
use Session;
use Validator;
use PDF;

class ProductController extends Controller
{
    
    private $supplier;
    private $category;
    private $car_model;
		private $settings;
    
    
    public function __construct(){
        
        $this->supplier = Supplier::setToSelect('supplier_name','Select supplier');
        $this->category = Category::setToSelect('cname','Select a product type');
        $this->car_model = CarModel::setToSelect('cmodel','Select a model');
				$this->settings = Setting::findOrFail(1);
    }
    
    
    public function index(Request $request)
    {
        
        //new \Custom\Helper\Sort;
       
    
        //\App::make('sort');
        $products = Product::getAll();

        $this->setSearch($request,$products);
        
        //$products->where('products.id','=',1);
        
        
        $sort = ($request->input('sort')) ? $request->input('sort') : 'products.updated_at';
        $order = ($request->input('order')) ? $request->input('order') : 'desc';
        
      
        $items = $products->orderBy($sort,$order)->paginate(50);
      
        $pagination_fields = [
            'sort'=>\Request::input('sort'),
            'order'=>\Request::input('order'),
            'pcode'=>\Request::input('pcode'),
            'cat_id'=>\Request::input('cat_id'),
            'maker'=>\Request::input('maker'),
            'application' => \Request::input('application'),
            'page'=>\Request::input('page')


        ];
      
        $sort_fields = $pagination_fields;
        $sort_fields['order'] = ((\Request::input('order'))=='desc' ? 'asc':'desc');
			
				Session::put('prev_url', $request->fullUrl());
      
        return view('product.index')->with([
          'category'=>$this->category,
          'car_model'=>$this->car_model,
          'products'=>$items,
          'item_count'=>count($items),
          'pagination_fields'=>$pagination_fields,
          'sort_fields' => $sort_fields
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('product.new')->with(['category'=>$this->category,'supplier'=>$this->supplier]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ProductRequest $request)
    {
        
        $newfile = upload_file($request,'image',public_path('img/products'));
        
        $data = $request->all();
        
      
        if($request->is_updated != null){
          $data['is_updated'] = 1;
        }else{
          
          $data['is_updated'] = 0 ;
        }
      
         //dd($data);
      
        if($newfile != null){
            $data['image_file'] = $newfile; //change this 
        }
        
      
        Product::create($data);
        
        
        
        return redirect('catalog/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
		public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
			
        $product = Product::findOrFail($id);

        return view('product.edit')->with(['category'=>$this->category,'supplier'=>$this->supplier,'product'=>$product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,ProductRequest $request)
    {
			
        $product = Product::findOrFail($id);
        
        $newfile = upload_file($request,'image',public_path('img/products'));
        
        $data = $request->all();
      
        if($request->is_updated != null){
          $data['is_updated'] = 1;
        }else{
          
          $data['is_updated'] = 0 ;
        }
        
        if($newfile != null){
            $data['image_file'] = $newfile; //change this 
            
            $this->deleteProductImage($product->image_file);
            
        }
		
		$product->update($data);
        
        return redirect(Session::get('prev_url'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        
        $product->delete();
        
        return redirect('catalog/products');
    }
    
    public function getList(){
        
        $id = \Request::segment(4);
        

        return Product::getStockList($id);
    }
    
    public function getOrderSlip(){
    
        $id = \Request::input('id');
        
        $order_slip = Product::getStockSingle($id);
        return Html::image('img/orders/'.$order_slip->order_img,'order slip',['class'=>'order-slip-img']);
    }
    
    
    public function addStock($id){
    
    		$product = Product::findOrFail($id);
        
        return view('product.restock.new')->with(['supplier'=>$this->supplier,'id'=>$id,'product'=>$product]);
    }

    public function saveStock(RestockRequest $request){
        
        
        
        
        DB::transaction(function ($request) use ($request){
            
            $newfile = upload_file($request,'order_img_file',public_path('img/orders'));
        
            $data = $request->all();

            if($newfile != null){
                $data['order_img'] = $newfile; //change this 
            }
            
            ProductStock::create($data);
            
            $product = Product::findOrFail($request->product_id);
            
            $product->current_stock = (int)$product->current_stock + (int)$request->qty;
					
						if($request->update_price){
							
							$product->wholesale_price = $request->price;
							$product->retail_price = $request->price + ($request->price * ($this->settings->base_markup / 100));
							
						}
					
						if($request->update_supplier){
							
							$product->supplier_id = $request->supplier_id;
						}
            
            $product->update();
        });
        
        
        
        
        return redirect('catalog/products');
      
    }
	
		public function showLowStockItems(Request $request){
			
			$products = Product::getLowStockItems();
        $this->setSearch($request,$products);
        
        //$products->where('products.id','=',1);
        
        
        $sort = ($request->input('sort')) ? $request->input('sort') : 'products.created_at';
        $order = ($request->input('order')) ? $request->input('order') : 'desc';
        
      
        $items = $products->orderBy($sort,$order)->paginate(50);


				//dd(count($products->get()));
      
        $pagination_fields = [
            'sort'=>\Request::input('sort'),
            'order'=>\Request::input('order'),
            'cat_id'=>\Request::input('cat_id'),
            'supplier_id'=>\Request::input('supplier_id'),
            'maker'=>\Request::input('maker'),
						'page' => \Request::input('page')
        ];
      
        $sort_fields = $pagination_fields;
        $sort_fields['order'] = ((\Request::input('order'))=='desc' ? 'asc':'desc');
      	
				Session::put('prev_url', $request->fullUrl());
				
        return view('product.lowstock')->with([
          'category'=>$this->category,
          'products'=>$items,
          'item_count'=>count($items),
          'pagination_fields'=>$pagination_fields,
          'sort_fields' => $sort_fields,
					'supplier' => $this->supplier
        ]);
		}
    
    public function showTrash(Request $request){
		
		$products = Product::getAll(true);

        $this->setSearch($request,$products);
        
        //$products->where('products.id','=',1);
        
        
        $sort = ($request->input('sort')) ? $request->input('sort') : 'products.created_at';
        $order = ($request->input('order')) ? $request->input('order') : 'desc';
        
      
        $items = $products->orderBy($sort,$order)->paginate(50);
      
        $pagination_fields = [
            'sort'=>\Request::input('sort'),
            'order'=>\Request::input('order'),
            'pcode'=>\Request::input('pcode'),
            'cat_id'=>\Request::input('cat_id'),
            'model_id'=>\Request::input('model_id'),
            'maker'=>\Request::input('maker'),
            'application' => \Request::input('application'),
            'year'=>\Request::input('year'),
						'page' => \Request::input('page')


        ];
      
        $sort_fields = $pagination_fields;
        $sort_fields['order'] = ((\Request::input('order'))=='desc' ? 'asc':'desc');
      
        return view('product.trash')->with([
          'category'=>$this->category,
          'products'=>$items,
          'item_count'=>count($items),
          'pagination_fields'=>$pagination_fields,
          'sort_fields' => $sort_fields
        ]);
    
    
    }
    
    public function permanentDelete($id){
    
        $product = Product::getTrashedSingle($id);
        
        $product->forceDelete();
        
        return redirect('catalog/products/trashed');
    }
    
    public function restoreTrashed($id){
    
        $product = Product::getTrashedSingle($id);
        
        $product->restore();
        
        return redirect('catalog/products/trashed');
        
    }
    public function getDetail($id){
    
        $product = Product::getSingle($id);
        
        $product_history = Product::getProductSaleHistory($id)->paginate(50);
       // dd($product_history);
        return view('product.view')->with(['product'=> $product,'list'=>$product_history]);
    }
        
    private function deleteProductImage($filename){
        
        $file = public_path('img/products/'.$filename);
    
        if(File::exists($file)){
        
            File::delete($file);
        }
    }
    
    private function setSearch($request,$products){
    
        if($request->input('pcode') != '' && $request->input('pcode') != null){
            //$products->where('products.pcode','=',$request->input('pcode'));
          $products->where('pcode', 'like', '%'.$request->input('pcode').'%');
					$products->orWhere('pname', 'like', '%'.$request->input('pcode').'%');
        }
        
        if($request->input('cat_id') != 0 && $request->input('cat_id') != null){
            $products->where('products.cat_id','=',$request->input('cat_id'));
        }
        
        
        if($request->input('maker') != '' && $request->input('maker') != null){
            $products->where('products.maker','like','%'.$request->input('maker'));
        }
        
        if($request->input('supplier_id') != 0 && $request->input('supplier_id') != null){
           // $products->whereRaw("FIND_IN_SET('".$request->input('year')."',year)");
          $products->where('supplier_id', '=',$request->input('supplier_id'));
        }
      
        if($request->input('application') != '' && $request->input('application') != null){
            //$products->whereRaw("FIND_IN_SET('".$request->input('application')."',application)");
            $products->where('application', 'like', '%'.$request->input('application').'%');
        }
    }
    
    
    public function uploadCsv(Request $request){
    
        $file = $request->file('csvfile');
			
				if(null != $file){
					
					$ext = $file->getClientOriginalExtension();
        
        
					if($ext != 'csv'){

							showAlertBox('Please use a csv file for upload.','danger');

					}else{

							$file = Csv::toArray($request->csvfile);

							if($file === false){

								 showAlertBox('Cannot open file.','danger');
							}else{

									$rules = [
											'pcode'   => 'required|unique:products,pcode',
											'maker'  => 'required',
											'min_stock' => 'integer',
											'max_stock' => 'integer',
											'current_stock' => 'integer',
											'reorder_point' => 'integer',
											'wholesale_price' => 'numeric',
											'retail_price' => 'numeric',
									];

									$errors = array();
									

									for($i = 0 ; $i < count($file); $i++){
											
											$messages = array('pcode.unique'=>$file[$i]['pcode']." is already taken");
										
											$v = Validator::make($file[$i],$rules,$messages);
										

											if($v->fails()){

													$errors[$i]['row'] = $i+2;
													$errors[$i]['pcode'] = $v->errors()->first('pcode');
													$errors[$i]['maker'] = $v->errors()->first('maker');
													$errors[$i]['min_stock'] = $v->errors()->first('min_stock');
													$errors[$i]['max_stock'] = $v->errors()->first('max_stock');
													$errors[$i]['current_stock'] = $v->errors()->first('current_stock');
													$errors[$i]['wholesale_price'] = $v->errors()->first('wholesale_price');
													$errors[$i]['retail_price'] = $v->errors()->first('retail_price');

											}else{

												$file[$i]["is_active"] = 1;
											 /* echo "<pre>";
												print_r($file[$i]);
												exit();*/
												Product::create($file[$i]);
											}

									}

									if(count($errors) > 0){

											//showAlertBox('Errors occured while uploading. Some records where not uploaded.','danger');
										//  dd($errors);
											Csv::arrayDownload($errors);
											exit;

									}else{
											showAlertBox('Upload Successful.','success');

									}

							}

					}
					
				}else{
					
					showAlertBox('Please add a csv file to upload.','danger');
				}
        
        
      return redirect('catalog/products');  
      //return redirect('catalog/products');
    }
  
	
		public function downloadCsvList(Request $request){
				
			
				
				
			$cat_id = $request->input('cat_id');
			$category = Category::findOrFail($cat_id);
			$products = Product::getAll();
			$filename = $category->cname.".csv";	

      $this->setSearch($request,$products);
				
			$list = $products->orderBy('maker','asc')->orderBy('pcode','asc')->get();
				
			header("Content-Disposition: attachment; filename='$filename'");
			header("Cache-control: private");
			header("Content-type: application/force-download");
			header("Content-transfer-encoding: binary\n");	
			
				
			 $f = fopen('php://output','w');
        
     
        $headers = array(
            'Product Code',
						'Maker',
						'Wholesale Price',
						'15%',
						'20%',
						'30%',
						'Application',
						'Current Stock',
						'Pname'
						
        
        );
       fputcsv($f, $headers, ",");
        
			foreach($list as $item){

					
					$array = array(
						$item->pcode,
						$item->maker,	
						$item->wholesale_price,
						setMarkup($item->wholesale_price,.15),
						setMarkup($item->wholesale_price,.20),
						setMarkup($item->wholesale_price,.30),
						$item->application,
						$item->current_stock,
						$item->pname
					);
				
					 fputcsv($f, $array, ",");
			}
        
      
			fclose($f);

   
				
			
					
			
			echo $f;
				
			//dd($list);
		}
		
		public function updateCurrentProductPrice(Request $request){

				$id = \Input::get("id");
				$price = \Input::get("price");
				$product = Product::findOrFail($id);
				$data = [
					"wholesale_price" => $price

				];

			if(!$product->update($data)){

				return array("success" => 0);
			}

			return array("success" => 1);
		}
	
		public function updateCurrentProductSupplier(Request $request){

				$id = \Input::get("id");
				$supplier_id = \Input::get("supplier_id");
				$product = Product::findOrFail($id);
				
				$data = [
					"supplier_id" => $supplier_id

				];

			if(!$product->update($data)){

				return array("success" => 0);
			}

			return array("success" => 1);
		}
	
		

		public function getPcodeList(){

				$res = Product::get();

				$data = array();


				foreach($res as $value){
					
					$data[] = $value->pcode;
					
				}
				
				return array('data'=>$data);
		}
	
		public function printLowStockItems(Request $request){
			
				$products = Product::getLowStockItems();
				//$settings = Setting::findOrFail(1);
    
    		$url_parts = parse_url(Session::get('prev_url'));
			
				//dd(array_key_exists('query',$url_parts));
			
				if(array_key_exists('query',$url_parts)){
					
					parse_str($url_parts['query'], $q);
					
					if($q['cat_id'] != 0 && $q['cat_id'] != null){
							$products->where('products.cat_id','=',$q['cat_id']);
					}


					if($q['maker'] != '' && $q['maker'] != null){
							$products->where('products.maker','like','%'.$q['maker']);
					}

					if($q['supplier_id'] != 0 && $q['supplier_id'] != null){
						 // $products->whereRaw("FIND_IN_SET('".$request->input('year')."',year)");
						$products->where('supplier_id', '=',$q['supplier_id']);
					}

				}
			
				$products->orderBy('categories.cname','asc');
				
        
        $pdf = PDF::loadView('product.print',['products'=>$products->get(),'settings'=>$this->settings]);
        
       	return $pdf->stream();
    
    }
	
		public function updaterIndex(){
		
			return view('product.updater');
		}
	
		public function restockIndex(){
			
			return view('product.restock.restock_all');
		}
	
		public function massRestock(Request $request){
			
			 $file = $request->file('csvfile');
			 $errors = 0;
			 $error_arr = array();
				if(null != $file){
					
					$ext = $file->getClientOriginalExtension();
        
        
					if($ext != 'csv'){

						echo "please use csv file to upload";

					}else{

							$file = Csv::toArray($request->csvfile);

							if($file === false){

								 echo "cannot open file";
							}else{
										
								foreach($file as $update){
									
									$product = Product::where('pcode','=',$update['pcode'])->first();
			
									if($product != null){
										
										$update['product_id'] = $product->id;
										$update['price'] = $product->wholesale_price;
										
										DB::transaction(function ($update) use ($update,$product){
            
											ProductStock::create($update);


											$product->current_stock = (int)$product->current_stock + (int)$update['qty'];

											$product->update();
										});
										
									}
										
								}
								
								
								if($errors > 0){
									
									echo "something went wrong went updating </br></br>";
									dd($error_arr);
								}else{
									echo "restock successful";
									
								}
								

							}

					}
					
				}else{
					
					echo "add file to upload";
				}
			
		}
	
		public function massUpdate(Request $request){
			
			 $file = $request->file('csvfile');
			 $errors = 0;
			 $error_arr = array();
				if(null != $file){
					
					$ext = $file->getClientOriginalExtension();
        
        
					if($ext != 'csv'){

						echo "please use csv file to upload";

					}else{

							$file = Csv::toArray($request->csvfile);

							if($file === false){

								 echo "cannot open file";
							}else{
									
									
								foreach($file as $update){
									
									$item = Product::where('pcode','=',$update['pcode'])->first();
									
									if(!$item->update($update)){
										
										$errors++;
										array_push($error_arr,$update['pcode']);
									}
								}
								
								if($errors > 0){
									
									echo "something went wrong went updating </br></br>";
									dd($error_arr);
								}else{
									echo "update successful";
									
								}
								

							}

					}
					
				}else{
					
					echo "add file to upload";
				}
        
		}
}
