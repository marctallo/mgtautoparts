<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\AuditTrail;
use Auth;
use App\Custom\Repository\BaseRepositoryInterface;


class UserController extends Controller{
    
    private $user;
	
		protected $user_repository;
    
    public function __construct(BaseRepositoryInterface $user_repository){
    
        $this->user = Auth::user();
			
				$this->user_repository = $user_repository;
    
    }
    public function index(){
        
        return view('user.index');
    }


    public function create(){
        //\Audit::log();
       
        return view('user.new');
    }


    public function store(UserRequest $request){
			
			$this->user_repository->store($request);

			return redirect('users');
    }

    public function show($id){
        
			if($this->user->role != 3){

					return redirect('dashboard');
			}

			$per_page = 10;
			$user = User::findOrFail($id);
			$trails = AuditTrail::getTrail($id)->paginate($per_page);


			return view('user.view')->with(['user'=>$user,'trails' => $trails]);
    }


    public function edit($id){
        
			$user =  $this->user_repository->find($id);
        
      return view('user.edit')->with('user',$user);
    }

    public function update($id,UserRequest $request){
			
        $this->user_repository->update($id,$request);
        
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        
				$this->user_repository->destroy($id);
        
        return redirect('users');
    }
    
    public function getList($trash)
    {
       return $this->user_repository->getList($trash , $this->user->id);
        
    }
    
    public function myAccount(){
        
   
        return view('user.account')->with(['user'=>$this->user]);
    
    }
    
    public function updateAccount($id,UserRequest $request){
    
       $this->user->update($request->all());
        
        return redirect('myaccount');
    }
    
    public function showTrash(){
    
        return view('user.trash');
    }
    
    public function permanentDelete($id){
    
        $this->user_repository->permanentDelete($id);
        
        return redirect('users/trashed');
    }
    
    public function restoreTrashed($id){
    
        $this->user_repository->restore($id);
        
        return redirect('users/trashed');
        
    }

}
