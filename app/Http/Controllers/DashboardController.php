<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductRequest;
use App\Custom\Services\ProductsService;
use App\Custom\Services\SalesService;

class DashboardController extends Controller
{
    
		protected $products_service;
	
		protected $sales_service;
	
		public function __construct(ProductsService $products_service , SalesService $sales_service){
			
			$this->products_service = $products_service;
			
			$this->sales_service = $sales_service;
		}	
	
    public function index(){
    
        
        $top_items = $this->products_service->getTopSelling();
			
        $critical_items = Product::getCriticalItems();
			
        $requested_items = ProductRequest::getMostRequestedProduct();
        
        $sales = $this->sales_service->getTotal();
        

        return view('dashboard.index')->with(
            ['top_items' => $top_items,
             'critical_items' => $critical_items,
             'sales' => $sales,
             'requested_items' => $requested_items
        ]);
    
    }
    
    public function dailySales(){
    
        return $this->sales_service->getDaily();
    }
    
    public function monthlySales(){
    
       return $this->sales_service->getMonthly();
    }
}
