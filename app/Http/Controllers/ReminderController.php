<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Reminder;

class ReminderController extends Controller
{
	
	private $id;
	
	public function __construct(){
		
		$this->id = \Auth::user()->id;
	}
    
	public function saveReminder(Request $request){

		
		$reminder = [
			'user_id' => $this->id,
			'title' => $request->input('title'),
			'start' => $request->input('start'),
			'end'	=> $request->input('end')
			
		];

		
		if($reminder = Reminder::create($reminder)){
			
			return array('success' => 1,'id'=>$reminder->id);
		}else{
			
			return array('success' => 0);
		}
	}
	
	public function getReminders(){
		
		$reminders = Reminder::getReminders($this->id);
		
		if(count($reminders)){
			
			return $reminders;
		}
	}
	
	public function removeReminder(Request $request){
		
		$result = array("success" => 0);
		
		$id = $request->input('id');
		
		$reminder = Reminder::findOrFail($id);
		
		
		if($reminder->delete()){
			
			$result['success'] = 1;
		}
		
		
		return $result;
		
	}
		
}
