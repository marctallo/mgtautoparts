<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('login/index');
    }

    public function auth(){
        
        $validator = \Validator::make(\Input::all(),array(
		
			'username' 	=> 'required',
			'password' 	=> 'required'
		));
		
		
		if($validator->fails()){
			
			return redirect('/')->withErrors($validator)->withInput();
		}else{
			 
            
            
			 if (\Auth::attempt(['username' => \Input::get('username'), 'password' => \Input::get('password')])){
                 
                 \Auth::user()->last_login = new \DateTime();
                 \Auth::user()->save();
                 
                 /*$audit = array(
                    'user_id' => \Auth::user()->id,
                    'item_id' => \Auth::user()->id,
                    'action'  => 'login',
                    'section' => 'login'
                );

                \Event::fire("audit",array($audit));*/
				
				
				return redirect()->intended('/dashboard');
			}else{
				
				$validator->errors()->add('username' ,'Invalid username or password.');
				
				return redirect('/')->withErrors($validator)->withInput();
			}
			
		}
    }
    
    public function logout(){
        
     
		\Auth::logout();
        
        
		
		return redirect('/');
	}

}
