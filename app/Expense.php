<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expense extends BaseModel
{
    
    use SoftDeletes;
    
    protected $table = 'expenses';
    
    
    protected $fillable = [
        'expense_category', 
        'expense_date', 
        'amount',
        'note'
    ];
    
    
    public function scopeGetExpense($query,$month){
        
        
        //dd($month);
        
        if($month == null){
        
            $month = date('n');
            
        }
        
		
		$query->leftJoin('expenses_category','expenses.expense_category','=','expenses_category.id')
              ->whereRaw('month(expense_date) ='.$month)
              ->whereRaw('year(expense_date) ='.date('Y'))
              ->select(['expenses.id','expenses.amount','expenses_category.cname','expenses.note'])
			  ->orderBy('expenses_category.cname','asc');
			 
	}
    
    
   
}
