<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
		protected $table = 'settings';
    protected $fillable = ['company_name', 'address', 'number','email','show_inactive','per_page','base_markup'];
}
